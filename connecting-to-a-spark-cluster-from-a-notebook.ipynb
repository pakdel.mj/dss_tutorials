{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Connecting to a Spark Cluster from a Notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "This tutorial covers:\n",
    "\n",
    "- Using a SparkHook from a Notebook to submit Spark commands to:\n",
    "    - the system Spark cluster\n",
    "    - a dedicated (on-demand) Spark cluster\n",
    "- Monitoring the spark cluster with the Spark Master UI\n",
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the Python notebooks provided by Data Science Studio, users can harness the platform's system Spark cluster or get \n",
    "a dedicated Spark cluster to execute Spark commands against.\n",
    "\n",
    "## The System Spark Cluster\n",
    "\n",
    "The system Spark cluster is an auto-scaling cluster that is shared between all users.  Because this cluster is always \n",
    "running, it can provide quick access to a spark session.\n",
    "\n",
    "> NOTE: Use a builder function to set specific spark application settings. When setting the Spark executor cores, Spark \n",
    ">executor memory, or setting the maximum number of executors (property of dynamic allocation), make sure to abide by the \n",
    ">limits of the system Spark cluster which can be found in the /opt/spark/system-spark-cluster-config directory.\n",
    "\n",
    "<span style=\"font-weight: bold; color: red\">The system spark cluster is a shared resource. Recipe developers should be \n",
    "careful to be good tenants and only create and hold spark sessions when they need to run some spark work.  The spark \n",
    "session should be stopped as soon as the the spark work is done. In a notebook the spark session is created as soon as \n",
    "the SparkHook() class is initiated. You should execute your spark action code as soon as possible after creating the \n",
    "spark hook and then call `spark.stop()` when your spark work is done. Otherwise you are wasting compute resources and \n",
    "preventing your peers from enjoying the full benefits of the system spark cluster.</span>\n",
    "\n",
    "In a notebook running the Python 2 kernel, simply using the Spark hook as seen below will establish a connection to the \n",
    "system's Spark cluster.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dss_airflow_utils.hooks import SparkHook\n",
    "\n",
    "def builder_func(builder):\n",
    "    builder.appName('my_spark_app_name')                         # Easier to find you application in spark-logs\n",
    "    builder.config('spark.dynamicAllocation.maxExecutors', '2')  # Default max executors is 1 \n",
    "    builder.config('spark.executor.memory', '1G')                # Increase requests when needed\n",
    "    # Add more spark configuration properties as needed \n",
    "    # see: https://spark.apache.org/docs/latest/configuration.html\n",
    "\n",
    "hook = SparkHook(conn_id=\"metastore_ndx_uat_adls\", builder_func=builder_func)\n",
    "spark = hook.get_spark_session()\n",
    "\n",
    "# Use spark session in a way you like, e.g., \n",
    "all_databases = spark.sql('show databases')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Let's look at some of the results\n",
    "all_databases.take(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't forget to stop spark session when you are done\n",
    "spark.stop()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a notebook running the Python 3 kernel, you will need to set the PYSPARK_PYTHON variable to point to Python 3 prior \n",
    "to connecting to the Spark cluster.  If the PYSPARK_PYTHON variable was not set before executing the Spark hook you will \n",
    "need to restart the kernel and set the environmental variable before you are able to continue. \n",
    "\n",
    "The code below first checks whether the kernel is Python 2 or Python 3 and sets the environment if needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the PYSPARK_PYTHON variable\n",
    "import os\n",
    "import sys\n",
    "if sys.version[0] == '3':\n",
    "    os.environ['PYSPARK_PYTHON']='/usr/bin/python3'\n",
    "\n",
    "from dss_airflow_utils.hooks import SparkHook\n",
    "\n",
    "def builder_func(builder):\n",
    "    builder.appName('my_spark_app_name')                         # Easier to find you application in spark-logs\n",
    "    builder.config('spark.dynamicAllocation.maxExecutors', '2')  # Default max executors is 1 \n",
    "    builder.config('spark.executor.memory', '1G')                # Increase requests when needed\n",
    "    # Add more spark configuration properties as needed \n",
    "    # see: https://spark.apache.org/docs/latest/configuration.html\n",
    "\n",
    "hook = SparkHook(conn_id=\"metastore_ndx_uat_adls\", builder_func=builder_func)\n",
    "spark = hook.get_spark_session()\n",
    "\n",
    "# Use spark session in a way you like, e.g., \n",
    "databases = spark.sql('show databases')\n",
    "\n",
    "# Don't forget to stop spark session when you are done\n",
    "spark.stop()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## The System Spark Master Web UI\n",
    "\n",
    "You can view the status of submitted spark jobs by looking at the spark master UI - accessible at \n",
    "_[namespace]-system-spark-master.[domain]_. A shortcut to this page is also available at the DSS home page."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Dedicated/On-demand Spark Clusters\n",
    "\n",
    "Your notebook can also generate its own Spark cluster via the Spark Cluster service by providing a SparkClusterConfig to \n",
    "your SparkHook. The notebook acts at the cluster's driver and has Spark2.3.3 installed which means that the worker you\n",
    "choose for your dedicated cluster also needs to be a Spark2.3.3 image.\n",
    "\n",
    "Note that when the following code is executed, you will be creating a new Spark cluster which will require time for the \n",
    "requested resources to be scheduled by Kubernetes and then start.\n",
    "Your dedicated Spark cluster will be auto-scaling, like the system cluster, which means it will only use resources as it \n",
    "needs them.  When you are actively using the cluster, it will be able to scale up to your request node count.  When it \n",
    "is not as busy, the cluster can scale down to one worker node. You can find the limits of resources requests in the\n",
    "cluster information section of DSS Home under the 'On-demand Spark Cluster' section.\n",
    "\n",
    "> <span style=\"font-weight: bold; color: red\">CAUTION: If you decide to use a dedicated Spark cluster, keep in mind that\n",
    ">it will be subject to the Spark Cluster extinguisher which spins down inactive clusters. The extinguisher evaluates the \n",
    ">activity of a spark cluster by checking if the cluster has any active applications and, if it does not, checks to see \n",
    ">the last time a request was sent to the cluster. If you have exited your Spark session and there are no more active \n",
    ">applications, your cluster may be deleted by the extinguisher. </span>\n",
    "\n",
    "The following code demonstrates creating a connection to a dedicated Spark cluster for a Python2 notebook. Note the \n",
    "import of the python logging module and the creation of a new logger calling dss_airflow_utils.  This logging enhances\n",
    "the output during the creation of the hook which will include the spark config information as well the spark uri and the\n",
    "spark master web console url. \n",
    "\n",
    "Just like with a hook to the system Spark cluster, you should execute your spark action code as soon as possible after \n",
    "creating the spark hook and then call `spark.stop()` when your spark work is done. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from dss_airflow_utils.hooks import SparkHook\n",
    "from dss_airflow_utils.spark import SparkClusterConfig\n",
    "\n",
    "import logging\n",
    "logger = logging.getLogger('dss_airflow_utils')\n",
    "logger.setLevel(logging.INFO)\n",
    "\n",
    "def builder_func(builder):\n",
    "    builder.appName('my_spark_app_name')                         # Easier to find you application in spark-logs\n",
    "    builder.config('spark.dynamicAllocation.maxExecutors', '2')  # Default max executors is 1 \n",
    "    builder.config('spark.executor.memory', '1G')                # Increase requests when needed\n",
    "\n",
    "# Note that the worker type uses Spark2.3.3 to match the notebook Spark version\n",
    "scc = SparkClusterConfig(worker_type=\"spark2.3.3-python2.7-worker\",\n",
    "                        num_nodes=2,\n",
    "                        request_cpu_per_node=1,\n",
    "                        request_memory_gb_per_node=4\n",
    "                        )\n",
    "\n",
    "hook = SparkHook(sparkcluster_config=scc, builder_func=builder_func)\n",
    "spark = hook.get_spark_session()\n",
    "\n",
    "# Use spark session in a way you like, e.g., \n",
    "databases = spark.sql('show databases')\n",
    "\n",
    "# Don't forget to stop spark session when you are done\n",
    "spark.stop()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The following code demonstrates creating a connection to a dedicated Spark cluster for a Python3 notebook. "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from dss_airflow_utils.hooks import SparkHook\n",
    "from dss_airflow_utils.spark import SparkClusterConfig\n",
    "\n",
    "import logging\n",
    "logger = logging.getLogger('dss_airflow_utils')\n",
    "logger.setLevel(logging.INFO)\n",
    "\n",
    "import os\n",
    "import sys\n",
    "if sys.version[0] == '3':\n",
    "    os.environ['PYSPARK_PYTHON']='/usr/bin/python3'\n",
    "\n",
    "def builder_func(builder):\n",
    "    builder.appName('my_spark_app_name')                         # Easier to find you application in spark-logs\n",
    "    builder.config('spark.dynamicAllocation.maxExecutors', '2')  # Default max executors is 1 \n",
    "    builder.config('spark.executor.memory', '1G')                # Increase requests when needed\n",
    "\n",
    "# Note that the worker type uses Spark2.3.3 to match the notebook Spark version\n",
    "scc = SparkClusterConfig(worker_type=\"spark2.3.3-python3.7-r3.6.0-worker\",\n",
    "                        num_nodes=2,\n",
    "                        request_cpu_per_node=1,\n",
    "                        request_memory_gb_per_node=4\n",
    "                        )\n",
    "\n",
    "hook = SparkHook(sparkcluster_config=scc, builder_func=builder_func)\n",
    "spark = hook.get_spark_session()\n",
    "\n",
    "# Use spark session in a way you like, e.g., \n",
    "databases = spark.sql('show databases')\n",
    "\n",
    "# Don't forget to stop spark session when you are done\n",
    "spark.stop()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "The `worker_type` specified in the SparkClusterConfig can be any of the spark2.3.3-* worker images that also match the \n",
    "kernel of Python you are running with. To see a list of the spark images, please refer to the Available Images section\n",
    "of the Cluster Information in DSS Home.\n",
    "\n",
    "When running your own cluster, you will have access to the cluster's Spark master UI. The link to the UI of the Spark \n",
    "master is included in the logs when the dss_airflow_utils logger is instantiated (as seen in the code above). You can\n",
    "also get the spark master ingress url directly from the hook object as seen below. When the cluster is spun down, the \n",
    "UI becomes unavailable, but you can still find the application logs (stdout and stderr) from your notebook. The logs of \n",
    "a Spark cluster can be found in a folder, named after the spark cluster's id, in the shared directory (/shared). The \n",
    "cluster's id is a property of the SparkHook object, but can also be found in the output of the cell that instantiates \n",
    "the SparkHook.  "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "# Returns your Spark cluster's master UI URL\n",
    "hook.spark_master_ingress_url\n",
    "\n",
    "# Returns your Spark cluster's id\n",
    "hook.cluster_id\n"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}