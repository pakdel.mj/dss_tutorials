{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"top\"></a>\n",
    "# Scheduling a Recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This User Guide covers:\n",
    "    \n",
    "- The `recipe schedule` command\n",
    "  * Breakdown of the arguments\n",
    "- Stopping a Scheduled Recipe\n",
    "- Additional Notes\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NOTE: This guide assumes familiarity with the recipe CLI that can be accessed through a Jupyter terminal.  This material is covered in the [Running A Recipe](./running-recipe.ipynb) and [Running A Recipe Example](./running-a-recipe-example.ipynb) tutorials.\n",
    "\n",
    "Airflow allows a DAG to be run on a predetermined schedule. The DSS ecosystem leverages that feature to enable the scheduling of published recipes. Any recipe that can be run can also be scheduled. The recipe CLI verb that will be used for this purpose is \"schedule\". First, let's take a look at the CLI documentation for this verb."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Usage:\n",
      "  recipe schedule <job_request_file> <schedule_interval> <start_date> [options]\n",
      "\n",
      "Description:\n",
      "  Schedule the recipe indicated in the job request file in Airflow. The\n",
      "  recipe should already be published to the Recipe Library.\n",
      "\n",
      "Arguments:\n",
      "  <job_request_file>    Job request file which should identify a specific\n",
      "                        recipe.\n",
      "  <schedule_interval>   A string that represents the interval on which the recipe will run. Several presets are provided\n",
      "                        namely @once, @hourly, @daily, @weekly, @monthly, @yearly. For further customization, a direct\n",
      "                        cron expression string can be provided (i.e. \"*/15 * * * *\" for every 15 minutes).\n",
      "  <start_date>          A string representing the date that the recipe will start running. Any date/datetime string\n",
      "                        that can be parsed by dateutils.parser is valid. Please note that start dates in the past\n",
      "                        are not valid and date strings will default to UTC.\n",
      "\n",
      "Options:\n",
      "  -h --help                                    Show this screen.\n",
      "  --api_url=<api_url>                          The url of the API to talk to. A default url is only available inside the cluster, so either\n",
      "                                               this option or the host option is required to connect externally.\n",
      "  --host=<host>                                The host (and tenant) for the recipe api and oauth2 api e.g. http://dss-uat.nielsen.com/default\n",
      "                                               This parameter will be used to construct the urls of the recipe and oauth2 APIs. They will be constructed\n",
      "                                               as {host}/recipe-service/api/v1 and {host}/oauth2-proxy/api/v1 respectively.\n",
      "                                               The api_url and oauth2_url parameters will override their respective host generated values if present.\n",
      "                                               NOTE: you probably just need to use this parameter and not the api_url and oauth2_url\n",
      "  --oauth2_url=<oauth2_url>                    The url of the oauth2 API to talk to e.g. http://dss-uat.nielsen.com/default/oauth2-proxy/api/v1\n",
      "                                               A default url is only available inside the cluster, so either this option or the host option is\n",
      "                                               required to connect externally.\n",
      "  --client_credentials_path=<client_credentials_path> The path to the yaml file that contains valid oauth2 client credentials.\n",
      "                                               The yaml file might look like:\n",
      "                                               client_id: my-client-id\n",
      "                                               client_secret: my-client-secret\n",
      "                                               These credentials are essential if accessing the recipe service from outside of a jupyter notebook.\n",
      "                                               If this value is not passed, the client credentials will be searched for on the following paths:\n",
      "                                               client_credentials.yaml, .client_credentials.yaml, ~/.dss/client_credentials.yaml, and ~/.dss/.client_credentials.yaml\n",
      "  --end_date=<end_date>                        A string representing the date that the recipe will stop running. Any date/datetime string\n",
      "                                               that can be parsed by dateutils.parser is valid. If not passed, this parameter will\n",
      "                                               default to None, meaning that the recipe will run on the interval indefinitely. This date string\n",
      "                                               will default to UTC.\n",
      "  --name=<name>                                A string that will be used to name the scheduled version of this recipe. Otherwise, an automatically generated name will\n",
      "                                               be used. The string is prepended to the dag id.\n",
      "\n",
      "Examples:\n",
      "    recipe schedule job_request.yaml @daily \"2119-01-31\"\n",
      "    recipe schedule job_request.yaml @hourly \"2119-01-31T05:30\" --end_date=\"2220-01-31\"\n",
      "    recipe schedule job_request.yaml \"*/15 * * * *\" \"2119-07-02 17:00\" --end_date=\"2119-07-02 17:15\" --name=\"every15mins\"\n",
      "    \n"
     ]
    }
   ],
   "source": [
    "! recipe schedule --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's break down the variables. The basic format for the command is:\n",
    "\n",
    "`recipe schedule <job_request_file> <schedule_interval> <start_date> [options]`\n",
    "\n",
    "The options include `--name` and `--end_date`, which will be described below.\n",
    "\n",
    "### Job Request File\n",
    "The job request file is typically a local file like `job_request.yaml`. For more information on the job request file, please see the [Running A Recipe](./running-recipe.ipynb) tutorial.\n",
    "\n",
    "### Schedule Interval\n",
    "The first required parameter after the job request file is `schedule_interval`. There are two types of acceptable schedule intervals - several preset adverb strings beginning with `@` or custom cron expressions. For the simpler scenarios, the following preset strings are available:\n",
    "* `@once` - run once at some time at some date\n",
    "* `@hourly` - run once every hour relative to some time (e.g., 2:30, 3:30, 4:30, etc.)\n",
    "* `@daily` - run once every day at some time (e.g., 5:00)\n",
    "* `@weekly` - run once every week at some time on some day of the week (e.g., Monday at 5:00)\n",
    "* `@monthly` - run once every month at some time on some day (e.g., the 13th at 5:00)\n",
    "* `@yearly` - run once every year on some day of some month at some time (e.g., April 1st at 5:00)\n",
    "\n",
    "A recipe scheduled using one of the preset expressions will run _relative to_ the provided start_date parameter so the example `recipe schedule job_request.yaml @hourly \"2119-01-31T05:30\"` will schedule a recipe to first run at 5:30 UTC on 2119-01-31 with its subsequent runs at 2119-01-31T06:30, 2119-01-31T07:30, 2119-01-31T08:30, etc. Similarly, `@daily 2119-01-31T05:30` will first run at 5:30 UTC on 2119-01-31 with its subsequent runs at 2119-02-01T05:30, 2119-02-02T05:30, etc.\n",
    "\n",
    "> **Note: The recipe scheduling interval, start date, and end date behavior will take effect at the beginning of August 2019**. Previously, the behavior relied on Airflow's scheduling implementation which had two noticeable differences.\n",
    "1. The preset intervals were _not_ relative to the provided start_date and so @daily would mean \"the **start** of every day\" and so on for the others\n",
    "2. The start/end date and execution date are assumed to be in the context of ETL workloads and so the first run would actually occur _one interval after_ the provided start date so @daily starting on a Monday would not run on Monday but instead Tuesday. Similar behavior would have occurred with end date but has since been changed as well.\n",
    ">\n",
    ">More information about expected side effects from this change can be found at the end of this tutorial in the Additional Notes section.\n",
    "\n",
    "#### Complex Schedules\n",
    "For intervals more complex than the provided presets, we need to use a [cron expression](https://help.ubuntu.com/community/CronHowto#Crontab_Lines). For example, let's say the recipe must run every half hour (2:00, 2:30, 3:00, 3:30, etc). To accomplish such an interval, a cron expression like `*/30 * * * *` can be used. For more information on the syntax and meaning of such an expression, plenty of material and cheatsheets are available online around cron, as it is a widely used linux utility. \n",
    "\n",
    "Wrapping up, to schedule a recipe specified in a job request file called `job_request.yaml` to run starting January 1st, 2019 at 00:00 and recurring every 30 minutes, the following recipe CLI command could be used:\n",
    "\n",
    "`recipe schedule job_request.yaml \"*/30 * * * *\" \"2019-01-01\"`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Start Date\n",
    "The second required parameter following the `schedule_interval` is a `start_date`. This value can be any date string that can be parsed by [python-dateutil's](https://dateutil.readthedocs.io/en/stable/) `dateutil.parser`. Any datetime strings without a specified timezone will be parsed in UTC. Some acceptable start date values are:\n",
    "* `\"2019-01-31\"`\n",
    "* `\"2019-01-31 5:20\"`\n",
    "* `\"2019-01-31T5:20\"`\n",
    "* `\"Sat Oct 11 17:13:46 UTC 2003\"`\n",
    "\n",
    "> Note: For simplicity's sake on DSS, it is recommended to avoid timezone complexities by working entirely with UTC. Airflow datetimes are almost entirely UTC-based and a UTC clock can be found in the upper-right corner of the airflow web UI.\n",
    "\n",
    "The start date value actually serves two purposes depending on the `schedule_interval` being used. If one of the interval presets is specified, the `start_date` will typically be the first run time as well as the base point to increment from (examples and their respective behavior above). However, if a custom interval is specified, it's likely that the first run **may not** occur on the provided `start_date` and so the `start_date` is really just a marker/switch to start evaluating your scheduling interval.\n",
    "\n",
    "For example, given the command `recipe schedule \"*/15 * * * *\" \"2019-01-01 2:20\"`, the first run will be at \"2019-01-01 2:30\" with subsequent runs at \"2019-01-01 2:45\", \"2019-01-01 3:00\", and so on and **_NOT_** at 2:20 with subsequent runs at 2:35, 2:50, 3:05, etc. The reason for this is rooted in the specificity of the cron expression `\"*/15 * * * *\"`. If that is unclear, then we encourage you to learn more about cron external to this tutorial and revisit this later or simply use one of the preset interval expressions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### End Date\n",
    "Let's now imagine a scenario in which a recipe only needs to run on a schedule for a predetermined amount of time, say through the first half of the year 2019. To accomplish this, an end date needs to be set in addition to the start date. For this, the optional `--end_date` argument is used. The `end_date` is subject to the same restrictions as the `start_date` and additionally, must be _after_ the `start_date`.\n",
    "\n",
    "If you specify the end date on the boundary of an interval, the boundary run is _not_ included (e.g., if schedule_interval is @hourly starting at 1:00 and your end date is 3:00, then there will be 2 runs, one at 1:00 and one at 2:00). If you schedule to end between interval boundaries, the last run will be the interval before the provided end date (e.g., an end at 2:15 in the previous example will also have 2 runs, one at 1:00 and one at 2:00)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Name\n",
    "The last option to `recipe schedule` provides a way to override the recipe service's naming convention for the scheduled recipe's dag id. By default, the recipe service will autogenerate a prefix for your recipe's dag id using the inputs provided. For example, `scheduled_2019_07_03T17_56_00_hourly_simple-recipe_0_0_3`.\n",
    "\n",
    "By providing a custom name via `--name`, you can name your scheduled recipe's dag id to better identify it.\n",
    "\n",
    "`recipe schedule job_request.yaml @weekly \"2119-07-03\" --name weekly_us`\n",
    "will schedule your recipe with a dag id `weekly_us_<recipe-id>_<recipe-version>`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stopping a Scheduled Recipe\n",
    "Previously, the recipe client's schedule verb had an option to `--delete` a scheduled recipe. This has been **removed** and replaced with `recipe pause`. A scheduled recipe can also be \"paused\" by turning the DAG off from the Airflow UI. The `recipe pause` command just requires a `dag_id`, so to pause the weekly recipe mentioned above, the command `recipe pause weekly_us_<recipe-id>_<recipe-version>` can be used. This will prevent any future scheduled runs of the recipe from running. If the recipe has a running dag run, it will continue to run. If you wish to unpause the recipe, use the command `recipe unpause weekly_us_<recipe-id>_<recipe-version>`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional Notes\n",
    "### Scheduled Recipes in the Airflow UI\n",
    "As noted earlier in this tutorial, **The recipe scheduling interval, start date, and end date behavior will take effect at the beginning of August 2019. Previously, the behavior relied on Airflow's scheduling implementation which had two noticeable differences**. Because of these differences, you may notice a few oddities when viewing your dag run information from the Airflow web interface. Specifically, a recipe scheduled to run @hourly starting at 2:00 will have a dag run in Airflow that (as expected) starts at 2:00 but the listed `execution_date` will say 1:00 and the `run_id` will include the same execution_date. You should notice the proper start date in the dag_id if you did not provide a custom name prefix via `--name` and if you look at the task instances, you would also notice the task instances themselves have the proper start times.\n",
    "### Backfilling\n",
    "Backfilling, or catch-up, is an Airflow native technique that allows runs to be scheduled for dates that have already past.\n",
    "\n",
    "Backfilling/catch-up is not supported in DSS at this time. The schedule verb enforces this by forcing the start date to be in the future. This prevents the accidental running of a large number of dags simultaneously."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
