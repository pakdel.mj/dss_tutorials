{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial: Publishing Recipes to the Recipe Library\n",
    "\n",
    "*******\n",
    "\n",
    "This tutorial covers:\n",
    "\n",
    "- The recipe command\n",
    "- The recipe library\n",
    "- Publishing a recipe\n",
    "- Reviewing a recipe (this is external to DSS)\n",
    "- Searching the recipe library\n",
    "\n",
    "******************"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The recipe command line interface (CLI) is included in your Jupyter terminal interface.  This is a master command which requires a sub-command to do anything.  We have [already seen](running-recipe.ipynb#top) the _recipe taste_ command.  Here are the other options.  You can get this information with the shell command \"recipe --help\". (You can run a shell command from the notebook by putting a exclamation mark in front of it.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Recipe Client connects to the Recipe Services API of the Analytics\r\n",
      "Services.\r\n",
      "\r\n",
      "Usage:\r\n",
      "  recipe [--help] <command> [<args>...]\r\n",
      "\r\n",
      "Commands:\r\n",
      "  publish         Publish the source folder of the recipe to the Recipe Library\r\n",
      "\r\n",
      "  run             Run in airflow the recipe indicated in the job request file.\r\n",
      "                  The recipe should be already published to the Recipe Library.\r\n",
      "\r\n",
      "  schedule        Schedule the recipe indicated in the job request file in\r\n",
      "                  Airflow. The recipe should be already published to the Recipe\r\n",
      "                  Library.\r\n",
      "\r\n",
      "  taste           Run in airflow the recipe indicated in the job request file\r\n",
      "                  without publishing it to the Recipe Library.\r\n",
      "\r\n",
      "  ingredient      Ingredient refers to the data your recipe relies on. This\r\n",
      "                  action will not trigger a recipe to run.\r\n",
      "\r\n",
      "    add           Adds data into the datacache.\r\n",
      "\r\n",
      "    get           Retrieves data in the datacache.\r\n",
      "\r\n",
      "  validate        Validate a job request file or the structure of a recipe\r\n",
      "                  source folder.\r\n",
      "\r\n",
      "  search          Search published recipe IDs and description strings.\r\n",
      "                  Retrieves the recipe ID, description, and latest version\r\n",
      "                  number (or optionally all versions).\r\n",
      "\r\n",
      "  test            Run the tests defined in the recipe source folder.\r\n",
      "\r\n",
      "Options:\r\n",
      "  -h --help       Show this screen.\r\n",
      "  --version       Show version.\r\n"
     ]
    }
   ],
   "source": [
    "! recipe --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you've finished developing a recipe you can publish it.  Publishing it sends it into a repository, which will be reviewed and, if it passes the minimum testing and quality checks, it will be merged into a public repository which is shared throughout your organization.  With this we can create a community supported set of resources that other recipe developers can use. \n",
    "\n",
    "Once a recipe is published it is immutable and no further edits can be done.  However, all recipes are versioned. Any changes, updates or fixes after publish should be applied, then re-published with an updated `metadata` increasing the version number, updating the description recording the changes. For best practices on versioning, see [semver](http://semver.org/).\n",
    "\n",
    "Every recipe published is submitted for review prior to publication in the production environment. Once you publish your recipe, contact the appropriate reviewer to check on the status of your request.\n",
    "\n",
    "Recipes are publish by using the recipe-cli, in the case of the example we've built up it would be"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "# Important!: DON'T DO THIS WITH THE EXAMPLE.\n",
    "recipe publish ~/work/my_first_dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will zip up the contents of skeleton and send it off to be reviewed. \n",
    "\n",
    "### Recipe Review and Promotion\n",
    "\n",
    "This is a process that is handled outside of DSS.  The intention is that several team members will review the code for good coding standards, efficiency, etc. before the recipe is promoted to being in the __recipe library__.\n",
    "\n",
    "The recipe library is a code versioning repository that DSS communicates with to retrieve recipes that have been reviewed and promoted. Review and promotion of a published recipe to the recipe repository is done by a process external to DSS.\n",
    "\n",
    "The recipes in the recipe library can be run by any team member without being local to your work directory like a recipe you are tasting must be.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### \"recipe search\" command line option\n",
    "\n",
    "You can find the names and descriptions of recipes in the recipe repository with the \"search\" option of the recipe command.  _Recipe search_ is provided to help you find the name of a recipe that you would want to call or use.\n",
    "\n",
    "Currently if you find a recipe that you think you would like to use you should retrieve the metadata.yaml file for that recipe directly from the code repository that backs DSS's repository."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "recipe search --help\n",
    "\n",
    "usage: recipe search [<search_pattern> options]\n",
    "\n",
    "Description:\n",
    "  Search the recipes in the Recipe Library for ones whose id or\n",
    "  description contains the search_pattern regular expression.\n",
    "\n",
    "Arguments:\n",
    "  <search_pattern>      Regular expression to match. [default: .*]\n",
    "\n",
    "Options:\n",
    "  -h --help             Show this screen.\n",
    "  --api_url=<api_url>   The url of the API to talk to\n",
    "                        [default: http://recipe-service:8000/api/v1]\n",
    "  --all-versions        Return all the version strings for recipes whose\n",
    "                        most recent version's recipe id or description matches\n",
    "                        the search expression.  Default is to just return the\n",
    "                        highest version string.\n",
    "  --match-case          Make the match case sensitive\n",
    "  --highlight           When printing output highlight the matches\n",
    "```\n",
    "\n",
    "Usually this command would be run from a Jupyter terminal but it can be run from a notebook by using the notebook's shell escape.\n",
    "\n",
    "Here a search is made for recipes with \"simple\" in their name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2018-09-21 21:44:05,740 - INFO - Attempting to search the recipes with id or description matching simple in the recipe repository.\n",
      "2018-09-21 21:44:05,740 - INFO - Returning only the highest version string.\n",
      "2018-09-21 21:44:05,740 - INFO - Returning case insensitive matches.\n",
      "2018-09-21 21:44:05,740 - INFO - \n",
      "2018-09-21 21:44:05,740 - INFO - Searching for 'simple'\n",
      "2018-09-21 21:44:08,653 - INFO - Found 1 recipes matching 'simple' in the recipe repository.\n",
      "\n",
      "     Recipe ID                      Highest Version\n",
      "     _________                      _______________\n",
      "\n",
      "     Recipe ID: simple-recipe                  2_30_0\n",
      "     Description: A simple recipe\n",
      "\n",
      "2018-09-21 21:44:08,654 - INFO - 'Search Recipes' command was successfully executed.\n"
     ]
    }
   ],
   "source": [
    "! recipe search simple"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### 'recipe run' and the RecipeRunOperator\n",
    "\n",
    "Once a recipe is in the recipe repository anyone on the team can run it from the command line or include it within their dag by using the RecipeRunOperator.  This is covered in the [Recipe Run Operator](recipe-run-operator.ipynb#top) tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* * *\n",
    "\n",
    "[Next](./data-contract.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
