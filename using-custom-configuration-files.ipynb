{
 "nbformat": 4,
 "nbformat_minor": 2,
 "metadata": {
  "language_info": {
   "name": "python",
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   }
  },
  "orig_nbformat": 2,
  "file_extension": ".py",
  "mimetype": "text/x-python",
  "name": "python",
  "npconvert_exporter": "python",
  "pygments_lexer": "ipython3",
  "version": 3
 },
 "cells": [
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Using non-python static configuration files.\n",
    "==============================\n",
    "\n",
    "*****\n",
    "\n",
    "This tutorial covers:\n",
    "-   [The recipe_custom_config Utility Method](#the-recipe_custom_config-utility-method)\n",
    "    \n",
    "*****\n",
    "\n",
    "There may come a time when you want to store some non-python file local to your recipe to be used during execution. \n",
    "> _Of course, for working with **large** sets of data, tools like the datacache or external data stores should be your go-to as opposed to attempting to bundle massive data sets with your recipe._ \n",
    "\n",
    "A simple example might be reading some configuration file beyond the scope of the job request. \n",
    "At first, this seems like it'd be a simple situation:\n",
    "\n",
    "```python\n",
    "# dag.py\n",
    "# imports here\n",
    "\n",
    "def read_file(path):\n",
    "    with open(path, \"r\") as f:\n",
    "        contents = f.read()\n",
    "    return contents\n",
    "\n",
    "@dag_factory\n",
    "def create_dag():  # pragma: no cover\n",
    "    with DAG(dag_id='my-first-recipe',\n",
    "             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "        # Define operators here, for example:\n",
    "        ...\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Let's say the file you're trying to read is next to `dag.py` inside of your recipe. During local testing,\n",
    "you write some unit test:\n",
    "```python\n",
    "def test_read_file():\n",
    "    assert read_file(\"file.txt\") == \"my configuration file\"\n",
    "```\n",
    "\n",
    "This seems to work fine so you continue developing your recipe with the assurance that your function\n",
    "does as you'd expect. Then, you submit the recipe to the recipe service via `recipe taste`, go\n",
    "to check the status of the running DAG, and unexpectedly find that your file cannot be found - what happened?"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "When you `taste`/`run` a recipe, that directory gets zipped and passed around the DSS environment. By the time it runs via Airflow, the shape of that directory has changed a little bit and the directory that it lives in is not in a very intuitive, human-friendly zone. Additionally, because various tasks will access that recipe throughout its execution, the processes that actually run your recipe's code through a python interpreter do so from various directories - thus causing relative paths like `read_file(\"file.txt\")` to become unreliable. \n",
    "\n",
    "So fine, if relative paths won't work, then let's try using absolute paths. Well wait, we just said that the DAG itself moves around, so how do I know where the DAG is? \n",
    "You can solve this problem with the help of a built-in magic python method `__file__` which returns the absolute path to the current file so you could adjust your code to do something like:\n",
    "\n",
    "```\n",
    "read_file(os.path.join(os.path.dirname(__file__), \"file.txt\"))\n",
    "```\n",
    "\n",
    "This seems like it should work - so long as the file you're calling `read_file` from is one of the files that is _next_ to `file.txt`. Then you `taste` your recipe again only to find that the file _still_ does not exist - _why_? Now you realize that these files are inside of a zip archive, and you need to deal with getting the file out of the zip archive. "
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Ultimately, this ends up being a tedious process of trial and error just to read from some static file within your recipe. Additionally, as a user, I don't want to deal with the subtle nuances between if I submit my recipe to be run through the recipe service or if I'm working with my recipe code locally. \n",
    "\n",
    "Thus, the `dss_airflow_utils` module provides a utility method to work with situations such as this providing you with a single entrypoint that works the same way regardless of if you're working locally or remotely via the recipe service. "
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## The `recipe_custom_config` utility method\n",
    "\n",
    "### Here's how it works\n",
    "\n",
    "First, let's stop relying on finding the desired file relative to the current file - this is brittle and makes your code's behavior change as it moves around within the same recipe. Instead, let's centrally locate these configuration files within a `configs` directory inside of the `dag/` dir. \n",
    "\n",
    "```\n",
    "/recipe$ tree\n",
    ".\n",
    "├── __init__.py\n",
    "├── config.yaml\n",
    "├── dag\n",
    "│   ├── __init__.py\n",
    "│   ├── configs\n",
    "│   │   ├── file.txt\n",
    "│   │   └── package.json\n",
    "│   ├── dag.py\n",
    "│   └── lib\n",
    "│       ├── __init__.py\n",
    "│       └── my_module.py\n",
    "├── metadata.yaml\n",
    "└── tests\n",
    "    ├── __init__.py\n",
    "    └── test_lib\n",
    "        ├── __init__.py\n",
    "        ├── test.yaml\n",
    "        └── test_my_module.py\n",
    "```\n",
    "> Note: it is very important that the `configs/` directory be at the top-level inside of `dag/`.\n",
    "\n",
    "\n",
    "```python\n",
    "from dss_airflow_utils.utils import recipe_custom_config\n",
    "def read_file():\n",
    "    with recipe_custom_config(\"file.txt\", __file__) as f:\n",
    "        return f.read()\n",
    "```\n",
    "\n",
    "The first argument to `recipe_custom_config` is the path to the desired file relative to the configs directory - this file _must be inside of the configs directory_. The second argument to recipe_custom_config is an anchor point. This is an extremely important indicator of where the DAG currently is within DSS and in almost every scenario, you want to pass the magic `__file__` symbol.  \n",
    "So in the example above, notice the first argument is simply \"file.txt\" because the file `file.txt` is at the top of the configs directory. These files could be further organized in sub-folders so long as they remain under the configs directory and the argument provided to `recipe_custom_config` describes the path to get to the desired file from `dag/configs/`."
   ]
  },
  {
   "source": [
    "from dss_airflow_utils.utils import recipe_custom_config\n",
    "\n",
    "help(recipe_custom_config)"
   ],
   "cell_type": "code",
   "outputs": [
    {
     "output_type": "stream",
     "name": "stdout",
     "text": "[2019-11-20 20:29:10,415] {settings.py:174} INFO - setting.configure_orm(): Using pool settings. pool_size=5, pool_recycle=3600\nHelp on function recipe_custom_config in module dss_airflow_utils.utils:\n\nrecipe_custom_config(path_to_config, relative_anchor)\n    Get a file handle to an config file from within a recipe.\n    \n    Use this utility method to reference non-python files from within\n    a recipe. When a recipe is sent to airflow, the DAG gets zipped\n    and imported from a different location on the file system than where\n    the zip lives so a relative path will not work from within a running recipe\n    and it is difficult to know where the path to a static file is as the\n    recipe moves around from local to running in Airflow.\n    \n    IMPORTANT: this utility can ONLY be used from within a recipe.\n    \n    :param path_to_config: a relative path to the desired config file from the\n                          recipe config files directory (top of dag/ folder)\n                          Note: the config file MUST be inside of this configs/ dir.\n    :param relative_anchor: an anchor to identify the current invocation origin.\n                            Almost always pass `__file__` here.\n    :return: a binary file-like object.\n             Note: this means you may have to decode it if you want a unicode string.\n\n"
    }
   ],
   "metadata": {},
   "execution_count": 1
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Additional info\n",
    "The `recipe_custom_config` utility is a context manager - thus allowing you to use the `with` statement with the assurance that any necessary file handles will be implicitly cleaned up so you do not have to worry about remembering to `f.close()`. This interface is _very_ similar to calling `open()` with the mode set to `\"rb\"`.\n",
    "\n",
    "Additionally, you may notice that the file handle returned by `recipe_custom_config` is actually a _binary_ file handle. This means that when you do `f.read()`, you'll get a sequence of bytes returned instead of a string.\n",
    "> ```b\"some text\" != \"some text\"```\n",
    "\n",
    "This may seem inconvenient at first because in order to parse the results from the stream as a string, you'll need to decode it like so `f.read().decode('utf-8')`, but it actually provides a common interface for many typical use-cases and flexibility for the more atypical use-cases. For example, let's say the configuration file that you're trying to access and use is a json file.  \n",
    "Notice below how the built-in json library will handle this binary file object just fine - similarly `json.loads` on the bytes literal will also work without having to explicitly decode it as utf-8. In other words, these will both have the same result:\n",
    "> `json.loads(f.read())` == `json.loads(f.read().decode('utf-8'))`\n",
    "\n",
    "```json\n",
    "// dag/configs/package.json\n",
    "{\n",
    "    \"author\": \"James Bond\",\n",
    "    \"version\": \"10.1.2\"\n",
    "}\n",
    "```\n",
    "\n",
    "To load and work with this file like an object in python, you can just do:\n",
    "```python\n",
    "import json\n",
    "with recipe_custom_config(\"package.json\", __file__) as f:\n",
    "    contents = json.load(f)\n",
    "print(contents)\n",
    "# {\"author\": \"James Bond\", \"version\": \"10.1.2\"}\n",
    "```"
   ]
  }
 ]
}