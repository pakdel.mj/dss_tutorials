from ...dag.dag import (builder_func, my_datacache_query_tag, create_sql,
                        calc_stats)

def test_builder_func():
    prop_dict = builder_func(None)
    assert len(prop_dict) == 3
    assert 'spark.dynamicAllocation.maxExecutors' in prop_dict
    assert 'spark.executor.memory' in prop_dict


def test_my_datacache_query_tag():
    name = 'a_query'
    tag = my_datacache_query_tag(name)
    assert tag.startswith(name)



def test_create_sql():
    class fake_df:
        columns = ['SPEND_FOOD1', 'SPEND_FOOD2',
                   'CHILDREN_DESC0', 'CHILDREN_DESC1', 'CHILDREN_DESC2']

    sql = create_sql(fake_df)
    assert sql == (' SPEND_FOOD1+SPEND_FOOD2 as spend_food, '
                   '1*CHILDREN_DESC1 + 2*CHILDREN_DESC2 as kids ')


def test_calc_stats():
    class mock_dataframe:
        def __init__(self):
            self.corr_called = 0
        def corr(self, col1, col2):
            self.corr_called += 1
            return 0.0

    my_fake_dataframe = mock_dataframe()

    class mock_metadata:
        def __init__(self):
            self.my_fake_dataframe = my_fake_dataframe
        def to_spark(self, _):
            return self.my_fake_dataframe

    class mock_xcom_puller:
        def __init__(self):
            self.my_fake_metadata = mock_metadata()
        def xcom_pull(self, **kwargs):
            return self.my_fake_metadata

    fake_context = {'ti': mock_xcom_puller()}

    # Call the function being tested
    calc_stats('spark', fake_context)

    # we went through the hierarchy and call the correlation function
    assert my_fake_dataframe.corr_called == 1
