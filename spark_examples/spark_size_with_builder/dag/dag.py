# imports from the python standard library
from datetime import datetime, timedelta
import logging

# imports of python package modules
from airflow import DAG

# imports of local modules
from dss_datacache_client import client
from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig
from dss_airflow_utils.operators import SparkOperator
from dss_airflow_utils.utils import get_config

# These are some default arguments that will be passed to each Airflow task.
# You can override them on a task basis if needed.
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'queue': { # queue Must be either an imagename (a string) or
               #  a string -> string dict
        "worker_type": "python-worker",  # here you can ask for specific
                                         # worker container
        "request_memory": ".5G", # how much computing resource a task needs
        "request_cpu": ".2"      # - you can override these on a task basis.
    }
}


# constants for task ids so we can have the compiler check that we spell
# them correctly everywhere they are used. Note that the tasks have names
# that explain what their function is to aid the reader of this dag.
PULL_DATA_TASK = "pull_data"
SAVE_EXTRACT_TASK = "save_extract"
CALC_STATS_TASK = "calc_stats"

# alternatively these functions could be placed in your dag/utils directory
#  and imported instead of being defined here.

def builder_func(unused_airflow_context):
    """ A builder function used in a DAG takes in the Airflow context object
        and returns a dictionary with spark properties as keys and the
        associated attribute as values.  Thus we can take information from
        the job request file and use it to decide in the program what
        values to assign.
    """
    prop_dict = {}
    # Ask for minimum resources first
    prop_dict['spark.dynamicAllocation.maxExecutors'] = '1'
    # increase requests when needed
    prop_dict['spark.executor.memory'] = '2G'
    # add more spark configuration properties as needed, e.g.
    # Set the number of latest rolling log files that are going to be
    # retained by the system to 1. Older log files will be deleted. Saves
    #  cloud space.
    prop_dict['spark.executor.logs.rolling.maxRetainedFiles'] = '1'
    # (see https://spark.apache.org/docs/latest/configuration.html for
    # other configs)
    return prop_dict

def my_datacache_query_tag(name):
    """ To prevent collisions in the datacache append the recipe name onto
        the name that is used as a key in the datacache.  The name should
        identify the data being stored.
    """
    return name + '_Developing-Spark-In-Jupyter-for-DSS'


def pull_data(spark, context):
    # get the name of the data_cache_name from the job_request file
    data_cache_name = get_config(context)["data_cache_name"]

    # query the data cache to find the data
    meta_data = client.get(query=data_cache_name,
                           tolerance={'type': 'range',
                                      'to': 'latest'})

    # read the parquet datafile from the datacache and have the spark session
    # convert it into a spark dataframe
    df = meta_data.to_spark(spark_session=spark)

    # in a real DAG you would do some work with this dataframe before putting
    # it in the datacache to pass to the next task.

    # put the dataframe into the data cache to pass to next task
    output_name = my_datacache_query_tag("sample_data")
    metadata = client.put(df, output_name, context=context)

    # pass the metadata for the cached dataframe to the next task
    return metadata


def create_sql(df):
    """ called from the save_extract function this function creates the SQL to
        sum the monthly spending in food stores and calculates the number
        of children in a household.  The data has been organized as a series
        of categorical variables so that children_desc_1 is 1 if the
        household has only one child, children_desc_2 is 1 if the household
        has two children, etc so that within a given row at most one of the
        children_desc_* fields is set to 1.  Food spending is in the dataset
        as spending per month so we want the annual sum.
    """
    # find the names of the columns we will put into the SQL
    food_cols = []
    kid_cols = []
    for colname in df.columns:
        if 'SPEND_FOOD' in colname:
            food_cols.append(colname)
        elif 'CHILDREN_DESC' in colname:
            kid_cols.append(colname)

    # sum up the monthly columns of spending in food stores
    annual_food = '+'.join(food_cols) + ' as spend_food'

    # create 1*CHILDREN_DESC_1 + 2*CHILDREN_DESC_2 ... to calc number
    # of children in household since values in these columns are 0 or 1
    # Since there are no columns with two or more digits at the end of
    # the name we can safely take the final digit of the name as the value
    # to multiply by.
    num_kids = ' + '.join([c[-1]+'*'+c for c in kid_cols[1:]]) + ' as kids'

    # create the extract statement for the columns desired
    sql = ' {spend_food}, {kids} '.format(
        spend_food=annual_food,
        kids=num_kids
    )

    return sql


def save_extract(spark, context):
    """ Create a extract of columns that are needed in the analysis that is
    to be done.  We get the dataframe from the datacache, create the SQL to
    pull the needed data and then put this new dataframe into the datacache.
    """
    # get the return value from the upstream task
    dataset_metadata = context['ti'].xcom_pull(task_ids=PULL_DATA_TASK)

    # create a spark dataframe from the datacache
    df = dataset_metadata.to_spark(spark)

    # set the dataframe to be viewed as an SQL table named Spending
    df.createOrReplaceTempView("Spending")

    # create a subset and write it to a new spark dataframe
    food_kids_df = spark.sql('select ' + create_sql(df) + ' from Spending')

    # pass the selected subset of data to next step through dataCacheClient
    output_name = "food_n_kids"
    metadata = client.put(food_kids_df, output_name, context=context)

    # A returned value is pushed into an xcom, as if we executed
    # context['ti'].xcom_push('return_value', metadata)
    return metadata


def calc_stats(spark, context):
    """ A task to calculate statistics and write the calculated value to
    the log.
    """
    # extract the dataframe prepared in upstream task
    extract_metadata = context['ti'].xcom_pull(task_ids=SAVE_EXTRACT_TASK)
    food_kids_df = extract_metadata.to_spark(spark)

    # calculate statistics
    pearson = food_kids_df.corr('spend_food', 'kids')

    # write the report, here just a number to the log
    logging.info("The correlation coefficient was {:.4f}".format(pearson))


# configure size of cluster
@dag_factory(DagConfig(SparkConfig(num_nodes=1,
                                   request_cpu_per_node=1,
                                   request_memory_gb_per_node=2)))
def create_dag():
    """
    Note that this example has three SparkOperators for illustration
    purposes, and data is passed from one to another in a straight line
    of execution.  Realistically this would be inefficient and you should
    really write this as a single python function called by a single
    SparkOperator, keep the data in memory and not write it out

    :return: The created dag for airflow to execute
    """
    with DAG(dag_id='Developing-Spark-In-Jupyter-for-DSS',
             default_args=default_args,
             schedule_interval=None) as dag:

        # spark driver configuration
        small_spark_queue = {
            "worker_type": "spark-worker",
            "request_memory": "2G",
            "request_cpu": "1"
        }

        pull_data_task = SparkOperator(
            func=pull_data,
            task_id=PULL_DATA_TASK,
            queue=small_spark_queue,
            # supply an executable function that returns the spark session
            # config as a dictionary.
            spark_confs=builder_func # spark session
            # scale overrides default spark_confs, explicit spark_confs
            # overrides scale
        )
        save_extract_task = SparkOperator(
            func=save_extract,
            task_id=SAVE_EXTRACT_TASK,
            queue=small_spark_queue,
            spark_confs=builder_func # spark session
        )
        calc_stats_task = SparkOperator(
            func=calc_stats,
            task_id=CALC_STATS_TASK,
            queue=small_spark_queue,
            spark_confs=builder_func # spark session
        )

        pull_data_task >> save_extract_task >> calc_stats_task

        return dag
