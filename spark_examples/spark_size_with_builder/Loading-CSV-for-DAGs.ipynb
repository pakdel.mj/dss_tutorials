{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial: Loading CSV Files For Use in DAGs\n",
    "\n",
    "****\n",
    "\n",
    "This tutorial covers:\n",
    "    \n",
    "    - Uploading a CSV file into the notebook\n",
    "    - Using the Datacache client to write it to the Datacache\n",
    "\n",
    "****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How to upload a file to your notebook\n",
    "\n",
    "After you have [logged into your notebook](./getting_started.ipynb) you can move file from your local system to the server that the notebook is running on.\n",
    "\n",
    "Usually at Nielsen your input data comes from a database and you pull it from the datacache directly using the appropriate connection hook.  This tutorial is for when you have CSV formatted data from outside Nielsen, or even from another group, that you wish to join with your data in an analysis.\n",
    "\n",
    "CSV is not a great data format but it is widely supported and often used as a 'lowest common denominator' data exchange format.  DSS does not support loading large datasets through the notebook, 100 Megabytes is the limit on what you can upload.  Uploading through a browser is inefficient even across an in-house network, and Jupyter needs to cache what you upload if you are going to work with it and CSV data is inefficent for reading.\n",
    "\n",
    "For these reasons we support\n",
    "- uploading only moderate size data sets\n",
    "- storing the data into the Datacache "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Uploading\n",
    "\n",
    "Go the to the file directory view tab of the Jupyter Notebook and navigate to the directory that you want the file to be placed in.  You should choose a directory under the \"work\" folder since only files and directories under \"work\" are retained when the notebook server goes down for maintenance.\n",
    "\n",
    "To create a directory within work, click the \"New\" button in the upper right of the page, and then click \"Folder\".  This pops up a dialog box where you put in the name for the file you are uploading.  The directory is then created and the empty directory is created with the name `Untitled Folder`.  Click on the folder's line (not the name, but beside it) and buttons appear at the top of the file list with actions you can perform.  One of these is \"Rename\".  Click on \"Rename\" and a dialog pops up and you can input a better directory name.  After you enter the new name and click on \"OK\" on the dialog box, you see the new directory with the notice \"Notebook list empty\" where files usually appear.\n",
    "\n",
    "To upload the file click on the the \"Upload\" button on the upper right and a file chooser dialog will appear.  Navigate to the file you want to upload and choose it.  The notebook will then refresh and the top line of the file list will be, on the left, a text input line populated with the name of the file you are uploading.  On the right will be a button marked \"Upload\".  If you want the name of the file to be different after it is uploaded change the name in the text input box.  To initiate the actual upload you click the \"Upload\" button.  The space between the button and the file name is used to show a progress bar.  When the upload is complete the file list refreshes and your file is not in it spot in directory listing.  If you upload to an existing directory that already has files in it the file will now appear in the list down where its name would put it in the alphabetical listing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Storing the data in the datacache\n",
    "\n",
    "Now we have the data uploaded to the server and are ready to load it into the datacache.  From the datacache we will be able to pull the data from the datacache to query it, join it with other data, etc. from an Airflow DAG.\n",
    "\n",
    "First let's be sure the data is where we expect it to be.  Your filename will be different.  <font style=\"background:yellow\">Modify the code cell to point to your uploaded CSV file.</font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-rw-r--r-- 1 notebook notebook 2.2M Feb 27 16:32 /home/notebook/example/tutorial/data/faked_input.csv\r\n"
     ]
    }
   ],
   "source": [
    "!ls -lh /home/notebook/example/tutorial/data/faked_input.csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To read the data into an in-memory data structure we use the pandas library.  <font style=\"background:yellow\">Modify the code cell to point to your uploaded CSV file and the parameters to match its format.</font>  If the delimiter used is not a vertical bar, change the third line below.  If the delimiter is a comma you can remove the line since comma is the default.  The `header` parameter tells pandas which line the header, defining the column names is found in. \"0\" is the first line.  The default is not header is present and you will have to refer to the columns by their numeric position."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "pd_df = pd.read_csv('/home/notebook/example/tutorial/data/faked_input.csv',\n",
    "                    delimiter=\"|\",\n",
    "                    header=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we use DSS's datacache client to store the data into the data lake.  See the [datacache tutorial]() for further information on how to use the datacache. <font style=\"background:yellow\">Change the query string to the name you want to use when pulling the data.</font>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this command loads our data file into the datacache\n",
    "from dss_datacache_client import client\n",
    "\n",
    "metadata = client.put(pd_df,\n",
    "                      query='tutorial_data_faked_input_parquet', \n",
    "                      context=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the data is in the datacache we can reference it from the notebook or from a DAG with code like this:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "    from dss_datacache_client import client\n",
    "    # query the data cache to find the data\n",
    "    meta_data = client.get(query=data_cache_query_string,\n",
    "                           tolerance={'type': 'range',\n",
    "                                      'to': 'latest'})\n",
    "\n",
    "    # read the parquet datafile from the datacache and have the spark session\n",
    "    # convert it into a spark dataframe\n",
    "    df = meta_data.to_spark(spark_session=spark)\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
