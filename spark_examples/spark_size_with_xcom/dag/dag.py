import os
import logging
from datetime import datetime, timedelta
from dss_airflow_utils.spark import SparkOperator, SparkClusterStart
from dss_airflow_utils.dag_factory import dag_factory, SparkConfig, DagConfig
from dss_airflow_utils.workspace_utils import path_in_workspace
from dss_airflow_utils.utils import get_config
from dss_datacache_client import client
from airflow import DAG
from airflow.operators.python_operator import PythonOperator


# Import of the custom packages and modules must be relative!
from .lib.util import data_writer, structured_noise_gen

# These are some default arguments that will be passed to each tasks.
# You can override them on a task basis if needed.
default_args = {
    'owner': 'e.xample@example.com',
    'start_date': datetime(2018, 10, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'queue': {                           # Must be either a string (imagename) or a dictionary of string -> string
        "worker_type": "python-worker",  # here you can ask for specific worker container
        "request_memory": ".5G",         # and how much computing resource a task will need
        "request_cpu": ".2"              # don't worry, you can override this on a task basis.
    }
}

# The SparkConfig defines the cluster parameters that are used when a Spark
# cluster is created.  The num_nodes parameter is the number of machines to
# spread the work across.  The request_cpu_per_node is the number of
# processors within the node to assign.  The request_memory_gb_per_node is
# the amount of memory to reserve on each node.
#
# The memory request is usually a large number since spark is an in-memory
# processing model and you want to be able to hold all the intermediate
# results of your processing. Asking for too much memory can slow down
# execution of your DAG since Airflow will hold off scheduling a spark job
# until resources become available.  Consistently asking for too much
# memory will increase the expense of running a job.
#
# The number of node and number of CPUs can influence the speed and
# efficiency of a job.  There have to be sufficient cpus to get the benefit
# of spark's parallel operations.  However having too many CPUs working
# on the same node can cause memory pressures that prevent spark from
# running as efficiently as it can.  On the other hand having too many
# nodes results in excessive network traffic as the work is distributed
# to the workers and the results are then collected.
#
# If you ask for too many nodes, or too much memory or cpu, the request
# will be reduced to the maximum allowed. The default values are the
# minimums, 1 cpu on 1 node and 2 gigibytes of memory.  You definitely
# want to specify these and get something larger.
#
# In this example we will actually not be using the values in this SparkConfig
# because we will override them with an xcom push to tell the spark system
# what our final choices are before the spark cluster is created.

spark_config = SparkConfig(num_nodes=1,
                           request_cpu_per_node=1,
                           request_memory_gb_per_node=5)


def push_xcoms(*args, **context):
    """ By using the xcom communications method to pass data between tasks
        in a dag we can get the information we need to override the spark
        config values passed to the DagConfig.  In this example the "start"
        task calls this function which can then calculate how many, and what
        size, resources to ask for.
    """
    size = args[0]
    logging.info("Spark Config set for generation of {size} by {size}".format(
        size=size))
    # NOTE: values for pedagogic purposes only, they are not suggested
    # or even all tested.
    if size < 25:
        nodes, mem = (1, 2.5)
    elif size < 100:
        nodes, mem = (2, 5)
    elif size < 250:
        nodes, mem = (4, 6)
    elif size < 1000:
        nodes, mem = (8, 16)
    else:
        nodes, mem = (16, 24)
    logging.info("Requesting {} worker nodes with {} memory.".format(
        nodes, mem))
    # This sets the size information that will be used when a call to a
    # SparkOperator causes the DAG's spark cluster to be spun up.  The
    # key is special here, allowing the information to be found by the
    # SparkOperator.  Note that the value is the SparkConfig with the
    # to_dict() method applied to it.
    context['ti'].xcom_push(key=SparkClusterStart.spark_config_key,
                            value=SparkConfig(num_nodes=nodes,
                                              request_memory_gb_per_node=mem)
                            .to_dict())
    return size  # gets pushed as an xcom with key 'return_value' and the
    # task_id of the task that ran this function ('task_get_size' in this
    # example)


def spark_func(spark, context):
    """ For this example I get the size of the spark data frame that is going
        to be produced from the return value of the 'start' task.
    """
    # get the size of array to create from the return value of previous task
    # default key= value is the key used to put return values to an xcom
    size = context['ti'].xcom_pull(task_ids=['start'])[0]
    logging.info("Size received is {}".format(size))

    # generation of the noise dataframe done in a testable function
    structured_noise_gen(size, spark, client)


def write_data(spark, context):
    """ Save the data created in a previous step """
    # Get parameters passed from the job request file
    config = get_config(context)
    noise_file_name = config.get("noise_file_name")
    noise_file_type = config.get("noise_file_type")
    file_name = path_in_workspace('.'.join([noise_file_name,
                                            noise_file_type]))
    # data is written out in testable function. Spark requires the URI form
    # of the name so I preprend 'file://'
    retval = data_writer('file://' + file_name, noise_file_type, client, spark)

    # This shows that the file was written and could be read in this
    # or a later task or DAG.
    try:
        logging.info("File written successfully")
        logging.info('File info: {}'.format(os.stat(file_name)))
    except OSError as e:
        logging.error("Could not stat the file: {}".format(repr(e)))
    return retval

# Size of Spark Driver.  The spark driver is a spark node that is spun up to
# handle non-parallel parts of the application.  It typically will have
# just one CPU. The amount of work it does grows with the size of the cluster.
spark_driver_config = {
    "worker_type": "spark-worker",
    "request_memory": "2G",
    "request_cpu": "1"
}


@dag_factory(DagConfig(spark_config))
def create_dag():
    with DAG(dag_id='spark_size_with_xcom_dag',
             default_args=default_args,
             schedule_interval=None) as dag:

        task_get_size = PythonOperator(
            task_id='start',
            provide_context=True,
            python_callable=push_xcoms,
            op_args=[100]  # this is the requested size for the array
        )

        task_create_array = SparkOperator(
            func=spark_func,
            task_id='create_random_array',
            queue=spark_driver_config
        )

        task_save_data = SparkOperator(
            func=write_data,
            task_id='write_updated_data',
            queue=spark_driver_config
        )

        # Define dependencies between operators

        task_get_size >> task_create_array >> task_save_data

        return dag  # Do not change this
