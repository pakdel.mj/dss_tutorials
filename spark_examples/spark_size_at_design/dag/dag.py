import os
import logging
from datetime import datetime, timedelta
from airflow import DAG
from dss_airflow_utils.spark import SparkOperator
from dss_airflow_utils.dag_factory import dag_factory, SparkConfig, DagConfig
from dss_airflow_utils.workspace_utils import path_in_workspace
from dss_airflow_utils.utils import get_config
from dss_datacache_client import client


# Import of the custom packages and modules must be relative!
from .lib.util import data_writer, structured_noise_gen

# These are some default arguments that will be passed to each tasks.
# You can override them on a task basis if needed.
default_args = {
    'owner': 'e.xample@example.com',
    'start_date': datetime(2018, 10, 1), # Sometime in the past
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'queue': {                           # Must be either a string (imagename) or a dictionary of string -> string
        "worker_type": "python-worker",  # here you can ask for specific worker container
        "request_memory": ".5G",         # and how much computing resource a task will need
        "request_cpu": ".2"              # don't worry, you can override this on a task basis.
    }
}

# The SparkConfig defines the cluster parameters that are used when a Spark
# cluster is created.  The num_nodes parameter is the number of machines to
# spread the work across.  The request_cpu_per_node is the number of
# processors within the node to assign.  The request_memory_gb_per_node is
# the amount of memory to reserve on each node.
#
# The memory request is usually a large number since spark is an in-memory
# processing model and you want to be able to hold all the intermediate
# results of your processing. Asking for too much memory can slow down
# execution of your DAG since Airflow will hold off scheduling a spark job
# until resources become available.  Consistently asking for too much
# memory will increase the expense of running a job.
#
# The number of node and number of CPUs can influence the speed and
# efficiency of a job.  There have to be sufficient cpus to get the benefit
# of spark's parallel operations.  However having too many CPUs working
# on the same node can cause memory pressures that prevent spark from
# running as efficiently as it can.  On the other hand having too many
# nodes results in excessive network traffic as the work is distributed
# to the workers and the results are then collected.
#
# If you ask for too many nodes, or too much memory or cpu, the request
# will be reduced to the maximum allowed. The default values are the
# minimums, 1 cpu on 1 node and 2 gigibytes of memory.  You definitely
# want to specify these to be something larger.
spark_config = SparkConfig(num_nodes=1,
                           request_cpu_per_node=1,
                           request_memory_gb_per_node='2G')


def spark_func(spark, **unused_context):
    """ For this example I get the size of the spark data frame that is going
        to be produced from the return value of the 'start' task.
    """
    # In this example the size is set here in the code
    size = 1024

    # generation of the noise dataframe done in a testable function
    structured_noise_gen(size, spark, client)


def write_data(spark, context):
    """ Save the data created in a previous step """
    # Get parameters passed from the job request file
    config = get_config(context)
    noise_file_name = config.get("noise_file_name")
    noise_file_type = config.get("noise_file_type")
    file_name = path_in_workspace('.'.join([noise_file_name,
                                            noise_file_type]))
    # data is written out in testable function. Spark requires the URI form
    # of the name so I preprend 'file://'
    retval = data_writer('file://' + file_name, noise_file_type, client, spark)

    # This shows that the file was written and could be read in this
    # or a later task or DAG.
    try:
        logging.info("File written successfully")
        logging.info('File info: {}'.format(os.stat(file_name)))
    except OSError as e:
        logging.error("Could not stat the file: {}".format(repr(e)))
    return retval


# Size of Spark Driver.  The spark driver is a spark node that is spun up to
# handle non-parallel parts of the application.  It typically will have
# just one CPU. The amount of work it does grows with the size of the cluster.
spark_driver_config = {
    "worker_type": "spark-worker",
    "request_memory": "2G",
    "request_cpu": "1"
}


@dag_factory(DagConfig(spark_config))
def create_dag():
    with DAG(dag_id='spark_size_at_design_dag',
             default_args=default_args,
             schedule_interval=None) as dag:

        creation_task = SparkOperator(
            func=spark_func,
            task_id='create_random_array',
            queue=spark_driver_config
        )

        storage_task = SparkOperator(
            func=write_data,
            task_id='write_updated_data',
            queue=spark_driver_config
        )

        # Define dependencies between operators

        creation_task >> storage_task

        return dag  # Do not change this
