"""
Here is defined any complex computation logic to support dag execution.
You can also create more modules under the "dag" directory.
Just remember to use *relative* imports.
"""

import random
import logging


class RangeException(Exception):
    """ Exception thrown by increasing_random function if the low or high
        parameters that do not follow rules for thos parameters. """


def increasing_rand(avg=0, low=0, high=0, size=1):
    """
    Create a list of random numbers with a range that varies
    across the list. Low index elements will have little
    variation, the highest index elements may range between
    low and high values.

    parameters:
    avg: number: average value across the whole list.  Will always equal
        the value of the first element
    low: number: the lowest allowable value at greatest variablity.
        Must be less than or equal to avg.
    high: number: the highest allowable value at greatest variability.
        Must be greater than or equal to avg.
    size: int: size of list to be returned. Must be greater than 0.

    returns:
    list of values of the same type as avg

    raises:
    RangeException if any of the parameters violate the preconditions.
    """
    if size <= 0 or low > avg or avg > high:
        raise RangeException

    retval = list([avg] * size)
    incr_up = (high - avg) / size
    incr_dn = (avg - low) / size
    up = down = 0
    for elem in retval:
        elem = float(elem + random.triangular(down, up, 0))
        up += incr_up
        down -= incr_dn
    return retval


def structured_noise_gen(size, spark, client):
    logging.info("Size received is {}".format(size))

    # we map the spark range function to specify a loop in spark worker
    # space and ask that increasing_range be called to create a row for
    # each element of the range.  Finally make it into a spark dataframe.
    noise_dataframe = (
        spark.sparkContext.range(size).map(lambda r:
                                           increasing_rand(avg=128.0,
                                                           low=0.0,
                                                           high=255.0,
                                                           size=size)).toDF())

    # now we put the spark DataFrame into DSS's datacache for use in
    # following tasks.  The 'query' parameter is a key that will be used
    # to find the data in later tasks.
    client.put(data=noise_dataframe, query='noise_dataframe')


def data_writer(file_name, noise_file_type, client, spark):
    # Search the data cache, pulling back the last dataset that was written
    cache_tolerance = {'type': 'range', 'to': 'latest'}

    # In this case, we supply a unique criteria to the datacache and a
    # tolerance parameter is supplied saying give me the latest in the cache.
    metadata = client.get(query='noise_dataframe',
                          tolerance=cache_tolerance)
    # Change the spark dataframe into a pandas dataframe so we can use
    # fast_parquet to write the data out (takes a pandas dataframe).
    data_frame = metadata.to_spark(spark)


    logging.info("write_data will write to {}".format(file_name))

    if noise_file_type == 'csv':
        # CSV (comma/character separated values) is a format that is used in
        # a lot of programs and is thus useful for moving data.  It has a lot
        # of drawbacks: it is very space in-efficient; names of the data rows
        # are either not kept or becomes part of the data (and the reader has
        # to know if it is there); processing the file requires fulling
        # reading the file (usually into memory); re-reading requires
        # additional programming; parallel reads from multiple workers is not
        # supported.  If possible choose parquet format for storing your data.
        data_frame.write.csv(path=file_name)
    elif noise_file_type == '.txt':
        data_frame.write.format('json').save(file_name)
    elif noise_file_type == 'parquet':
        data_frame.write.format('parquet').save(file_name)
    return file_name
