from ...dag.lib.util import increasing_rand, structured_noise_gen, data_writer


def test_dataset_generation():
    size = 10
    avg = 128.0
    low = 0.0
    high = 255.0

    # create the rows of the dataset.
    noise_dataset = [increasing_rand(avg=avg, low=low, high=high, size=size)
                     for _ in range(size)]

    # check that our list of Rows of lists fit within expected ranges
    for i in range(size):
        assert noise_dataset[i][0] == avg
        assert low < noise_dataset[i][size - 1] < high


def test_structured_noise_gen():

    # first, create mock classes to stand in for real classes called in func
    class mock_df:
        def __init__(self):
            self.df_name = 'mock_dataframe'

        def toDF(self):
            return self.df_name

    class mock_sparkContext:
        def __init__(self):
            self.rtn_df = mock_df()

        def range(self, unused_size):
            return self

        def map(self, unused_func):
            return self.rtn_df

    class mock_spark:
        def __init__(self):
            self.mocked_sparkContext = mock_sparkContext()

        @property
        def sparkContext(self):
            return self.mocked_sparkContext

    class mock_client:
        def __init__(self):
            self.mock_dataset = None
            self.mock_query = ''
            self.put_called_cnt = 0

        def put(self, **kwargs):
            self.put_called_cnt += 1
            self.mock_dataset = kwargs['data']
            self.mock_query = kwargs['query']

    # initialize variables to be passed
    size = 20
    mock_spark_instance = mock_spark()
    mock_client_instance = mock_client()

    # do the call
    structured_noise_gen(size, mock_spark_instance, mock_client_instance)

    # make sure that the internal values are as expected
    assert mock_client_instance.mock_dataset == 'mock_dataframe'
    assert mock_client_instance.put_called_cnt == 1
    assert mock_client_instance.mock_query == 'noise_dataframe'


def test_data_writer():

    # first, create mock classes to stand in for real classes called in func
    class mock_df:
        def __init__(self):
            self.file_type = None
            self.save_cnt = 0

        @property
        def write(self):
            return self

        def format(self, file_type):
            self.file_type = file_type
            return self

        def save(self, the_file_name):
            self.save_cnt += 1
            return the_file_name

    the_mock_df = mock_df()

    class mock_metadata:
        def __init__(self):
            self.returned_df = the_mock_df

        def to_spark(self, unused_spark):
            return self.returned_df

    class mock_client:
        def __init__(self):
            self.get_called_cnt = 0

        def get(self, **unused_kwargs):
            self.get_called_cnt += 1
            return mock_metadata()

    # initialize variables to be passed
    file_name = 'fake_name.parquet'
    noise_file_type = 'parquet'
    client = mock_client()
    spark = None

    # do the call
    returned_file_name = data_writer(file_name,
                                     noise_file_type,
                                     client,
                                     spark)

    # make sure that the internal values are as expected
    assert returned_file_name == file_name
    assert the_mock_df.file_type == noise_file_type
    assert client.get_called_cnt == 1
    assert the_mock_df.save_cnt == 1
