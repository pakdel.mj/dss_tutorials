{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Debugging Recipes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "This tutorial covers:\n",
    "\n",
    "- Error messages as the primary source of information in debugging\n",
    "- Error messages from the recipe client\n",
    "- Error messages from an Airflow recipe run and how to find them\n",
    "- Error messages from the Spark History server\n",
    "- Error messages from a Spark cluster's application logs\n",
    "- Python and logging in Python\n",
    "- Common Python problem - indentation\n",
    "\n",
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The purpose of this document is to introduce the tools available to recipe\n",
    "developers for resolving failures and other problems. It will also highlight\n",
    "some common pitfalls in recipe development. There is a section specific to\n",
    "wrtiting effective Python code.\n",
    "\n",
    "If you learn only one thing from this document: The DSS and the Recipe\n",
    "Client are designed to generate useful error messages in the event of failure,\n",
    "which provide a concise explanation of what went wrong and point you to where in\n",
    "your code where the problem happened. These error messages tend to be the most\n",
    "direct and effective means of debugging your recipe, and they will get logged\n",
    "and be accessible through the Airflow UI.\n",
    "\n",
    "## Recipe Client\n",
    "\n",
    "The recipe client is the command line tool for submitting recipes to the DSS\n",
    "for scheduling and execution. Before you can execute your recipe, you\n",
    "will need to either `taste` or `publish` it using the recipe client.\n",
    "\n",
    "To get the full list of commands for the recipe client, use ```recipe\n",
    "--help```. Similar functionality is available for each recipe client command;\n",
    "for example, ```recipe taste --help``` or ```recipe publish --help``` will\n",
    "provide full usage information for the `taste` or `publish` command,\n",
    "respectively. After executing a recipe client command, pay attention to its\n",
    "output on the terminal. This output will tell you whether the command succeeded\n",
    "and offer an explanation if the command fails.\n",
    "\n",
    "### Common `ERROR` messages and how to fix them\n",
    "\n",
    "**`Failed to load recipe with status code 409`**\n",
    "\n",
    "The most likely cause is a syntax error in your recipe code or business logic.\n",
    "If this is the case, the next line will provide an error message, a file name,\n",
    "and a line number to investigate. \n",
    "\n",
    "**`Failed to send job request to the recipe client. Got error: ('Connection\n",
    "aborted.', RemoteDisconnected('Remote end closed connection without\n",
    "response',))`**\n",
    "\n",
    "Be sure that the `recipe_id`s agree between `metadata.yaml` and your job request\n",
    "file.\n",
    "\n",
    "## Airflow UI (Application Logs)\n",
    "\n",
    "The Airflow UI provides a wealth of information about recipe executions. From the Airflow UI, you can search for your recipe by its `recipe_id`\n",
    "and access its execution history. Note that Airflow calls it a DAG (short for\n",
    "Directed Acyclic Graph) instead of a recipe.\n",
    "\n",
    "![Airflow DAG Graph View](images/airflow-dag-screenshot.jpg \"Airflow\n",
    "DAG Graph View\")\n",
    "\n",
    "The DAG graph view of your recipe displays the tasks that make up your recipe,\n",
    "their dependencies, and the status of each task. For recipes with multiple\n",
    "executions, you can also select which to investigate by selecting from the `Run`\n",
    "dropdown box.\n",
    "\n",
    "Left clicking on a task in the graph brings up a dialogue box that allows you to\n",
    "interact with that task. Clicking on \"View Log\" will provide access to that\n",
    "task's application log. In addition to logging stack traces from uncaught\n",
    "exceptions and information associated with the execution of the task, any\n",
    "logging you did in your business logic will show up here.\n",
    "\n",
    "## Spark History Server\n",
    "\n",
    "Though error messages and logging from Spark jobs are accessible through the\n",
    "Airflow UI, tuning the performance of Spark jobs often requires more nuanced\n",
    "information about how work is being distributed through the cluster. \n",
    "\n",
    "To surface this information, a Spark History Server is available for all Spark\n",
    "jobs executed through DSS. You can access the server via `/spark-logs/` on the \n",
    "same domain where you access the other DSS apps like Jupyterhub and Airflow. \n",
    "From the Spark History Server, you can search for your recipe by its recipe id\n",
    "and select the most recent execution. From there, you can look at Spark SQL \n",
    "query plans and Spark event timelines to diagnose the performance bottlenecks \n",
    "of your job.\n",
    "\n",
    "## Spark Application Logs\n",
    "\n",
    "A Spark cluster's application logs can provide insight into failed tasks. \n",
    "These logs can be accessed through the Spark master's UI Web Console while the\n",
    "cluster is up. The logs are still available after the Spark cluster is spun \n",
    "down and can be found in the recipe's workspace which is stored in the shared \n",
    "drive (/shared). Keep in mind that all of the cluster's logs are stored in the\n",
    "parent recipe's workspace.  \n",
    "\n",
    "## Python\n",
    "\n",
    "Python is an interpretted, object-oriented, general purpose programming\n",
    "language, which emphasizes code readability. It also has a convenient foreign\n",
    "function interface, so it's easy to write performant numerical routines in a\n",
    "compiled language (usually C/C++/Fortran/Java/Scala) and call them from Python.\n",
    "As a result, there are many good tools in Python for data manipulation and\n",
    "analysis.\n",
    "\n",
    "### Python Logging Module\n",
    "\n",
    "There's an easy to use `logging` module you might want to make use of. A example\n",
    "code snippet follows to demonstrate its use.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "def log_whether_argument(argument=None):\n",
    "    \"\"\"Log whether the optional argument is provided.\"\"\"\n",
    "    if argument is None:\n",
    "        logging.info('there was no argument')\n",
    "    else:\n",
    "        logging.info('the argument was {}'.format(argument))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whenever `log_whether_argument` is invoked, whether it was called with or\n",
    "without its optional argument will be logged. If invoked as part of a recipe,\n",
    "this logged information will be accessible through the Airflow UI. Information\n",
    "you log may help you to interpret the behavior of your recipe.\n",
    "\n",
    "### Python Indentation\n",
    "\n",
    "One of the more common pitfalls for developers coming to Python from other\n",
    "languages is its use of indentation. While many languages have conventions\n",
    "about using indentation to delineate the hierarchy of blocks of code, it is\n",
    "mandatory syntax in Python, and incorrect indentation will cause the interpreter\n",
    "to throw an `IndentationError` rather than executing your code.\n",
    "\n",
    "Notice that in the code snippet above, there are three levels of indentation.\n",
    "They represent code that is executed when the module is initialized, code that\n",
    "is executed when we invoke the function `log_whether_argument`, and code whose\n",
    "execution depends on conditional branching. Also notice that each line that ends\n",
    "in a colon starts a new indented block.\n",
    "\n",
    "Each indentation is 4 characters wide. Spaces are preferred over tabs, and most\n",
    "editors (including Jupyter) will automatically convert tabs to 4 spaces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****\n",
    "\n",
    "Next we look at a [DSS Hands On Tutorial](./dss-hands-on-tutorial-v2.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}