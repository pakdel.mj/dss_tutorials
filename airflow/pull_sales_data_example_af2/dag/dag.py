# pull_ap_data_example_af2/dag/dag.py

# Example to pull data from sales facts database and make use of data cache.

# Running primarily in Python 3

#################### PRELIMINARIES #################################


# basic packages
from datetime import datetime, timedelta
import logging
import pandas as pd


# Airflow-related packages
from airflow import DAG
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.utils.trigger_rule import TriggerRule

from dss_airflow_utils.operators import SparkOperator
from dss_airflow_utils.dag_factory import dag_factory
from dss_airflow_utils.workspace_utils import path_in_workspace

# Import user-defined functions - import of the custom packages and modules must be relative!
from .lib.pull_sales_data_example_functions import read_data, summarize_data



#################### CONSTANTS #####################################

NBR_RETRIES = 0

# These are some default arguments that will be passed to each tasks.
# You can override them (or add to them) on a task basis if needed.

PYTHON_3_QUEUE = {
    "worker_type" : "python3-worker",
    "request_memory" : "16G",
    "request_cpu": "1"
}
DEFAULT_ARGS = {
    'owner': 'airflow',
    'start_date': datetime(2019, 4, 5),
    'retries': NBR_RETRIES,
    'retry_delay': timedelta(seconds=10),
    'queue': PYTHON_3_QUEUE
}


PYTHON_2_QUEUE = {
    "worker_type" : "python-worker",
    "request_memory" : "16G",
    "request_cpu": "1"
}


#        "worker_type": "python-worker",     # here you can ask for specific worker container
#        "request_memory": "16G",            # and how much maximum computing resource a task will need
#        "request_cpu": "4"                  # don't worry, it starts small and will retry with more if it runs
#                                            # out of memory or CPU



# Size of Spark Driver
MY_SPARK_QUEUE = {
    "worker_type": "spark3-worker",
    "request_memory": "16G",
    "request_cpu": "1",
    "env": {
        "PYSPARK_PYTHON": "/usr/bin/python3"
    }
}


# define how we wish to pull objects from the data cache
DC_TOLERANCE = {"type": "range", "to": "latest_cached"}
# another way:  DC_TOLERANCE={'type': 'version', 'value': 1}


RETURN_VALUE_TASK_ID = 'return_value'
DO_WORK_TASK_ID = 'summarize_data'
TAKE_IT_EASY_TASK_ID = 'take_it_easy'



#################### FUNCTIONS #####################################
# This is the actual code you want to run

# If not defined here, can be pulled in from dag/lib directory
# (see "Import user-defined functions", above)


def return_value(**context):

    # read in number of records of data we pulled
    my_file_loc = path_in_workspace("n_rows.parquet")
    n_rows = pd.read_parquet(my_file_loc).iloc[0, 0]

    # set our return value:  1 means continue with task(s)
    if n_rows > 0:
        rv = 1
    else:
        rv = 0

    return rv


def have_data_or_not(ti, **context):
    upstream_returned_value = ti.xcom_pull(task_ids=RETURN_VALUE_TASK_ID)
    logging.info('upstream_return_value is {}'.format(upstream_returned_value))

    return DO_WORK_TASK_ID if upstream_returned_value else TAKE_IT_EASY_TASK_ID


def take_it_easy():
    logging.info('alternate branch')


def done():
    logging.info("===============================================")
    logging.info('All finished!')
    logging.info("===============================================")



# This decorator is required, so do not touch it!
# Don't put anything between the decorator and def keyword
@dag_factory
def create_dag():
    with DAG(dag_id='pull_sales_data_example_af2',
             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:


        #################### DEFINE TASKS ##################################

        read_in_data = SparkOperator(
            task_id='read_data',
            func=read_data,
            queue=MY_SPARK_QUEUE,
            metastore_conn_id='metastore_ndx_uat_adls'
        )

        return_value_task = PythonOperator(
            task_id=RETURN_VALUE_TASK_ID,
            python_callable=return_value,
            queue=PYTHON_2_QUEUE,
            provide_context=True
        )       # we set this to be Python 2 so XCom window displays properly in Airflow UI
                # (either Py2 or Py3 will work properly internally)

        have_data_or_not_task = BranchPythonOperator(
            task_id='have_data_or_not',
            python_callable=have_data_or_not,
            queue=PYTHON_2_QUEUE,
            provide_context=True
        )       # this version of Python must match that of return_value_task,
                #     as this step reads the XCom

        summarize_data_task = PythonOperator(
            task_id=DO_WORK_TASK_ID,
            python_callable=summarize_data,
            provide_context=True
        )

        take_it_easy_task = PythonOperator(
            task_id=TAKE_IT_EASY_TASK_ID,
            python_callable=take_it_easy
        )

        done_task = PythonOperator(
            task_id='All_Done',
            python_callable=done,
            trigger_rule=TriggerRule.ONE_SUCCESS
        )



        #################### RUN TASKS #####################################

        read_in_data >> return_value_task >> have_data_or_not_task
        have_data_or_not_task >> [summarize_data_task, take_it_easy_task]
        [summarize_data_task, take_it_easy_task] >> done_task

        # note:  spark session will stop automatically


        return dag  # Do not change this
