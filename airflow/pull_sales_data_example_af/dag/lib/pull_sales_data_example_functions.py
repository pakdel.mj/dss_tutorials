# pull_sales_data_example_functions.py

"""
Here you can define any complex computation logic to support dag execution.
You can also create more modules under the "dag" directory.
Just remember to use *relative* imports.
"""


#-------------------------------------------------------------------#
#   Pull in data as per user-specified parameters, aggregate,
#   and write out to the data cache
#-------------------------------------------------------------------#

def read_data(spark, context):

    import logging

    from dss_airflow_utils.workspace_utils import get_workspace
    from dss_airflow_utils.utils import get_config  # need this to read in user parameters from job_request
    config = get_config(context)
    #from dss_airflow_utils.utils import from_config  # to read in user parameters into an Airflow task def'n.

    # Datacache client (to write/read/search the data cache)
    from dss_datacache_client import client


    #-----------------------------------------------------------#
    #   Read in user-specified parameters
    #-----------------------------------------------------------#
    facts = config.get('facts_db')
    category = config.get('category')
    dma_code = config.get('dma_code')
    week = config.get('week')
    dc_query_tag = config.get('dc_query_tag')

    logging.info("===============================================")
    logging.info("We will pull data for the {0} category in DMA #{1}".format(category, dma_code))
    logging.info("from the {0} database.".format(facts))
    logging.info("===============================================")


    #-----------------------------------------------------------#
    #   Set up the query
    #-----------------------------------------------------------#

    # first pass, identify all data we want
    l = """
        select str.channel as Channel,
               str.store_name as Banner,
               str.pod_id,
               str.store_number,
               fct.vol,
               fct.units,
               fct.dol,
               prdc.product_key
        from {0}.fct
        inner join {0}.prdc
           on prdc.product_key = fct.product_key
        inner join {0}.str
           on str.pod_id = fct.pod_id
        where prdc.pg_category like '%{1}%'
          and str.dma_code = '{2}'
          and fct.period_description_short like '%{3}%'
        """.format(facts, category, dma_code, week)

    logging.info("===============================================")
    logging.info("Pulling all records - our query is:")
    logging.info(l)
    logging.info("===============================================")

    my_data_step1 = spark.sql(l)
    my_data_step1.createOrReplaceTempView('step1')

    # second pass, compute aggregations of interest
    my_data_spk = spark.sql("""
        select Channel,
               Banner,
               count(distinct pod_id) as Nbr_Stores,
               sum(DOL) as Dollars,
               sum(vol) as Volume,
               sum(units) as Units,
               count(*) as Nbr_Recs
        from step1
        group by Channel, Banner
        order by Channel, Banner
        """)
    my_data_spk.createOrReplaceTempView('my_data')

    # pull in name of DMA, too, for labeling
    dma_name = spark.sql("""
        select distinct dma_name
        from {0}.str
        where dma_code = '{1}'""".format(facts, dma_code)).toPandas()


    #-----------------------------------------------------------#
    #   Execute the query and save results to data cache
    #-----------------------------------------------------------#

    # did we pull any data?  If so, write to the data cache. If not, write a message.
    n_rows = spark.sql("select count(*) as n_rows from step1").toPandas()

    # n_rows = pd.DataFrame({'n_rows':[0]}) # a fake-up, for testing branching

    # write out n_rows to workspace so we can retrieve it later
    n_rows.to_parquet("{}/n_rows.parquet".format(get_workspace()))


    if n_rows.iloc[0, 0] > 0:
        
        # count number of rows we are writing out so we can put message in log
        n_rows_out = spark.sql("select count(*) as n_rows from my_data").toPandas()
        
        # execute the query and write the data to the data cache
        client.put(my_data_spk, query=dc_query_tag, context=context)

        logging.info("===============================================")
        logging.info("Data saved to data cache with query string '{}'.".format(dc_query_tag))
        logging.info("We pulled {0} rows of data, and".format(str(n_rows.iloc[0, 0])))
        logging.info("we are outputting {0} rows of channel-banner summary data."\
                     .format(str(n_rows_out.iloc[0, 0])))
        logging.info("===============================================")

        # write out dma_name to workspace so we can use it in another task
        dma_name.to_parquet("{}/dma_name.parquet".format(get_workspace()))

    else:

        logging.info("===============================================")
        logging.info("Query returned no records!  Check your parameters.")
        logging.info("Nothing saved to data cache.")
        logging.info("===============================================")


#-------------------------------------------------------------------#
#   Read in saved data, perform further aggregations, print to log
#-------------------------------------------------------------------#

def summarize_data(**context):

    import logging

    import pandas as pd

    from dss_airflow_utils.workspace_utils import path_in_workspace
    from dss_airflow_utils.utils import get_config  # need this to read in user parameters from job_request
    config = get_config(context)
    #from dss_airflow_utils.utils import from_config  # to read in user parameters into an Airflow task def'n.

    # Datacache client (to write/read/search the data cache)
    from dss_datacache_client import client


    #-----------------------------------------------------------#
    #   Read in user-specified parameters
    #-----------------------------------------------------------#
    category = config.get('category')
    dma_code = config.get('dma_code')
    dc_query_tag = config.get('dc_query_tag')

    # pull meta data
    my_meta = client.get(query=dc_query_tag, tolerance={"type":"range", "to":"latest_cached"})
    #logging.info("===============================================")
    #logging.info("\n\nMetadata for data cache object with query tag ")
    #logging.info("{0} are:".format(dc_query_tag))
    #logging.info(my_meta)
    #logging.info("===============================================")


    # Because our data pull was relatively small, we are going to pull it into Pandas
    # If your saved data were very large, .to_spark() would be more appropriate than .to_pandas()

    sample_data = my_meta.to_pandas()

    #logging.info("===============================================")
    #logging.info("\nSample Data\n")
    #logging.info('Shape:', sample_data.shape, '\n\nData Types')
    #logging.info(sample_data.dtypes)
    #logging.info('\nSample Records')
    #logging.info(sample_data.head(3))
    #logging.info("===============================================")


    # summarize by channel - strip down data first
    sample_data_subset = sample_data.copy()
    sample_data_subset.drop(['Volume'], axis=1, inplace=True)
    sample_data_subset['Units'] = sample_data_subset['Units'].astype(int)
    sample_data_subset = sample_data_subset.loc[:, ['Channel', 'Nbr_Stores', 'Dollars', 'Units', 'Nbr_Recs']]

    channel_summary = sample_data_subset.groupby(['Channel']).agg(['sum']) # compute some descriptive stats
    channel_summary.columns = [col[0] for col in channel_summary.columns] # 2-level index --> 1-level
    channel_summary['Avg_Price'] = channel_summary['Dollars']/channel_summary['Units'] # compute

    # format the float vars w/2 decimal places
    pd.options.display.float_format = '${:,.2f}'.format

    # read in DMA name, so we can use it on output
    my_file_loc = path_in_workspace("dma_name.parquet")
    dma_name = pd.read_parquet(my_file_loc)

    logging.info("===============================================")
    logging.info("Summary of Sales in {0}".format(category))
    logging.info("for DMA {0}: {1}".format(dma_code, dma_name.iloc[0, 0]))
    logging.info(channel_summary)
    logging.info("===============================================")
