# pull_ap_data_example_af/dag/dag.py

# Example to pull data from sales facts database and make use of data cache.

# Running primarily in Python 3

#################### PRELIMINARIES #################################


# basic packages
from datetime import datetime, timedelta

# Airflow-related packages
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from dss_airflow_utils.operators import SparkOperator
from dss_airflow_utils.dag_factory import dag_factory

# Import user-defined functions - import of the custom packages and modules must be relative!
from .lib.pull_sales_data_example_functions import read_data, summarize_data



#################### CONSTANTS #####################################

NBR_RETRIES = 0

# These are some default arguments that will be passed to each tasks.
# You can override them (or add to them) on a task basis if needed.

PYTHON_3_QUEUE = {
    "worker_type" : "python3-worker",
    "request_memory" : "16G",
    "request_cpu": "1"
}
DEFAULT_ARGS = {
    'owner': 'airflow',
    'start_date': datetime(2019, 4, 5),
    'retries': NBR_RETRIES,
    'retry_delay': timedelta(seconds=10),
    'queue': PYTHON_3_QUEUE
}


PYTHON_2_QUEUE = {
    "worker_type" : "python-worker",
    "request_memory" : "16G",
    "request_cpu": "1"
}


#        "worker_type": "python-worker",     # here you can ask for specific worker container
#        "request_memory": "16G",            # and how much maximum computing resource a task will need
#        "request_cpu": "4"                  # don't worry, it starts small and will retry with more if it runs
#                                            # out of memory or CPU



# Size of Spark Driver
MY_SPARK_QUEUE = {
    "worker_type": "spark3-worker",
    "request_memory": "16G",
    "request_cpu": "1",
    "env": {
        "PYSPARK_PYTHON": "/usr/bin/python3"
    }
}


# define how we wish to pull objects from the data cache
DC_TOLERANCE = {"type": "range", "to": "latest_cached"}
# another way:  DC_TOLERANCE={'type': 'version', 'value': 1}


#################### FUNCTIONS #####################################
# This is the actual code you want to run

# If not defined here, can be pulled in from dag/lib directory
# (see "Import user-defined functions", above)


# This decorator is required, so do not touch it!
# Don't put anything between the decorator and def keyword
@dag_factory
def create_dag():
    with DAG(dag_id='pull_sales_data_example_af',
             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:


        #################### DEFINE TASKS ##################################

        read_in_data = SparkOperator(
            task_id='read_data',
            func=read_data,
            queue=MY_SPARK_QUEUE,
            metastore_conn_id='metastore_ndx_uat_adls'
        )

        summarize_data_task = PythonOperator(
            task_id='summarize_data',
            python_callable=summarize_data,
            provide_context=True
        )



        #################### RUN TASKS #####################################

        read_in_data >> summarize_data_task

        # note:  spark session will stop automatically


        return dag  # Do not change this
