{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Airflow and DAGs\n",
    "\n",
    "### Purpose:\n",
    "- Acquaint potential/new users with the purpose of using Airflow\n",
    "- Describe the structure of files in an Airflow project/job\n",
    "- Walk through the structure of an Airflow \"dag.py\" program\n",
    "- Identify some key elements you will probably always need\n",
    "- Illustrate with actual code\n",
    "\n",
    "### Assumptions:\n",
    "- That you are or will be working in Data Science Studio (DSS)\n",
    "- That you have some experience with Python\n",
    "\n",
    "### <span style=\"color:#90EE90\">Version: Python 3 </span>    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Table of Contents\n",
    "\n",
    "- [Part 1: Why Airflow?](#Part-1:--Why-Airflow?)\n",
    "    - [Part 1 Exercise](#Part-1-Exercise)\n",
    "- [Part 2: Structure of an Airflow Project](#Part-2:--Structure-of-an-Airflow-Project)\n",
    "    - [Part 2a: General Directory Structure and Required Files](#Part-2a:--General-Directory-Structure-and-Required-Files)\n",
    "    - [Part 2b: metadata.yaml and job_request.yaml](#Part-2b:--metadata.yaml-and-job_request.yaml)\n",
    "    - [Part 2c: dag.py](#Part-2c:--dag.py)\n",
    "- [Part 3: Testing and Monitoring an Airflow Process](#Part-3:--Testing-and-Monitoring-an-Airflow-Process)\n",
    "    - [Part 3 Exercise](#Part-3-Exercise)\n",
    "- [Part 4: Reviewing an Airflow Process](#Part-4:--Reviewing-an-Airflow-Process)\n",
    "- [Part 5: Stopping an Airflow Process](#Part-5:--Stopping-an-Airflow-Process)\n",
    "- [Part 6:  Improving Upon Your Airflow Process (Recipe)](#Part-6:--Improving-Upon-Your-Airflow-Process-(Recipe))\n",
    "- [Part 7: Designing Your Airflow Process](#Part-7:--Designing-Your-Airflow-Process)\n",
    "- [Part 8: Restarting a DAG](#Part-8:--Restarting-a-DAG)\n",
    "- [Part 9: Other \"Hooks\" and Operators](#Part-9:--Other-%22Hooks%22-and-Operators)\n",
    "- [Appendix.  Command Line recipe Commands](#Appendix.-Command-Line-recipe-Commands)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 1:  Why Airflow?</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <span style=\"color:LimeGreen\">Use Case #1</span><br>You want to run a Python program, and you need more resources than in a notebook\n",
    "\n",
    "#### <span style=\"color:DimGray; font: normal 13pt Calibri; line-height:20px\">Even with a choice of capacities for notebooks, you are still limited to one container.  With Airflow, you can access greater resources for both CPU and memory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <span style=\"color:LimeGreen\">Use Case #2</span><br>You want to use Spark, and you need more resources than in a notebook\n",
    "\n",
    "\n",
    "#### <span style=\"color:DimGray; font: normal 13pt Calibri; line-height:20px\">You are processing a lot of data, and you want to take advantage of Spark's distributed-processing environment.  Further, you need to bring these data back into another work environment, such as Python, which would have memory limitations in the notebook environment.</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <span style=\"color:LimeGreen\">Use Case #3</span><br>You want to run your code in a production environment\n",
    "\n",
    "#### <span style=\"color:DimGray; font: normal 13pt Calibri; line-height:20px\">You want to create pipelines of tasks that run in sequence and can be scheduled.</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <span style=\"color:LimeGreen\">Use Case #4</span><br>You want to create a process that could be initiated from outside of DSS\n",
    "\n",
    "#### <span style=\"color:DimGray; font: normal 13pt Calibri; line-height:20px\">You want to be able to initiate the DSS-based process from outside the platform, perhaps as part of an existing set of processes that run in other environments.</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:orange\">Part 1 Exercise</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will start by creating code to extract and summarize some sales data. Step through the [pull_sales_data_example](./pull_sales_data_example.ipynb) notebook. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 2:  Structure of an Airflow Project</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:LightSkyBlue\">Part 2a:  General Directory Structure and Required Files</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <span style=\"line-height:20px\">An Airflow project (job/process) consists of a directory and subdirectory containing several files.</span>\n",
    "\n",
    "### There are three primary file components to an Airflow project:\n",
    "- metadata.yaml - _this defines the possible parameters that could be passed to the program_\n",
    "- job_request.yaml - _this gives the current values of those parameters_\n",
    "- dag.py - _this is the core program_\n",
    "\n",
    "### <span style=\"color:DarkSlateGray; font: normal 13pt Calibri; line-height:20px\">These three primary components must reside in a particular directory structure.  Here is a schematic of the simplest directory structure you will need:</span>\n",
    "\n",
    "```sh\n",
    "{your project name}       <-- this is your top-level directory\n",
    "    |--metadata.yaml      \n",
    "    |--job_request.yaml   \n",
    "    |--dag\n",
    "    |   |--dag.py         \n",
    "    |   |--__init__.py    <-- this file has 0 bytes but needs to be here\n",
    "    |--.pylintrc\n",
    "    |--.coveragerc\n",
    "```\n",
    "\n",
    "\n",
    "#### <span style=\"line-height:20px\">Even if you are not passing any parameters to the program, you still need both .yaml files </span><span style=\"color:DimGray; font: normal 13pt Calibri; line-height:20px\">(although \"job_request.yaml\" can actually be named anything).</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example, where the project name is \"my_airflow_project\":\n",
    "\n",
    "<img src=\"./images/introduction_to_airflow_and_dags/airflow_project_directory.png\" alt=\"airflow_project_directory graph is missing!\" width=\"650\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "#### <span style=\"color:DarkSlateGray; font: normal 12pt Calibri; line-height:20px\">Typically, there will also be a \"lib\" subdirectory under the \"dag\" subdirectory, and it will contain user-written functions such as utilities that can be called by the dag.py program. In that case, you will have this structure:</span>\n",
    "\n",
    "```sh\n",
    "{your project name}       <-- this is your top-level directory\n",
    "    |--metadata.yaml      \n",
    "    |--job_request.yaml   \n",
    "    |--dag\n",
    "    |   |--dag.py         \n",
    "    |   |--__init__.py    <-- this file has 0 bytes but needs to be here\n",
    "    |   |--lib\n",
    "    |      |your_user_functions.py\n",
    "    |      |maybe_another_set_of_functions.py\n",
    "    |      |--__init__.py  <-- same as above\n",
    "    |--.pylintrc\n",
    "    |--.coveragerc\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Continuing our example, we have a user-defined functions file called \"my_user_functions.py\":\n",
    "\n",
    "<img src=\"./images/introduction_to_airflow_and_dags/airflow_project_lib_directory.png\" alt=\"airflow_project_lib_directory graph is missing!\" width=\"650\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:LightSkyBlue\">Part 2b:  metadata.yaml and job_request.yaml</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The metadata.yaml and job_request.yaml files will reside in the top-level of your Airflow project directory, e.g.:\n",
    "\n",
    "- <span style=\"background-color:#FFFF00; font: normal 10pt Lucida Console\">pull_sales_data_example_af/</span><span style=\"font: normal 10pt Lucida Console\">metadata.yaml</span>\n",
    "- <span style=\"background-color:#FFFF00; font: normal 10pt Lucida Console\">pull_sales_data_example_af/</span><span style=\"font: normal 10pt Lucida Console\">job_request.yaml</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### <span style=\"line-height:20px\">The</span> <span style=\"color:Chocolate; line-height:20px\">metadata.yaml</span><span style=\"line-height:20px\"> file contains the name and version number of your Airflow project and definitions of any modifiable parameters that you wish to allow the user to set.</span>\n",
    "\n",
    "\n",
    "- Example of the header information.  spec_version is the version of the syntax checker.\n",
    "\n",
    "```sh\n",
    "spec_version: v2   # this is the version of the syntax checker in use.  keep like this.\n",
    "recipe_id: pull_sales_data_example_af   # name of your Airflow project\n",
    "recipe_version: \"0_0_1\"  # Recipe version must be in this format\n",
    "description: |\n",
    "  Example to pull data from sales facts database and make use of data cache.\n",
    "```\n",
    "\n",
    "- You must have the email parameter (even if no others), as your Airflow run_id will incorporate your name\n",
    "   \n",
    "```sh   \n",
    "configuration:\n",
    "  # NOTE:  types allowed are null, boolean, object, array, number, string, or integer\n",
    "  properties:\n",
    "      email:\n",
    "      description: This required parameter is your email address so you can be\n",
    "        notified of the success or failure of your dag run.\n",
    "      type: string\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### <span style=\"line-height:20px\">The</span> <span style=\"color:#1E90FF; line-height:20px\">job_request.yaml</span><span style=\"line-height:20px\"> file contains the values of the user-modifiable parameters.  It must also have the same header information as the metadata.yaml file.</span>\n",
    "\n",
    "- Example, matching metadata.yaml above:\n",
    "\n",
    "```sh   \n",
    "spec_version: v2\n",
    "recipe_id: pull_sales_data_example_af\n",
    "recipe_version: \"0_0_1\"  # Recipe version must be in this format\n",
    "description: |\n",
    "  Example to pull data from sales facts database and make use of data cache.\n",
    "configuration:\n",
    "  email: e.xample@example.com  # be sure to put your own e-mail!\n",
    "```   \n",
    "\n",
    "- Spark cluster configuration information would go at the bottom, if relevant:\n",
    "\n",
    "```sh\n",
    "system_configuration:\n",
    "  spark_config:\n",
    "    num_nodes: 1\n",
    "    request_cpu_per_node: 1\n",
    "    request_memory_gb_per_node: \"16G\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting Default Parameters\n",
    "\n",
    "In metadata.yaml, you can specify the value you want a user-defined parameter to take if the value is not set in job_request.yaml.\n",
    "\n",
    "<img src=\"./images/introduction_to_airflow_and_dags/default_parms_example.png\" alt=\"missing_parms_example_graphic!\" width=2000>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Specifying Valid Values of Parameters\n",
    "\n",
    "If you know there are limited valid values for a user-defined parameter, you can list them in the metadata.yaml file.  Any value not in this list will prevent the job from getting submitted to Airflow.\n",
    "\n",
    "<img src=\"./images/introduction_to_airflow_and_dags/example_of_enum.png\" alt='missing_enum!' width=650>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:LightSkyBlue\">Part 2c:  dag.py</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dag.py file will reside in the dag subdirectory of your Airflow project directory, e.g.:\n",
    "\n",
    "- <span style=\"background-color:#FFFF00; font: normal 10pt Lucida Console\">pull_sales_data_example_af</span><span style=\"font: normal 10pt Lucida Console\">/dag/dag.py</span>\n",
    "\n",
    "The name of your Airflow project must be specified in the dag_id= field in the dag.py program:\n",
    "\n",
    "<span style=\"font: normal 10pt Lucida Console\">def create_dag():<br>\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;with DAG(dag_id='</span><span style=\"background-color:#FFFF00; font: normal 10pt Lucida Console\">pull_sales_data_example_af</span><span style=\"font: normal 10pt Lucida Console\">', ....</span>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Structure of dag.py\n",
    "#### The dag.py file must contain\n",
    "- General configuration settings\n",
    "- Functions (could be imported from external module)\n",
    "- Defined Airflow tasks that call those functions\n",
    "- Statement(s) identifying the order in which the tasks are to be executed\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We will examine these in more detail in the Part 3 Exercise.\n",
    "\n",
    "For further study, please look at the first part of [this](./introduction-to-airflow-and-dags-additional_material.ipynb) tutorial notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 3:  Testing and Monitoring an Airflow Process</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:orange\">Part 3 Exercise</span>\n",
    "\n",
    "## Editing and Submitting for Testing\n",
    "\n",
    "We have a sample Airflow project that performs the same steps as in the\n",
    "[pull_sales_data_example](./pull_sales_data_example.ipynb) notebook. \n",
    "\n",
    "Let's copy this sample DAG and needed yaml files to a separate location. Executing the following cell will copy the needed files under a tutorial/passing_parameters subdirectory in your home directory. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir -p ~/tutorial\n",
    "cp -pr pull_sales_data_example_af ~/tutorial/."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then...\n",
    "- Open a terminal window\n",
    "- Change directory (cd) to /home/notebook/tutorial/pull_sales_data_example_af\n",
    "- Edit the job_request.yaml file, modifying the dc_query_tag and email parameters.\n",
    "- Do a test run of the code by submitting this at the command prompt in a terminal window:\n",
    "\n",
    "    <span style=\"font: normal 10pt Lucida Console\">recipe taste job_request.yaml</span>\n",
    "    \n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Monitoring your job\n",
    "- Go to your Landing Page\n",
    "- Open the Airflow web application\n",
    "- Choose Browse-Dag runs (the application might open automatically to this view)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Browse_DAGs-smaller.png](./images/introduction_to_airflow_and_dags/Browse_DAGs-smaller.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add Filter on dag_id\n",
    "\n",
    "![dag_runs_filter_dag_id.png](./images/introduction_to_airflow_and_dags/dag_runs_filter_dag_id.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Choose the name of your project \n",
    "\n",
    "![dag_runs_filter_apply.png](./images/introduction_to_airflow_and_dags/dag_runs_filter_apply.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### You will see current and (if relevant) prior jobs\n",
    "\n",
    "![dag_runs_filtered.png](./images/introduction_to_airflow_and_dags/dag_runs_filtered.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Note how \"taste\" command, project name, version number, log-in name are reflected in the dag_id and run_id, as well as the date and time\n",
    "\n",
    "![dag_runs_filtered_annotated.png](./images/introduction_to_airflow_and_dags/dag_runs_filtered_annotated.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the job is running, we can move on to reviewing our job."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 4:  Reviewing an Airflow Process</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Directed Acyclic Graph\n",
    "\n",
    "- Similar to a flow chart, except all tasks flow in one direction\n",
    "  (i.e., tasks in Airflow do not loop, will not cycle in any way)\n",
    "  \n",
    "- Find your job on the Browse-DAG Runs screen and click on the Dag Id of it. This will take you to the Directed Acyclic Graph, or \"DAG,\" which will show you all the tasks in your Airflow project.    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![pull_sales_data_example_af_DAG_only.png](./images/introduction_to_airflow_and_dags/pull_sales_data_example_af_DAG_only.png)\n",
    "\n",
    "#### <span style=\"color:green; line-height:20px\">Note: When spoken, \"dag\" can refer to the Directed Acyclic Graph, the dag.py program, or the entire set of files submitted to Airflow (see [this glossary](./airflow-and-dags-glossary-of-terms.ipynb)).  I will try to reserve capitalized \"DAG\" for the graph, and \"dag\" or \"Airflow project/process/job\" for the program process being run.   In addition, the Airflow process is commonly referred to as a \"recipe\"; you will see this usage in the more advanced tutorials.</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Branching Task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike the [pull_sales_data_example](./pull_sales_data_example.ipynb) notebook, we have now added an automated branching step, \"have_data_or_not\".  If the \"read_data\" step found no records, the process will not try to run the \"summarize_data\" step.  As we were examining the results interactively in the notebook, we could make this determination ourselves.\n",
    "\n",
    "The branch makes its decision based on a value set at the \"return_value\" step. \n",
    "Let's look at the details of that task. Click on the \"return_value\" task bubble, then choose View Log.\n",
    "\n",
    "![view_log.png](./images/introduction_to_airflow_and_dags/view_log.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This brings up a screen with the log.  Then, click on the XCom button.\n",
    "\n",
    "![choose_xcom_button.png](./images/introduction_to_airflow_and_dags/choose_xcom_button.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that a value of 1 was exported, to be read in and acted upon by another step (namely, the \"have_data_or_not\" task).\n",
    "\n",
    "![return_value_xcom.png](./images/introduction_to_airflow_and_dags/return_value_xcom.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall in the notebook, we checked the number of records pulled by our Spark query:\n",
    "    \n",
    "```sh\n",
    "n_rows = spark.sql(\"select count(*) as n_rows from step1\").toPandas()\n",
    "```\n",
    "\n",
    "In our new code, we've added another step:\n",
    "\n",
    "```sh\n",
    "# write out n_rows to workspace so we can retrieve it later\n",
    "n_rows.to_parquet(\"{}/n_rows.parquet\".format(get_workspace()))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, in the \"return_value\" function, we read in the n_rows.parquet file and return an indicator whether or not we have data:\n",
    "    \n",
    "```sh\n",
    "def return_value(**context):\n",
    "    \n",
    "    # read in number of records of data we pulled\n",
    "    my_file_loc = path_in_workspace(\"n_rows.parquet\")\n",
    "    n_rows = pd.read_parquet(my_file_loc).iloc[0,0]\n",
    "    \n",
    "    # set our return value:  1 means continue with task(s)\n",
    "    if n_rows > 0:\n",
    "        rv = 1\n",
    "    else:\n",
    "        rv = 0\n",
    "        \n",
    "    return rv\n",
    "```\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Airflow Log\n",
    "\n",
    "#### Let's return to our DAG, left-click on the \"read_data\" task bubble, then choose \"View Log\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Some excerpts from log\n",
    "\n",
    "\n",
    "![Airflow-Spark-DEMO-successful_run-Log.png](./images/introduction_to_airflow_and_dags/Airflow-Spark-DEMO-successful_run-Log.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <i><span style=\"color:orange\">On Your Own...</span></i>\n",
    "\n",
    "Look at the Airflow logs for other steps.  In particular, see how our informational messages are displayed for the \"read_data\" and \"summarize_data\" tasks. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 5:  Stopping an Airflow Process</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### <span style=\"line-height:20px\">Suppose you realize, after submitted a job with the recipe command, that you have made an error.  Rather than let it keep running and use resources, you can stop it and clear the resources.</span>  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to stop it.  Select the job from the Browse - DAG Runs screen by clicking in the checkbox on the far left, then Set state to 'failed', as shown.\n",
    "\n",
    "![with_selected_set_to_failed.png](./images/introduction_to_airflow_and_dags/with_selected_set_to_failed.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, after it has stopped, then you can select it again and delete it.\n",
    "\n",
    "<b><span style=\"color:red\">IMPORTANT:</span></b>  If your Airflow process had an active Spark cluster when you set it to failed, please give the system 10 or so minutes to close down the Spark cluster before deleting the record from the Airflow web interface database.\n",
    "\n",
    "![delete_ended_job.png](./images/introduction_to_airflow_and_dags/delete_ended_job.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 6:  Improving Upon Your Airflow Process (Recipe)</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In our notebook and in the recipe we just ran, we had a step that checked to see if our specified query returned any data.  However, if it did not, the recipe *still* went on to the next task, \"summarize_data.\"  However, if no data exist for our specified query parameters, there will not be a data frame pushed to the data cache, and (potentially) the summarize_data step will fail. \n",
    "\n",
    "\n",
    "- (\"Potentially\" it will fail, because if you used the same `dc_query_tag` for two runs in a row but you changed your query parameters in-between, summarize_data would run.  However, it would read the most recent entry in the data cache with query=dc_query_tag, which would be the data from the earlier run, *not* the data you intended to summarize with your new run.)\n",
    "\n",
    "So, what can we do instead?  What we'd like is to see if the \"read_data\" step found any data, and only move to the \"summarize_data\" step if so.  \n",
    "\n",
    "We will now run a recipe that adds these features to our original code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir -p ~/tutorial\n",
    "cp -pr pull_sales_data_example_af2 ~/tutorial/."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, as you did before...\n",
    "- Open a terminal window\n",
    "- Change directory (cd) to /home/notebook/tutorial/pull_sales_data_example_af2\n",
    "- Edit the job_request.yaml file, modifying the dc_query_tag and email parameters.\n",
    "- Do a test run of the code by submitting this at the command prompt in a terminal window:\n",
    "\n",
    "    <span style=\"font: normal 10pt Lucida Console\">recipe taste job_request.yaml</span>\n",
    "    \n",
    "Note also that we have added an additional parameter into this job_request.yaml:\n",
    "```sh\n",
    "  send_dag_complete_email: True\n",
    "```\n",
    "in the \n",
    "```sh\n",
    "system_configuration:\n",
    "```\n",
    "subsection.  This will send a message to the e-mail address you provide when the recipe has finished (whether with success or failure).  \n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Follow the steps given earlier to monitor your job."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <i><span style=\"color:orange\">On Your Own...</span></i>\n",
    "\n",
    "Try running each of your recipes with an error in your job_request.yaml.  For example, try putting a spelling error in your category specification:\n",
    "```sh\n",
    "  category: \"LANDRY CARE\"\n",
    "```  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 7:  Designing Your Airflow Process</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <span style=\"color:LimeGreen\">Think about your process ...</span>\n",
    "\n",
    "### First, what do you want to accomplish?\n",
    "### Next, how will you accomplish this?  \n",
    "- Can you do it all with Python code?\n",
    "- From what sources will you need to pull in data? (servers, files, etc.)\n",
    "- What other resources will you need (e.g., Spark)?\n",
    "\n",
    "### What future enhancements or expansions might be needed?\n",
    "- What would be best to modularize?\n",
    "- What user-defined parameters should you incorporate?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 8:  Restarting a DAG</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### <span style=\"line-height:20px\">During development, sometimes you will want to test only part of a process.  There is a way to re-start an Airflow process from a particular step, or run it only up to a particular step.</span>\n",
    "- This can be used to modify code and re-start a process without having to repeat more resource-intensive steps\n",
    "- In can be used in testing and when fixing errors\n",
    "\n",
    "For more information, see \n",
    "[\"Re-Starting a DAG\"](./introduction-to-airflow-and-dags-additional_material.ipynb/#Re-Starting-a-DAG)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Part 9:  Other \"Hooks\" and Operators</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See:\n",
    "\n",
    "[Hooks](./hooks.ipynb)\n",
    "\n",
    "[Operators](./operators.ipynb)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <span style=\"color:blue\">Appendix. Command-Line recipe Commands</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### To issue any commands, first navigate your project directory (called \"your_project\" here as an example). Then ....\n",
    "\n",
    "#### To submit a program on which you are working to Airflow and issue this command:\n",
    "    recipe taste job_request.yaml\n",
    "#### To invoke the syntax checker without running the program, issue this command:\n",
    "    recipe validate job_request.yaml\n",
    "#### To re-start a failed Airflow job after fixing the code in your project directory, issue this command:\n",
    "    recipe taste job_request.yaml --from _failed step_ --run_id _full_run_id_from Airflow_\n",
    "    \n",
    "#### To get more information, type\n",
    "    recipe --help\n",
    "    recipe taste --help\n",
    "    recipe _command_ --help"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Usage:\r\n",
      "  recipe taste <job_request_file> [<source_folder>] [options]\r\n",
      "\r\n",
      "Description:\r\n",
      "  Run the recipe indicated in the job request file in Airflow without publishing it to the Recipe\r\n",
      "  Library.\r\n",
      "\r\n",
      "Arguments:\r\n",
      "  <job_request_file>    Job request file which should identify a specific recipe.\r\n",
      "  <source_folder>       Source folder of the recipe.\r\n",
      "\r\n",
      "Options:\r\n",
      "  -h --help                 Show this screen.\r\n",
      "  --api_url=<api_url>       The url of the API to talk to. A default url is only available inside the cluster, so either \r\n",
      "                            this option or the host option is required to connect externally.\r\n",
      "  --host=<host>             The host (and tenant) for the recipe api and oauth2 api e.g. http://dss-uat.nielsen.com/default\r\n",
      "                            This parameter will be used to construct the urls of the recipe and oauth2 APIs. They will be constructed\r\n",
      "                            as {host}/recipe-service/api/v1 and {host}/oauth2-proxy/api/v1 respectively. \r\n",
      "                            The api_url and oauth2_url parameters will override their respective host generated values if present.\r\n",
      "                            NOTE: you probably just need to use this parameter and not the api_url and oauth2_url\r\n",
      "  --oauth2_url=<oauth2_url> The url of the oauth2 API to talk to e.g. http://dss-uat.nielsen.com/default/oauth2-proxy/api/v1\r\n",
      "                            A default url is only available inside the cluster, so either this option or the host option is \r\n",
      "                            required to connect externally.\r\n",
      "  --client_credentials_path=<client_credentials_path> The path to the yaml file that contains valid oauth2 client credentials.\r\n",
      "                            The yaml file might look like:\r\n",
      "                            client_id: my-client-id\r\n",
      "                            client_secret: my-client-secret\r\n",
      "                            These credentials are essential if accessing the recipe service from outside of a jupyter notebook.\r\n",
      "                            If this value is not passed, the client credentials will be searched for on the following paths:\r\n",
      "                            client_credentials.yaml, .client_credentials.yaml, ~/.dss/client_credentials.yaml, and ~/.dss/.client_credentials.yaml\r\n",
      "  --allow-multiple          Allow concurrent tastes of a recipe.  This flag prevents the recipe code from being reloaded\r\n",
      "                            into Airflow.  Only call this flag if the recipe has already been tasted.\r\n",
      "  --run_id=<run_id>         The run id of a completed dag run. Supplying the run_id will clear all or some of the task\r\n",
      "                            instances in the dag_run.  Executing recipe taste with a run_id only works if no other\r\n",
      "                            instances of that recipe are running.  The --allow-multiple flag does not apply when a run_id\r\n",
      "                            is supplied. Use --from, --to,  or both to select which tasks to re-execute. Use --only-failed\r\n",
      "                            to re-execute only the tasks which are in a failed state.\r\n",
      "  --from=<task_id>...       Taste a recipe from one or more specified tasks of an executed dag run. When listing many tasks,\r\n",
      "                            separate the tasks with a comma leaving no spaces in between.\r\n",
      "                            Example: recipe taste job_request.yaml --from task1,task2,task3 --run_id run_1 [--only_failed]\r\n",
      "  --to=<task_id>...         Taste a recipe to one or more specified tasks of an executed dag run. When listing many tasks,\r\n",
      "                            separate the tasks with a comma leaving no spaces in between.\r\n",
      "                            Example: recipe taste job_request.yaml --to task1,task2,task3 --run_id run_1 [--only_failed]\r\n",
      "  --only_failed             Taste from/to only failed tasks.\r\n",
      "  --email=<email>           The email address of the person publishing the DAG. This must be set if running outside\r\n",
      "                            of a Jupyter notebook.\r\n"
     ]
    }
   ],
   "source": [
    "!recipe taste --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Back to Table of Contents](#Table-of-Contents)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
