{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Support for Custom Python Packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*****\n",
    "\n",
    "This tutorial covers:\n",
    "-   [Notebook Use Case](#Notebook-Use-Case)\n",
    "-   [Recipe Use Case](#Recipe-Use-Case)\n",
    "-   [Recipe Test and Publish](#Recipe-Test-and-Publish)\n",
    "-   [Limitations](#Limitations)\n",
    "    \n",
    "*****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "DSS provides some popular Python packages out of the box in both Jupyter notebook and Airflow worker containers. If you need to use a package that is not provided by DSS, you can install and use it in both notebook and dag use cases. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook Use Case"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To install a Python package in a notebook, you can run the following command in a terminal:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```bash\n",
    "pip  install SOME_PACKAGE --user\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above command will install SOME_PACKAGE into your notebook container that will be preserved between notebook restarts. Once installed, you can use SOME_PACKAGE in the same way as you use any other installed Python packages. E.g., you can open any Python notebook file and execute `import SOME_PACKAGE`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Use Case"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The recipe use case for a custom Python package is slightly different, since each task runs in a separate container and container state is not preserved between task runs. It would be wasteful to run pip install at the beginning of every run of a task."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, to define additional Python packages that you want to use in a recipe, you need to define a pip [requirements file](https://pip.pypa.io/en/stable/user_guide/#requirements-files) and put it under the `python_requirements` directory that you need to create at the root of your recipe directory (on the same level as the `dag` directory)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of the recipe directory structure with one requirements.txt file. The name of the directory is important and must be exactly `python_requirements`. The name of the requirements file is not important as long as it ends with `.txt`, but by convention, it is usually called `requirements.txt`. You can define more than one requirements file if you need different set of requirements in different tasks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "my_recipe\n",
    "|-- dag\n",
    "|   `-- dag.py\n",
    ".\n",
    ". *omitted*\n",
    ".\n",
    "|-- python_requirements\n",
    "|   `-- requirements.txt\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the pip documentation: \n",
    "> \"*Logically, a Requirements file is just a list of pip install arguments placed in a file. Note that you should not rely on the items in the file being installed by pip in any particular order.*\"\n",
    "\n",
    "Here is an example content of a requirements.txt file:\n",
    "```python\n",
    "pkg1==1.2.3\n",
    "pkg2==3.2.1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note**, that we highly recommend that you peg the specific version of each package you want to be installed. While we don't validate that you do that, we might enforce this rule in the future."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since tasks in one dag could be of different types and not all of them need the additional python packages, once you define the requirements file, you need to specify which tasks use the additional packages in the dag. To support this additional argument, we have added a DSS `PythonOperator` into dss_airflow_utils:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "from dss_airflow_utils.operators.python_operator import PythonOperator\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The DSS PythonOperator has similar input parameters and serves the same purpose as the airflow PythonOperator. It has an additional `requirements` input parameter that is expected to be either a string with the requirements file name or a list of requirements file names from the `python_requirements` directory. If you provide more than one file name in the list and if those requirements have conflicting packages, the right most requirement file will take the precedence.\n",
    "\n",
    "Finally, to import and use the custom packages from the requirements file, you need to import them locally inside the python_callable function. Here are example sections of a dag.py file:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "\n",
    "from dss_airflow_utils.operators.python_operator import PythonOperator\n",
    "\n",
    "...\n",
    "\n",
    "def my_func(file_path, **context):\n",
    "    import pkg1\n",
    "    \n",
    "    pkg1.so_something()\n",
    "\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(...) as dag:\n",
    "    ...\n",
    "        PythonOperator(\n",
    "            task_id='end',\n",
    "            python_callable=my_func,\n",
    "            provide_context=True,\n",
    "            requirements='requirements.txt',\n",
    "            worker_type = 'python3.7-worker'   \n",
    "        )\n",
    "    ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### How it works"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you do `recipe taste`, `recipe test` or `recipe publish` of the recipe with python_requirements present for the first time or after any change in any requirements file, DSS will trigger the build dag to pull and package your requirements. We package all requirements specified in the requirements file including their intermediate dependencies. Any run of your recipe will use the same Python dependencies that were packaged at the build dag run, even if any upstream dependencies changed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Test and Publish\n",
    "\n",
    "For `recipe test` and `recipe publish`, you may encounter problems when attempting to run tests that make use of custom python requirements. The reason for this is that you need to additionally specify for a given test suite if it requires additional python dependencies. Because a recipe is allowed to have multiple requirements files for different tasks as well as multiple test suites, it's difficult to know which requirements are needed for a set of tests. To specify a requirements file for a set of tests, you need to add this extra line to the `test.yaml`:\n",
    "\n",
    "```\n",
    "config:\n",
    "    ...\n",
    "    python_requirements: <insert-name-of-requirements-file-here>  # THIS LINE\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Limitations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Currently the custom Python dependencies via the requirements files can only be used with the DSS PythonOperator.\n",
    "- You cannot use custom Python dependencies at the dag level, e.g., in the code where you create tasks.\n",
    "- You cannot import the custom Python dependencies at the top of the dag.py module or at the top of any modules that are imported in the dag.py"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}