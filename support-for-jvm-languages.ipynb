{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Support for JVM Languages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*****\n",
    "\n",
    "This tutorial covers:\n",
    "-   [Supported JVM languages](#Supported-JVM-languages)\n",
    "-   [How to use Scala in a notebook](#How-to-use-Scala-in-a-notebook)\n",
    "-   [How to add Java code to a recipe](#How-to-add-Java-code-to-a-recipe)\n",
    "-   [How to add Scala code to a recipe](#How-to-add-Scala-code-to-a-recipe)\n",
    "-   [How to build Java/Scala code](#How-to-build-Java/Scala-code)\n",
    "-   [How to run Java/Scala code in a recipe](#How-to-run-Java/Scala-code-in-a-recipe)\n",
    "-   [How to run a Spark-enabled JAR in Recipe](#How-to-Run-a-Spark-enabled-JAR-in-a-Recipe)\n",
    "-   [What platform features are not yet supported in JVM languages](#What-platform-features-are-not-yet-supported-in-JVM-languages)\n",
    "    \n",
    "*****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Supported JVM languages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Java and Scala are the two supported JVM languages. While Scala is supported in both interactive notebook and recipe use cases, Java is only supported in a non-interactive way through a recipe requiring a build step."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to use Scala in a notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To use Scala in a notebook, you need create a notebook with the `apache_toree_scala - Scala` kernel. Note, that you can also change the kernel of an existing notebook.\n",
    "\n",
    "#### How to use spark\n",
    "You can use spark connecting to the system spark cluster from Scala. The code below shows how to create a spark session with default configuration:\n",
    "\n",
    "```scala\n",
    "import org.apache.spark.sql.SparkSession\n",
    "val spark = SparkSession.builder.appName(\"Simple Scala Application\").getOrCreate()\n",
    "```\n",
    "\n",
    "If you need a larger than default spark session, you can use the spark builder and provide spark configuration key value pairs as shown below: \n",
    "\n",
    "```scala\n",
    "val spark = SparkSession.builder.appName(\"Simple Scala Application\")\n",
    "                                .config(\"spark.executor.cores\", 2)\n",
    "                                .config(\"spark.executor.memory\", \"2G\")\n",
    "                                .getOrCreate()\n",
    "```\n",
    "\n",
    "Please note that by default you can have only one spark session per notebook. You need to stop an existing spark session or restart your notebook kernel before executing the above code for the new session configuration to take effect.\n",
    "\n",
    "##### Connecting spark cluster to a data lake source\n",
    "\n",
    "Since SparkHoook is not avaialble in Scala notebooks, it is **not** yet possible to use provided airflow connection ids to connect to corresponding data sources. Therefore, to configure spark to connect to certain data source, you need run the following command in a terminal (replacing `metastore_ndx_uat_adls` with appropriate connection id):\n",
    "```bash\n",
    "python -c 'from dss_airflow_utils.hooks.spark_hook import SparkHook; SparkHook(\"metastore_ndx_uat_adls\")'\n",
    "```\n",
    "\n",
    "Note that when `SparkHook(\"metastore_ndx_uat_adls\")` executes, it changes spark configuration on the notebook container level, not per individual notebook file. Therefore, you cannot have multiple spark sessions in different notebook files connecting to different data lake sources.\n",
    "\n",
    "Then run the command below (notice `.enableHiveSupport()`) to tell spark to use hive:\n",
    "\n",
    "```scala\n",
    "val spark = SparkSession.builder.appName(\"Simple Scala Application\")\n",
    "                                .config(\"spark.executor.cores\", 2)\n",
    "                                .config(\"spark.executor.memory\", \"2G\")\n",
    "                                .enableHiveSupport()\n",
    "                                .getOrCreate()\n",
    "```\n",
    "Once the new spark session is created, you can then run spark query like you do it in python.\n",
    "\n",
    "```scala\n",
    "val sdf = spark.sql(\"select product_key, pg_subbrand from aod_pg_prototxi.prdc limit 2\")\n",
    "sdf.show()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to add Java code to a recipe\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `recipe init` command allows you to quickly create a boilerplate recipe directory structure based on a default or custom template. In addition to creating boilerplate recipes, the `recipe init` command can be run on a single recipe multiple times to add \"recipe mixins\". Recipe mixins are recipe templates that add additional code or configuration boilerplate for different languages or features.\n",
    "\n",
    "We have created a recipe mixin to help you add a Java/Maven build process to your recipe. This recipe mixin is called the `maven_java_mixin`, and it adds a \"hello world\" Java application to your recipe, complete with a Maven pom.xml."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example invocations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Existing Recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have an existing recipe and want to add a Java/Maven build process to that recipe, you can run `recipe init -t maven_java_mixin .` anywhere within your recipe to easily install the `maven_java_mixin`. When running the command, you will be prompted for various names that will be used to template out the new recipe mixin directory structure. You will be asked to provide the `package_name`, which is the name of the Java package that will be created, and the `main_class`, which is the name of the Java main class that will be generated for this boilerplate project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "notebook:/home/notebook$ cd myrecipe/\n",
    "notebook:/home/notebook/myrecipe$ recipe init -t maven_java_mixin .\n",
    "package_name [my_maven_package]: my_package\n",
    "main_class [App]: MyApp\n",
    "2019-04-23 15:57:08,680 - INFO - Recipe Initialized in /home/notebook/myrecipe/\n",
    "2019-04-23 15:57:08,681 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### New Recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you would like to create a new recipe with a Java/Maven build process, you can first create the recipe with the default `recipe init` template, and then run `recipe init -t maven_java_mixin myrecipe`, with the option to install the `maven_java_mixin`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "notebook@:/home/notebook$ recipe init myrecipe\n",
    "recipe_id [my-first-recipe]: my_recipe\n",
    "email [e.xample@example.com]:\n",
    "2019-04-23 16:02:31,050 - INFO - Recipe Initialized in /home/notebook/myrecipe/\n",
    "2019-04-23 16:02:31,050 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "\n",
    "notebook:/home/notebook$ recipe init -t maven_java_mixin myrecipe\n",
    "package_name [my_maven_package]: my_package1\n",
    "main_class [App]: MyApp\n",
    "2019-04-23 16:03:21,771 - INFO - Recipe Initialized in /home/notebook/myrecipe/\n",
    "2019-04-23 16:03:21,771 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Multiple Java Packages in existing recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to maintain multiple Java/Maven packages in your recipe, you can install the `maven_java_mixin` multiple times within the same recipe and provide different package names."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "notebook:/home/notebook/myrecipe$ recipe init -t maven_java_mixin  .\n",
    "package_name [my_maven_package]: foo\n",
    "main_class [App]:\n",
    "2019-04-23 16:34:35,780 - INFO - Recipe Initialized in /home/notebook/myrecipe/\n",
    "2019-04-23 16:34:35,780 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "\n",
    "notebook:/home/notebook/myrecipe$ recipe init -t maven_java_mixin  .\n",
    "package_name [my_maven_package]: bar\n",
    "main_class [App]:\n",
    "2019-04-23 16:34:41,189 - INFO - Recipe Initialized in /home/notebook/myrecipe/\n",
    "2019-04-23 16:34:41,189 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"java-mixin-directory-structure\"></a>\n",
    "### Directory Structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running the above `recipe init` command with the `maven_java_mixin` will create a new `maven` sub-directory of your top-level recipe directory. This `maven` directory will contain one or more Java packages, each containing a sample \"hello world\" Java app. \n",
    "\n",
    "Each Java package will contain a default pom.xml file that you can customize to suit your needs. Please note that the groupId, artifactId, and version keys in the pom.xml do not need to be set, as these will be automatically populated by the Java/Maven build process. \n",
    "\n",
    "It is expected that you use this example project as a starting point for further development. You can even create your own derivative Maven Recipe Mixins to suit your specific needs. More information on how to do this is available in the `recipe init` tutorial. \n",
    "\n",
    "*Note: If you already have files or directories in your recipe with the same name as those in the recipe mixin, your files and directories will take precedence over any new ones created and will not be overwritten.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "my_recipe\n",
    "|-- dag\n",
    "|   `-- dag.py\n",
    ".\n",
    ". *omitted*\n",
    ".\n",
    "|-- maven\n",
    "|   `-- my_package\n",
    "|       |-- pom.xml\n",
    "|       `-- src\n",
    "|           |-- main\n",
    "|           |   `-- java\n",
    "|           |       `-- com\n",
    "|           |           `-- nielsen\n",
    "|           |               `-- dss\n",
    "|           |                   `-- my_package\n",
    "|           |                       `-- MyApp.java\n",
    "|           `-- test\n",
    "|               `-- java\n",
    "|                   `-- com\n",
    "|                       `-- nielsen\n",
    "|                           `-- dss\n",
    "|                               `-- my_package\n",
    "|                                   `-- MyAppTest.java\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to add Scala code to a recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This section assumes that you have read the [How to add java code to a recipe](#how-to-add-java-code-to-a-recipe) section. If you have not, we encourage you to do so before you read this section.\n",
    "\n",
    "In order to easily create a skeleton for Scala code within your recipe, we have provided the `maven_scala_mixin`. This template can be added to your recipe by calling `recipe init -t maven_scala_mixin` anywhere within your recipe directory structure.\n",
    "\n",
    "Similiar to calling the `maven_java_mixin`, you will be prompted to input values for the package name and main class for your Scala application:\n",
    "<pre>\n",
    "recipe init -t maven_scala_mixin\n",
    "package_name [my_maven_package]: my_package\n",
    "main_class [App]: MyApp\n",
    "2019-05-01 15:53:43,925 - INFO - Recipe Initialized in /tmp/my_recipe/\n",
    "2019-05-01 15:53:43,926 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Directory Structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The directory structure is very similar to the Java directory structure with the same highlights. Please refer to the [Java mixin directory struture](#java-mixin-directory-structure) for more details. The resulting directory will be as follows:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "my_recipe\n",
    "|-- dag\n",
    "|   `-- dag.py\n",
    ".\n",
    ". *omitted*\n",
    ".\n",
    "|-- maven\n",
    "|   `-- my_package\n",
    "|       |-- pom.xml\n",
    "|       `-- src\n",
    "|           |-- main\n",
    "|           |   `-- scala\n",
    "|           |       `-- com\n",
    "|           |           `-- nielsen\n",
    "|           |               `-- dss\n",
    "|           |                   `-- my_package\n",
    "|           |                       `-- MyApp.scala\n",
    "|           `-- test\n",
    "|               `-- scala\n",
    "|                   `-- com\n",
    "|                       `-- nielsen\n",
    "|                           `-- dss\n",
    "|                               `-- my_package\n",
    "|                                   `-- MyAppTest.scala\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please note that the default test package that comes with the `maven_scala_mixin` is JUnit, which was chosen to be consistent with the `maven_java_mixin`. If you would like to add another testing dependency (i.e. ScalaTest), you'll need to update the `pom.xml` file in the `maven/my_package` directory. For more details, please refer to [this tutorial](http://www.scalatest.org/user_guide/using_the_scalatest_maven_plugin)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Directory Structure with multiple mixins"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is perfectly acceptable to initialize multiple templates in the same recipe. You can even initialize multiple templates with different JVM languages. Consider the following example:\n",
    "<pre>\n",
    "recipe init my_recipe\n",
    "recipe_id [my-first-recipe]: \n",
    "email [e.xample@example.com]: \n",
    "2019-05-01 18:58:35,347 - INFO - Recipe Initialized in /tmp/my_recipe/\n",
    "2019-05-01 18:58:35,347 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "\n",
    "recipe init -t maven_java_mixin my_recipe\n",
    "package_name [my_maven_package]: first_java_package\n",
    "main_class [App]: FirstJavaApp\n",
    "2019-05-01 18:59:38,787 - INFO - Recipe Initialized in /tmp/my_recipe/\n",
    "2019-05-01 18:59:38,787 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "\n",
    "recipe init -t maven_java_mixin my_recipe\n",
    "package_name [my_maven_package]: second_java_package\n",
    "main_class [App]: SecondJavaApp\n",
    "2019-05-01 19:00:07,693 - INFO - Recipe Initialized in /tmp/my_recipe/\n",
    "2019-05-01 19:00:07,693 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "\n",
    "recipe init -t maven_scala_mixin my_recipe\n",
    "package_name [my_maven_package]: scala_package\n",
    "main_class [App]: \n",
    "2019-05-01 19:00:32,502 - INFO - Recipe Initialized in /tmp/my_recipe/\n",
    "2019-05-01 19:00:32,502 - INFO - 'Initialize Recipe' command was successfully executed.\n",
    "</pre>\n",
    "\n",
    "This series of calls results in the following directory structure:\n",
    "<pre>\n",
    "my_recipe\n",
    "|-- dag\n",
    "|   `-- dag.py\n",
    ".\n",
    ". *omitted*\n",
    ".\n",
    "|-- maven\n",
    "|   `-- first_java_package\n",
    "|       |-- pom.xml\n",
    "|       `-- src\n",
    "|           |-- main\n",
    "|           |   `-- java\n",
    "|           |       `-- com\n",
    "|           |           `-- nielsen\n",
    "|           |               `-- dss\n",
    "|           |                   `-- first_java_package\n",
    "|           |                       `-- FirstJavaApp.java\n",
    "|           `-- test\n",
    "|               `-- java\n",
    "|                   `-- com\n",
    "|                       `-- nielsen\n",
    "|                           `-- dss\n",
    "|                               `-- first_java_package\n",
    "|                                   `-- FirstJavaAppTest.java\n",
    "|   `-- second_java_package\n",
    "|       |-- pom.xml\n",
    "|       `-- src\n",
    "|           |-- main\n",
    "|           |   `-- java\n",
    "|           |       `-- com\n",
    "|           |           `-- nielsen\n",
    "|           |               `-- dss\n",
    "|           |                   `-- second_java_package\n",
    "|           |                       `-- SecondJavaApp.java\n",
    "|           `-- test\n",
    "|               `-- java\n",
    "|                   `-- com\n",
    "|                       `-- nielsen\n",
    "|                           `-- dss\n",
    "|                               `-- second_java_package\n",
    "|                                   `-- SecondJavaAppTest.java\n",
    "|   `-- scala_package\n",
    "|       |-- pom.xml\n",
    "|       `-- src\n",
    "|           |-- main\n",
    "|           |   `-- scala\n",
    "|           |       `-- com\n",
    "|           |           `-- nielsen\n",
    "|           |               `-- dss\n",
    "|           |                   `-- scala_package\n",
    "|           |                       `-- App.scala\n",
    "|           `-- test\n",
    "|               `-- scala\n",
    "|                   `-- com\n",
    "|                       `-- nielsen\n",
    "|                           `-- dss\n",
    "|                               `-- second_java_package\n",
    "|                                   `-- AppTest.scala\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please notice that each package ended up in their own eponymous directory and had no conflict with one another. Further, each package will get its own `pom.xml` which allows DSS to create disparate `.jar` files that can be called by the `JVMOperator`. One word of caution: please __DO NOT__ initialize different templates in the same package. It won't inherently break your recipe, but it will pollute the `maven` folder with unused code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to build Java/Scala code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run Java or Scala (or other JVM language) code in a dag, the code needs to be compiled and packaged by Maven into a JAR. Fortunately, no additional actions are required from you. Recipe client `taste`, `test` and `publish` commands automatically identify if your recipe contains any maven artifacts and inject the `maven_build` task into the `build dag`. Below are three examples of the build dag for my-first-recipe recipe version 0_0_0:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The build dag for the `recipe taste` use case:\n",
    "\n",
    "  <img src=\"./images/taste_build_dag.png\" alt=\"Taste Build Dag\" style=\"width: 1100px;\" width=\"1100px\"/>\n",
    "  \n",
    "  \n",
    "* The build dag for the `recipe test` use case:\n",
    "\n",
    "  <img src=\"./images/test_build_dag.png\" alt=\"Test Build Dag\" style=\"width: 1100px;\" width=\"1100px\"/>\n",
    "  \n",
    "  \n",
    "* The build dag for the `recipe publish` use case:\n",
    "\n",
    "  <img src=\"./images/publish_build_dag.png\" alt=\"Publish Build Dag\" style=\"width: 1100px;\" width=\"1100px\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the buid dag_id is the same regardles of the action - in all three cases above, the dag_id is `build_my-first-recipe_0_0_0_ekarataev` which follows the `build_YOUR-RECIPE-ID_YOUR_RECIPE_VERION_YOUREMAIL` struture. The run_id, however, has the action prefix. Note `taste_`, `test_` and `publish_` prefixes in the three run_ids above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `maven_build` task will run the `mvn deploy` command that will compile, test, package and deploy your code as a JAR to an Artifactory repository (you don't need to worry about what it is or where it is located, rest assured we will handle it and keep your JAR safe and sound). Each Maven artifact in your recipe will be built sequentially by task. All Maven output will also be printed to the task log. If you have any junit tests included, you can find the output of the test runs in the `maven_build` task logs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since in the `taste` use case, your recipe dag will have one or more tasks that will run the artifacts' code, the execution of your dag will have to wait for the corresponding build dag run to finish. To do that, we will automatically inject the `wait_for_build_artifacts` task at the very beginning of your dag before any other task. The `wait_for_build_artifacts` task will periodically (every 30 seconds) check on the execution status of the corresponding build dag. If the build dag fails, the `wait_for_build_artifacts` will also fail and thus your whole tasted dag run will fail. On the other hand, as soon as the build dag finishes and the `wait_for_build_artifacts` completes successfully, your tasted dag run will proceed to execute its tasks in a normal fashion. Here is an example of the dag injected with the `wait_for_build_artifacts` task:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./images/taste_user_recipe.png\" alt=\"Tasted User Dag\" style=\"width: 1100px;\" width=\"1100px\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `wait_for_build_artifacts` task logs will tell you the url of the corresponding build dag run and the its status.\n",
    "\n",
    "For the recipe run or schedule use cases no `wait_for_build_artifacts` tasks are injected because recipe artifacts will be built during the publish step."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to run Java/Scala code in a recipe\n",
    "A recipe can run java/scala code that is packaged as a JAR if the dag contains a `JvmOperator`. \n",
    "\n",
    "### JvmOperator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "        Create a JVM Operator that executes a JAR.\n",
      "\n",
      "        Note, that the JAR must be identifiable via maven coordinates and be\n",
      "        available to DSS in either the central maven repository or one\n",
      "        registered as an airflow connection. If DSS built the JAR for you,\n",
      "        do not fret - it will be available already.\n",
      "\n",
      "        Must provide a main_class and either package_name or artifact_id.\n",
      "\n",
      "        :param package_name: name of the package to run. Ex. If your recipe\n",
      "          contains java code, this value should be the name of the directory\n",
      "          containing your java package source code and pom.xml. This is just\n",
      "          a convenience alias for artifact_id - both work the exact same and\n",
      "          one of them is required as an input parameter. If both are\n",
      "          provided, the value for artifact_id will be used.\n",
      "        :param group_id: maven group id string\n",
      "        :param artifact_id: maven artifact id string\n",
      "        :param version: maven artifact version string\n",
      "        :param main_class: full name of the main class to run\n",
      "          full name consists of <package location> + <simple name of class>\n",
      "          ex. org.company.app.MainClass\n",
      "        :param main_args: either a list of arguments to pass to the\n",
      "          main class or a python callable function that accepts an airflow\n",
      "          context as input parameter and optionally returns a list of input\n",
      "          arguments that should be passed to the main class.\n",
      "          Such function can do any pre-execution preparations like read values\n",
      "          from job request or xcom, create necessary dirs or files, etc.\n",
      "          Will also accept a single number or string as one parameter.\n",
      "          Note, input parameters will be converted to string by calling\n",
      "          str(value) before passing them to `java` command.\n",
      "        :param mvn_conn_id: registered airflow connection with credentials\n",
      "          to an extra maven repository to search in addition to the central\n",
      "          mvn repo. Defaults to the DSS artifactory test repo.\n",
      "        \n"
     ]
    }
   ],
   "source": [
    "from dss_airflow_utils.operators.jvm_operator import JvmOperator\n",
    "print(JvmOperator.__init__.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The JVM Operator uses [maven](https://maven.apache.org/what-is-maven.html) to download, install, and run JARs. The primary input parameters are:\n",
    " - The main class to run\n",
    " - A package identifier\n",
    "     * name of a package included within the recipe to be built by DSS\n",
    "     * full maven coordinates to a JAR stored in the [central maven repository](https://mvnrepository.com/repos/central) or in a DSS repository\n",
    "\n",
    "So if there is custom JVM-language code included within your recipe, DSS will compile and package the code into JAR(s) for you and thus you need only specify the main class to run and the name of the package that contains that class. The package name should be the name of the directory with the pom.xml file.\n",
    "\n",
    "For example, given the directory structure below, the appropriate package name would be `my_package`.\n",
    "<pre>\n",
    "my_recipe\n",
    "|-- dag\n",
    "|   `-- dag.py\n",
    ".\n",
    ". *omitted*\n",
    ".\n",
    "|-- maven\n",
    "|   `-- my_package\n",
    "|       |-- pom.xml\n",
    "|       `-- src\n",
    "|           |-- main\n",
    "|           |   `-- java\n",
    "|           |       `-- com\n",
    "|           |           `-- nielsen\n",
    "|           |               `-- dss\n",
    "|           |                   `-- my_package\n",
    "|           |                       `-- MyApp.java\n",
    "|           `-- test\n",
    "|               `-- java\n",
    "|                   `-- com\n",
    "|                       `-- nielsen\n",
    "|                           `-- dss\n",
    "|                               `-- my_package\n",
    "|                                   `-- MyAppTest.java\n",
    "</pre>\n",
    "\n",
    "Additionally, the JVM Operator can be used to run code not included within a recipe (i.e. a third party dependency available in the central maven repository). In order to do so, in addition to the main class to run (and instead of package_name), the full maven coordinates(group_id, artifact_id, version) must be provided.\n",
    "\n",
    "> Note: the package_name and artifact_id parameters are actually just aliases of one another as the DSS artifact build process uses the package_name as the artifact id at build-time.\n",
    "\n",
    "##### Examples:\n",
    "```python\n",
    "java_task = JvmOperator(\n",
    "    task_id='run-some-java',\n",
    "    artifact_id='my-guava-app',\n",
    "    group_id='com.nielsen.dss.testrecipe.deps',\n",
    "    version='1.0-SNAPSHOT',\n",
    "    main_class='com.nielsen.dss.my_package.MyApp'\n",
    ")\n",
    "\n",
    "my_jar_task = JvmOperator(\n",
    "    task_id='run-my-java',\n",
    "    package_name='my_package',\n",
    "    main_class='com.nielsen.dss.my_package.MyApp'\n",
    ")\n",
    "```\n",
    "> **Important**: The JvmOperator relies on the worker image having maven installed - which it will handle by default; however be careful not to override the worker type it uses via DEFAULT_ARGS. If you specify a worker type other than the maven-worker, spark3-worker, or spark-worker in the DEFAULT_ARGS, provide a queue argument to the JvmOperator with {'worker_type': 'maven-worker'}.\n",
    "\n",
    "### Passing Command Line Arguments\n",
    "Arguments are passed to the JAR from the `main_args` parameter. The simplest form of a `main_args` parameter is a list of parameters to pass (each will be converted to strings) like `['arg1', 'arg2', 5]`; however `main_args` can also be a callable that can be used as a pre-execution hook to run any necessary setup steps and/or to dynamically generate the argument list.\n",
    "\n",
    "```python\n",
    "from dss_airflow_utils.utils import get_config\n",
    "\n",
    "def get_main_args(context):\n",
    "    config = get_config(context)\n",
    "    \n",
    "    return [config.get('some_parameter', 'some_default_value')]\n",
    "\n",
    "java_task = JvmOperator(\n",
    "    task_id='run-some-java',\n",
    "    package_name='my_package',\n",
    "    main_class='com.nielsen.dss.my_package.MyApp',\n",
    "    main_args=get_main_args\n",
    ")\n",
    "\n",
    "java_task2 = JvmOperator(\n",
    "    task_id='run-some-java2',\n",
    "    package_name='my_package',\n",
    "    main_class='com.nielsen.dss.my_package.MyApp',\n",
    "    main_args=['arg1', 'arg2']\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to Run a Spark-enabled JAR in a Recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to running Java code within a recipe, you can also use Java to run Spark jobs. You can do this with the SparkJvmOperator. With the SparkJvmOperator, you can download and execute Jar files that make calls to Spark. The SparkJvmOperator is a hybrid of both the SparkOperator and the JvmOperator, so the usage is similar to those.\n",
    "\n",
    "Note that you must specify the `artifact_id`, `group_id` and `version`, or the `package_name`, just as you do for the [JvmOperator](#how-to-run-in-recipe). Just as for the JvmOperator, you need to provide a `main_class` argument; the optional `main_args` argument can be a dict, callable, or iterable that can be used to pass arguments to the JAR file.\n",
    "\n",
    "In addition, you need to configure a SparkConfig to size and configure the Spark cluster as you do for a [SparkOperator](spark-configuration.ipynb). You can pass in custom `spark_confs` to customize the Spark's behavior, and the `scale` parameter works the same way that it does for the SparkOperator. Also, the metastore_connection_id can be passed to the operator to specify a connection to a hive metastore. See the \"spark-configuration\" tutorial for more information on how to size and configure a Spark cluster."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "        Spark Options:\n",
      "        :param queue (object, optional): describes the docker container that\n",
      "            will be execute func\n",
      "        :param spark_confs (object, optional): Any additional spark configurations\n",
      "            you want to pass.\n",
      "        :param metastore_conn_id (str, optional): The airflow connection id\n",
      "        :param spark_master_url (str, optional): The url to the spark master node.\n",
      "\n",
      "        JVM Options:\n",
      "        :param package_name: name of the package to run. Ex. If your recipe\n",
      "            contains java code, this value should be the name of the directory\n",
      "            containing your java package source code and pom.xml. This is just\n",
      "            a convenience alias for artifact_id - both work the exact same and\n",
      "            one of them is required as an input parameter. If both are\n",
      "            provided, the value for artifact_id will be used.\n",
      "        :param group_id: maven group id string\n",
      "        :param artifact_id: maven artifact id string\n",
      "        :param version: maven artifact version string\n",
      "        :param main_class: full name of the main class to run\n",
      "            full name consists of <package location> + <simple name of class>\n",
      "            ex. org.company.app.MainClass\n",
      "        :param main_args: either a list of arguments to pass to the\n",
      "            main class or a python callable function that accepts an airflow\n",
      "            context as input parameter and optionally returns a list of input\n",
      "            arguments that should be passed to the main class.\n",
      "            Such function can do any pre-execution preparations like read values\n",
      "            from job request or xcom, create necessary dirs or files, etc.\n",
      "            Will also accept a single number or string as one parameter.\n",
      "            Note, input parameters will be converted to string by calling\n",
      "            str(value) before passing them to `java` command.\n",
      "        :param mvn_conn_id: registered airflow connection with credentials\n",
      "            to an extra maven repository to search in addition to the central\n",
      "            mvn repo. Defaults to the DSS Artifactory test repo.\n",
      "        \n"
     ]
    }
   ],
   "source": [
    "from dss_airflow_utils.operators.spark_jvm_operator import SparkJvmOperator\n",
    "print(SparkJvmOperator.__init__.__doc__)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is an example of a DAG that invokes the SparkJvmOperator in two different ways. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "##### Examples:\n",
    "```python\n",
    "from dss_airflow_utils.operators.spark_jvm_operator import SparkJvmOperator\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "...\n",
    "...\n",
    "...\n",
    "\n",
    "\n",
    "from dss_airflow_utils.utils import get_config\n",
    "\n",
    "def get_main_args(context):\n",
    "    config = get_config(context)\n",
    "    return [config.get('some_parameter', 'some_default_value')]\n",
    "\n",
    "\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=1,\n",
    "                request_cpu_per_node=1,\n",
    "                request_memory_gb_per_node=1)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='e2e_spark_jvm_operator',\n",
    "             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "\n",
    "    # Specify the main_args as a list\n",
    "    java_task = SparkJvmOperator(\n",
    "        task_id='java_task',\n",
    "        package_name='my_package',\n",
    "        # provide a main class if not specified as part of the Jar\n",
    "        main_class='com.nielsen.dss.my_package.MyApp',\n",
    "        # provide optional arguments to the Jar (can be a list or callable)\n",
    "        main_args=[\"a\", \"b\"],\n",
    "    )\n",
    "\n",
    "    # Specify the main_args as a callable\n",
    "    java_package_task = SparkJvmOperator(\n",
    "        task_id='java_package_task',\n",
    "        package_name='my_package',\n",
    "        main_class='com.nielsen.dss.my_package.MyApp',\n",
    "        # The main_args can take a function that returns the arguments\n",
    "        main_args=get_main_args,\n",
    "    )\n",
    "    \n",
    "    java_task >> java_package_task\n",
    "    \n",
    "    return dag  # Do not change this\n",
    "\n",
    "```\n",
    "\n",
    "\n",
    "> **Important**: The SparkJvmOperator relies on the worker image having maven installed - which it will handle by default. If you specify a worker type other than spark3-worker or spark-worker in the DEFAULT_ARGS, provide a `queue` argument to the SparkJvmOperator with `{'worker_type': 'spark3-worker'}`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What platform features are not yet supported in JVM languages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We currently do **not** provide `dss_airflow_utils` and `dss_datacache_client` in JVM languages. What this means is that in your JVM language code, you cannot directly use any of the hooks or utility functions that you are accustomed to use in \"python-based\" tasks (e.g., in PythonOperator or SparkOperator, etc.). For example, you cannot read values from the job request directly. You also cannot use any of the airflow utilities such as xcoms, you do not have access to the airflow context, and, finally, you cannot interact with the datacache directly from JVM language code. \n",
    "\n",
    "What you can do is pass a python callable as the `main_args` input parameter as described above that can read xcoms, pull data from datacache, or read input parameters from job request, and then pass the required information into the main class of your JVM code. Both python callable and your JVM code see the same file system, so for relatively large input parameters (e.g., a nested dictionary object passed by xcom or job request), you can first write it to dag run workspace and then pass the path to the file as an input parameter to your JVM code."
   ]
  }
 ],
 "metadata": {
  "file_extension": ".py",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  },
  "mimetype": "text/x-python",
  "name": "python",
  "nbconvert_exporter": "python",
  "pygments_lexer": "ipython3",
  "version": "3.5.2"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
