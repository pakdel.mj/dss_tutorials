{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Recipe Development with MLflow\n",
    "\n",
    "The first steps towards taking advantage of MLflow include understanding the new dag structure required for training and\n",
    "serving models as well as becoming familiar with the dss_ml_utils package.\n",
    "\n",
    "## Creating the Dag \n",
    "\n",
    "### Dag Structure\n",
    "\n",
    "You learned how to create a dag and what the expected dag structure is from the \n",
    "[Running a Recipe](./running-recipe.ipynb) tutorial.  The same basic structure is used for machine learning dags with \n",
    "some additional elements. Below, we have the structure expected for an MLflow dag.\n",
    "\n",
    "```bash\n",
    "my_first_mlflow_dag/\n",
    "    |- .coveragerc\n",
    "    |- .pylintrc\n",
    "    |- __init__.py\n",
    "    |- config.yaml\n",
    "    |- metadata.yaml\n",
    "    |- serving_job_request.yaml\n",
    "    |- training_job_request.yaml\n",
    "    |- dag/\n",
    "        |- __init__.py\n",
    "        |- dag.py\n",
    "        |- dag_training.py\n",
    "        |- lib/\n",
    "            |- __init__.py\n",
    "            |- my_module.py\n",
    "    |- tests/\n",
    "          |- __init__.py\n",
    "          |- test_lib/\n",
    "                   |- __init__.py\n",
    "                   |- test.yaml\n",
    "                   |- test_my_module.py\n",
    "\n",
    "```\n",
    "\n",
    "The difference in this dag structure is the two sets of job_request and dag.py files.  There is one job_request and dag \n",
    "file for training your model and a second pair for serving it.  The information to select what action is taken is found \n",
    "in the job_request and the metadata.yaml.  \n",
    "\n",
    "### Metadata\n",
    "\n",
    "The metadata.yaml for a machine learning model will take in a training configuration which will host the parameters you \n",
    "can set to tune your models."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: example_ml_model\n",
    "recipe_version: \"0_0_0\"  # Recipe version must be in this format\n",
    "description: |\n",
    "  This optional field should be used to describe on a high level what the recipe is about in plain English.\n",
    "  E.g., This recipe is a skeleton example to be used as starting point to write new recipes.\n",
    "configuration:\n",
    "  # Configuration is optional, you would need to define it (as a valid jsonschema but in yaml format) if you\n",
    "  # want your recipe to accept any input parameters via a job request.\n",
    "  # For a friendly jsonschema reference, see https://spacetelescope.github.io/understanding-json-schema/index.html\n",
    "  properties:\n",
    "    name_of_the_first_parameter:\n",
    "      description: Plain English description on the what this parameter mean, is used for, whatever.\n",
    "      type: string\n",
    "    name_of_the_second_parameter:\n",
    "      description: Same as above\n",
    "      type: number\n",
    "    email:\n",
    "      description: This parameter allows you to specify your email address if you wish to be\n",
    "        notified of the success or failure of your dag run.\n",
    "      type: string\n",
    "training_configuration:\n",
    "  properties:\n",
    "    sk_alpha:\n",
    "      description: alpha param to tune elasticnet for wine quality predictor\n",
    "      type: number\n",
    "    sk_l1_ratio:\n",
    "      description: l1 ratio param to tune elasticnet for wine quality predictor\n",
    "      type: number\n",
    "```"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "There are two types of job requests that we can have for this type of recipe.  The first one we address is the job \n",
    "request that triggers the training portion of this recipe, meaning it will execute the dag_training.py module. The \n",
    "second kind of job request will run the dag.py module.  \n",
    "\n",
    "### Job Request - Training\n",
    "\n",
    "When training your models, make sure that the job request contains the 'dag_training' parameter set in the \n",
    "system_configuration of your job_request with the value set to 'True'.  Information defined in the metadata.yaml \n",
    "under the training_configuration should also be filled out in this job request definition."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: example_ml_model\n",
    "recipe_version: \"0_0_0\"\n",
    "description: |\n",
    "  This optional field should be used to describe on a high level what the recipe is about in plain English.\n",
    "  E.g., This recipe is a skeleton example to be used as starting point to write new recipes.\n",
    "system_configuration:\n",
    "  send_dag_complete_email: False\n",
    "  dag_training: True\n",
    "training_configuration:\n",
    "  sk_alpha: 0.8\n",
    "  sk_l1_ratio: 0.9\n",
    "  py_answer: 42\n",
    "configuration:\n",
    "  sklearn_model_suffix: \"scikit-elasticnet\"\n",
    "  pyfunc_model_suffix: \"super-smart\"\n",
    "  email: \"example@example.com\"\n",
    "```\n"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "As mentioned before, 'dag_training' is set to 'True', so Airflow will execute the dag_training.py module of your recipe.\n",
    "The dag id for the training dag will include 'train' before the recipe_id.\n",
    "\n",
    "The recipe that this job request serves has two models that it creates and the model_suffixes are being provided via the\n",
    "job request. \n",
    "\n",
    "### Job Request - Serving\n",
    "\n",
    "To execute the other half of your recipe, the dag.py module, simply omit the 'dag_training' parameter from your job\n",
    "request."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: example_ml_model\n",
    "recipe_version: \"0_0_0\"\n",
    "description: |\n",
    "  This optional field should be used to describe on a high level what the recipe is about in plain English.\n",
    "  E.g., This recipe is a skeleton example to be used as starting point to write new recipes.\n",
    "system_configuration:\n",
    "  send_dag_complete_email: False\n",
    "configuration:\n",
    "  model_name: \"mlflow_kmichael_0_0_0_super-smart\"\n",
    "  email: \"example@example.com\"\n",
    "```"
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "In the example above, the model name for the recipe to use is being provided through the job request. \n",
    "\n",
    "### DSS ML UTILS\n",
    "\n",
    "To communicate with MLflow on Data Science Studio, your dag will need to use the dss_ml_utils package.  This package \n",
    "contains wrappers around the MLflow Python API that will register, log, and load models from and to your recipes.  The\n",
    "dss_ml_utils package currently supports Pyfunc, PyTorch, and sckit-learn models.  \n",
    "\n",
    "For more information on dss_ml_utils, you may use the Python help function.\n",
    "\n",
    "```python\n",
    "import dss_ml_utils as mlflow\n",
    "\n",
    "# for help with the utils\n",
    "help(mlflow.utils)\n",
    "# for help with one of the models\n",
    "help(mlflow.sklearn)\n",
    "```\n",
    "\n",
    "Let's see how the utils are used in action. "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Training\n",
    "\n",
    "The following code demonstrates the use of the ml utils to register and get your model started.  The example \n",
    "dag_training.py imports the well known iris dataset, logs a parameter, a metric, and the model, and it registers the \n",
    "model. "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.operators.python_operator import PythonOperator\n",
    "from dss_airflow_utils.utils import get_training_config\n",
    "\n",
    "import dss_ml_utils as mlflow\n",
    "from dss_ml_utils.utils import RegisterModelInfo\n",
    "\n",
    "from sklearn.datasets import load_iris\n",
    "from sklearn.ensemble import RandomForestRegressor\n",
    "from sklearn.metrics import mean_squared_error\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "DEFAULT_ARGS = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'queue': {\n",
    "        \"request_memory\": \"4G\",\n",
    "        \"request_cpu\": \"1\",\n",
    "        \"worker_type\": \"python3.7-worker\",\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def train_and_log_iris_model(**context):\n",
    "    iris = load_iris()\n",
    "    x = iris.data[:, 2:]\n",
    "    y = iris.target\n",
    "    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=7)\n",
    "\n",
    "    num_estimators = 100\n",
    "    mlflow.log_param(\"num_estimators\",num_estimators)\n",
    "    \n",
    "    rf = RandomForestRegressor(n_estimators=num_estimators)\n",
    "    rf.fit(X_train, y_train)\n",
    "    predictions = rf.predict(X_test)\n",
    "    \n",
    "    mse = mean_squared_error(y_test, predictions)\n",
    "    mlflow.log_metric(\"mse\", mse)\n",
    "    \n",
    "    mlflow.sklearn.log_model(rf, \n",
    "                             model_suffix=\"random-forest-model\",\n",
    "                             register_model_info=RegisterModelInfo(\n",
    "                                 \"Staging\", \"iris example model\")\n",
    "                             )\n",
    "\n",
    "\n",
    "# This decorator is required, so do not touch it!\n",
    "# Don't put anything between the decorator and def keyword\n",
    "@dag_factory\n",
    "def create_dag():  # pragma: no cover\n",
    "    with DAG(dag_id='iris_example',\n",
    "             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "        # See the link below for documentation of the PythonOperator\n",
    "        # https://airflow.apache.org/code.html#airflow.operators.PythonOperator\n",
    "        sklearn = PythonOperator(\n",
    "            task_id='scikit-learn',\n",
    "            python_callable=train_and_log_iris_model,\n",
    "            provide_context=True,\n",
    "        )\n",
    "\n",
    "        # Define dependencies between operators here, for example:\n",
    "        sklearn \n",
    "\n",
    "        return dag  # Do not change this"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% \n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "When creating an MLflow run with the Python API, a new run is instantiated with a call to start_run() as seen below.\n",
    "\n",
    "```python\n",
    "with mlflow.start_run() as run:\n",
    "    ...\n",
    "```\n",
    "\n",
    "Note that the train_and_log_iris_model method used to train our model does **NOT** explicitly create an MLflow run. This \n",
    "is because the run is instantiated with the first call to mlflow.log_param() in this dag, but any of the calls available \n",
    "in dss_ml_utils will start a new MLflow run for the current recipe if one has not already been created.  The MLflow run \n",
    "object itself can be accessed by calling mlflow.active_run().  \n",
    "\n",
    "For convenience, the log_model and load_model functions take in slightly different parameters than their original MLflow \n",
    "counterparts. The log_model call at the end of the method is unique to the DSS platform in that it takes in the \n",
    "model_suffix and register_model_info parameters. `log_model` will autogenerate a name for the model artifact based on \n",
    "the current recipe run. \n",
    "\n",
    "As a convenience, if you'd like to provide a custom identifier for the model but still want the model name to be \n",
    "associated with your recipe run, you can provide a model_suffix. The model_suffix is an optional string value to append \n",
    "to the model name upon model registration if you are running a  single model in your recipe.  If you are running more \n",
    "than one model in your dag, you will need to specify the model suffix. \n",
    "\n",
    "Including register_model_info in this call will make sure that the model created in the recipe becomes a registered \n",
    "MLflow model, enabling the model to be loaded for use in other recipes. The register_model_info parameter takes two bits \n",
    "of information, the model stage and a description. The stage of the model can be staging or production. The description \n",
    "allows you to briefly define your model and will be viewable in the MLflow models UI.  "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Serving\n",
    "The following code demonstrates the use of the ml utils to load a model. "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.operators.python_operator import PythonOperator\n",
    "from dss_airflow_utils.utils import get_training_config\n",
    "\n",
    "import dss_ml_utils as mlflow\n",
    "from dss_ml_utils.utils import RegisterModelInfo\n",
    "\n",
    "import pandas as pd\n",
    "\n",
    "from sklearn.datasets import load_iris\n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "DEFAULT_ARGS = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'queue': {\n",
    "        \"request_memory\": \"4G\",\n",
    "        \"request_cpu\": \"1\",\n",
    "        \"worker_type\": \"python3.7-worker\",\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def predict_iris(input_data, model_name, stage):\n",
    "    # load model\n",
    "    iris_model = mlflow.sklearn.load_model(model_name=model_name, stage=stage)\n",
    "    \n",
    "    # apply model to new dataset\n",
    "    predictions = iris_model.predict(pd.DataFrame(input_data))\n",
    "    return predictions\n",
    "\n",
    "# This decorator is required, so do not touch it!\n",
    "# Don't put anything between the decorator and def keyword\n",
    "@dag_factory\n",
    "def create_dag():  # pragma: no cover\n",
    "    with DAG(dag_id='iris_example',\n",
    "             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "        # See the link below for documentation of the PythonOperator\n",
    "        # https://airflow.apache.org/code.html#airflow.operators.PythonOperator\n",
    "        sklearn_predict = PythonOperator(\n",
    "            task_id='scikit-predict',\n",
    "            python_callable=predict_iris,\n",
    "            op_kwargs={\n",
    "                \"stage\": \"Staging\",\n",
    "                \"model_name\": \"example_mlflow_dag_tpodlipni_0_0_0_random-forest-model\",\n",
    "                \"input_data\": [[4.9, 3.1]],\n",
    "            },\n",
    "        )\n",
    "\n",
    "        # Define dependencies between operators here, for example:\n",
    "        sklearn_predict \n",
    "\n",
    "        return dag  # Do not change this"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "To load a model into your recipe, you can supply the following information:\n",
    "- model_name: the full model name\n",
    "- model_suffix\n",
    "- stage: stage of model, can be staging or production\n",
    "- version: the version number of the model\n",
    "\n",
    "If none of the above parameters are provided into the load_model call, then the method will attempt to pull the latest \n",
    "version in the production stage associated with the calling recipe.  \n",
    "\n",
    "If the recipe you are running is serving the same model that it created, you will not need to supply the model name. \n",
    "The exception to this is if you included a model_suffix when registering your model, in which case the model_name or the\n",
    "model_suffix should be supplied.  \n",
    "\n",
    "If the recipe you are running is serving a model that it did not train, then you will **need** to supply the model_name.\n",
    "\n",
    "For stage, if you do not supply a value, the default will be production.  If you supply the stage, the latest version of\n",
    "the model in that stage will be used. \n",
    "\n",
    "If a version is provided then the stage input is ignored. \n"
   ],
   "metadata": {
    "collapsed": false
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "source": [],
    "metadata": {
     "collapsed": false
    }
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}