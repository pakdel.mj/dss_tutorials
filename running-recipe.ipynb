{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"top\"></a>\n",
    "# Tutorial: Running a Recipe\n",
    "\n",
    "***\n",
    "In this tutorial we look at:\n",
    "- What files and directories are within a recipe's directory structure\n",
    "- What those files and directories are used for\n",
    "- How to modify the copied skeleton to have something that can be run as a recipe\n",
    "- How to test your recipe\n",
    "- What the version string in a recipe means\n",
    "***\n",
    "\n",
    "In the [previous page](./getting-started.ipynb#top) you ran `recipe init -d ~/my_first_dag` to create a recipe skeleton in your work directory.  Let's look at what those files are, how they contribute to the recipe, and what needs to be changed before we submit this to the recipe server.\n",
    "\n",
    "Here is the structure of the my_first_dag directory created by `recipe init`:\n",
    "\n",
    "```bash\n",
    "my_first_dag/   \n",
    "        |- .coveragerc\n",
    "        |- .pylintrc\n",
    "        |- job_request.yaml\n",
    "        |- metadata.yaml\n",
    "        |- __init__.py\n",
    "        |- tests/\n",
    "              |- __init__.py\n",
    "              |- test_lib/\n",
    "                       |- test_my_module.py\n",
    "                       |- test.yaml\n",
    "                       |- __init__.py\n",
    "        |- dag/\n",
    "            |- dag.py\n",
    "            |- __init__.py\n",
    "            |- lib/\n",
    "                |- my_module.py\n",
    "                |- __init__.py\n",
    "\n",
    "```\n",
    "\n",
    "The first two files, `.coveragerc` and `.pylintrc` and the `tests` directory are used when testing the recipe when you go to publish it.  That is the final step in building a recipe so we will leave a discussion of testing until later.\n",
    "\n",
    "The `__init__.py` files that are present in each directory are 0-length files that mark the directory as a Python module. Having these present allows your code to `import` the routines in python files into your code.\n",
    "\n",
    "The important three files are `dag.py` in the dag directory that holds the python code that Airflow will run, `job_request.yaml` which Airflow will read to determine how to run your dag, and `metadata.yaml` in the top directory of your recipe which defines the fields that make up the job_request file.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****\n",
    "Let's copy a sample DAG and needed yaml files as a starting point for this tutorial. Executing the following cell will copy the needed files under a tutorial/running_recipe subdirectory in your home directory. \n",
    "****"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cd ~\n",
    "!mkdir -p ~/tutorial \n",
    "!cp -r ~/example/tutorial/sample_dags/running_recipe ~/tutorial/\n",
    "!ls ~/tutorial/running_recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "#### Here are the contents of the sample dag.py file under tutorial/running_recipe/dag/dag.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!dag.py dag_id=tutorial_02_1\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from airflow.operators.bash_operator import BashOperator\n",
    "from airflow.operators.python_operator import PythonOperator\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.workspace_utils import path_in_workspace\n",
    "from dss_airflow_utils.utils import get_config\n",
    "\n",
    "\n",
    "# Import of the custom packages and modules must be relative!\n",
    "from .lib.my_module import my_business_logic\n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "DEFAULT_ARGS = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'pool': 'dss-testing-pool',\n",
    "    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string\n",
    "        \"worker_type\": \"python-worker\",     # here you can ask for specific worker container\n",
    "        \"request_memory\": \".5G\",            # and how much computing resource a task will need\n",
    "        \"request_cpu\": \".2\"                 # don't worry, you can override this on a task basis.\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def my_func(file_path, **context):\n",
    "    # Here is how you can get parameters passed via job request\n",
    "    config = get_config(context)\n",
    "    my_business_logic(file_path,\n",
    "                      config.get(\"name_of_the_first_parameter\"),\n",
    "                      config.get(\"name_of_the_second_parameter\"))\n",
    "\n",
    "# This decorator is required, so do not touch it!\n",
    "# Don't put anything between the decorator and def keyword\n",
    "# pylint: disable=pointless-statement\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_02_1',\n",
    "             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "        # Define operators here, for example:\n",
    "\n",
    "        output_file = path_in_workspace(\"foo.txt\")\n",
    "\n",
    "        cmd = \"echo 'Hi there, I am the first task in this DAG' > {}\".format(\n",
    "            output_file)\n",
    "\n",
    "        t_1 = BashOperator(\n",
    "            task_id='start',\n",
    "            bash_command=cmd\n",
    "        )\n",
    "\n",
    "        # See the link below for documentation of the PythonOperator\n",
    "        # https://airflow.apache.org/code.html#airflow.operators.PythonOperator\n",
    "        t_2 = PythonOperator(\n",
    "            task_id='end',\n",
    "            python_callable=my_func,\n",
    "            provide_context=True,\n",
    "            op_args=[output_file]\n",
    "        )\n",
    "\n",
    "        # Define dependencies between operators here, for example:\n",
    "\n",
    "        t_1 >> t_2\n",
    "\n",
    "        return dag  # Do not change this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The dag_id\n",
    "<font style=\"background:yellow\">\n",
    "Before we go any further, let's make a small edit to this file, and replace the value of the `dag_id`.\n",
    "</font><br>\n",
    "\n",
    "<font style=\"background:yellow\">\n",
    "The line currently reads:\n",
    "</font>\n",
    "\n",
    "```python\n",
    "   with DAG(dag_id='tutorial_02_1',\n",
    "                default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "```\n",
    "\n",
    "<font style=\"background:yellow\">\n",
    "Change it to something like:\n",
    "</font>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "<a id=\"dag.py\"></a>\n",
    "<div style=\"border:1px solid red\">\n",
    "\n",
    "```python\n",
    "with DAG(dag_id='my_first_recipe', default_args=default_args, schedule_interval=None) as dag:\n",
    "```\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what makes a proper dag ID?  It only has to be a string of printable characters starting with a letter.  But to be \"proper\" it must also define the purpose of the dag (why you wrote it).\n",
    "\n",
    "**NOTE**: It's important that the dag ids are unique across the system.\n",
    "\n",
    "It might also be useful to have your own copy of a dag when developing a recipe in a team, so that when you test your changes they do not conflict with the changes your teammates make. During recipe development, you will run ```recipe taste``` to test your recipes. \"Recipe taste\" will automatically append your initials to dag id."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Metadata\n",
    "\n",
    "Now save the dag.py file and open the `~/tutorial/running_recipe/metadata.yaml` file that you copied at the top of this tutorial.  It should look like this:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!metadata.yaml dag_id=tutorial_02_1\n",
    "\n",
    "```yaml\n",
    "# Pound symbol starts a comment. This is a comment.\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_02_1\n",
    "recipe_version: \"0_0_0\"  # Recipe version must be in this format\n",
    "description: |\n",
    "  This optional field should be used to describe on a high level what the recipe is about in plain English.\n",
    "  E.g., This recipe is a skeleton example to be used as starting point to write new recipes.\n",
    "configuration:\n",
    "  # Configuration is optional, you would need to define it (as a valid jsonschema but in yaml format) if you\n",
    "  # want your recipe to accept any input parameters via a job request.\n",
    "  # For a friendly jsonschema reference, see https://spacetelescope.github.io/understanding-json-schema/index.html\n",
    "  properties:\n",
    "    name_of_the_first_parameter:\n",
    "      description: Plain English description on the what this parameter mean, is used for, whatever.\n",
    "      type: string\n",
    "    name_of_the_second_parameter:\n",
    "      description: Same as above\n",
    "      type: number\n",
    "    email:\n",
    "      description: This parameter allows you to specify your email address if you wish to be\n",
    "        notified of the success or failure of your dag run.\n",
    "      type: string\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the dag.py file is the body of a function, the metadata.yaml is the function signature.  It defines the contract needed to invoke this dag. Let's dig into the properties exposed here.\n",
    "\n",
    "\n",
    "| tag name | required (Y/N) | value description |\n",
    "|:---:|:---:|:---|\n",
    "| `spec_version` | Y | If the metadata interpreter changes in the future this value will tell it how to read the file. `v2` is the latest version. You don't need to change this value. |\n",
    "| `recipe_id` | Y | This identifies the dag to run, it **MUST** match your `dag_id` in your recipe **EXACTLY** |\n",
    "| `recipe_version` | Y | This is important when versioning a recipe, an advanced topic we'll get to later. Versions are defined with the pattern of three integer seperated by underscores (`\"<int>_<int>_<int>\"`)|\n",
    "| `description` | N | This is a human readable description of your recipe |\n",
    "| `configuration` | N | This dictionary defines the contract needed to invoke this recipe, it contains the names of parameters, their types, and whether they are required or not. Configuration **must** be a valid jsonschema defined in yaml format. For a friendly jsonschema reference, see https://spacetelescope.github.io/understanding-json-schema/index.html |\n",
    "\n",
    "\n",
    "Let's edit this file to match our current recipe. At the moment we don't have much in the way of configuration, so our metadata.yaml is going to be pretty light. Email is a required field as it is used to create unique dag runs and you can be sent job status alerts(if you enable notifications), so be sure that is included as a configuration parameter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change recipe_id to match the dag_id that you put in your dag.py file (\"my_first_recipe\" if you made the change shown [above](#dag.py).\n",
    "\n",
    "```yaml\n",
    "recipe_id: my_first_recipe\n",
    "```\n",
    "\n",
    "You should also change the \"Description\" to something appropriate to your recipe. Save the metadata.yaml file, and let's move to the last piece."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Job Request\n",
    "\n",
    "Continuing our analogy of a function, the job request is the invocation of that function. It is responsible for supplying the arguments to the function. Even when your recipe doesn't require any arguments, a job request is still required to run it.\n",
    "\n",
    "Unlike the other files we are discussing, the job_request.yaml file does not have to live within the recipe file structure. When we get to discussing the `recipe taste` command below you will see that command can include a path to the metadata.  However it is a good practice to keep the file inside the recipe directory during development so that the recipe and its metadata are associated.\n",
    "\n",
    "Here is the job request you copied over at the top of the tutorial located at `~/tutorial/running_recipe/job_request.yaml`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!job_request.yaml dag_id=tutorial_02_1\n",
    "\n",
    "```yaml\n",
    "# This is a sample job request that for this recipe.\n",
    "# Job request is not required.\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_02_1\n",
    "recipe_version: \"0_0_0\"\n",
    "description: |\n",
    "  This optional field should be used to describe on a high level what the recipe is about in plain English.\n",
    "  E.g., This recipe is a skeleton example to be used as starting point to write new recipes.\n",
    "configuration:\n",
    "  # Note here that the configuration in the job request is not a json schema anymore.\n",
    "  # It must be an object that conforms to the schema defined in metadata.yaml.\n",
    "  name_of_the_first_parameter: abc\n",
    "  name_of_the_second_parameter: 123\n",
    "  email: e.xample@example.com\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first three lines are the same as in the `metadata.yaml` file and are used for the same purposes. So you need to update recipe_id to the same value as in your metadata.yaml file. The last element, `configuration` must provide values for the parameters defined in the metadata.yaml file.\n",
    "\n",
    "\n",
    "<p>\n",
    "<font style=\"background:yellow\">\n",
    "Update the recipe_id value to \"my_first_recipe\".\n",
    "</font>\n",
    "</p>\n",
    "\n",
    "<p>\n",
    "<font style=\"background:yellow\">\n",
    "Update the email value in the job request with your work email address.\n",
    "</font>\n",
    "</p>\n",
    "\n",
    "<p>\n",
    "<font style=\"background:yellow\">\n",
    "Feel free to change the values of \"name_of_the_first_parameter\" and \"name_of_the_second_parameter\".  According to the metadata.yaml file the first parameter must be a string and the second parameter must be a number. If you changed the \"Description\" field in the metadata file you must also change it here in the job_request so that the two files match.\n",
    "</font>\n",
    "</p>\n",
    "\n",
    "<p>\n",
    "<font style=\"background:yellow\">\n",
    "Save this file and we'll finish with actually testing the recipe.\n",
    "</font>\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Command Line Interface (CLI)\n",
    "\n",
    "<font style=\"background:yellow\">\n",
    "Open a terminal in your Jupyter Notebook.\n",
    "</font>\n",
    "\n",
    "To open the terminal go the Jupyter's directory listing page, click on the \"New\" dropdown, and select \"Terminal\".\n",
    "\n",
    "![starting a terminal](./images/starting_terminal.png)\n",
    "\n",
    "This will open a new Jupyter terminal session in a new browser tab.  Initially your directory will be set to the notebook user's home directory `/home/notebook`.  To run your recipe you want to change your directory to the top directory in your recipe's file hierarchy and issue the \"recipe taste\" command.\n",
    "\n",
    "<font style=\"background:yellow\">In the Jupyter terminal issue the commands</font>\n",
    "\n",
    "```bash\n",
    "cd ~/work/my_first_dag\n",
    "recipe taste job_request.yaml\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This should output to the console something like what is shown in this screen capture:\n",
    "\n",
    "![Taste First DAG](./images/taste_first_dag.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now navigate the to Airflow UI in your browser reached by going the the landing page and clicking on the Airflow icon. In the list of DAGs (Browse -> DAG Runs) you should see your new recipe there.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Recipe Version Number"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far we have explained that the recipe-version value is three integers with underscores between, usually `0_0_0`.  We have not explained any meaning for the string or the integers contained within it.\n",
    "\n",
    "The three integers are version numbers (as you might have guessed) and are meant to have specific meanings.  The integers must each be greater than or equal to 0 and higher numbers always indicate a later version.\n",
    "\n",
    "The first number is the major version.  Changing the major version of a recipe is meant to indicate that the recipe is different enough that its use is incompatible with other versions.  Possibly you are calculating a different way to measure something and it should not be used like the old version. Or you modify the DAG in the recipe in such a way that the output is different enough that it can't be used as input by a higher order DAG that calls this one with a RunRecipeOperator.\n",
    "\n",
    "The second number is the minor version.  It should change whenever there is a change to the DAG or the relationship between steps within the DAG.\n",
    "\n",
    "The third number is the micro version number or the release number.  It should change every time you publish your recipe.  This way if you want the latest version of a recipe it is the one with the highest release number.\n",
    "\n",
    "If everything moved in a straight line forward there would be no reason to have three parts to a version number.  But that doesn't happen usually.  If it does the major and minor versions will always be `0_0_` and only the release number will go up, producing a sequence like `0_0_0`, `0_0_1`, `0_0_2`.  But when the calculations in a recipe are reorganized the minor version should change, and we will add `0_1_0`. If this new recipe gets another release it will be tagged `0_1_1`.  The old version may still be being used and may need to be modified to handle some unforseen condition.  The old version's sequence is updated with a new release number `0_0_3`. And then finally we get a major change and release `1_0_0`.\n",
    "\n",
    "So we have created the sequence \n",
    "- `0_0_0`\n",
    "- `0_0_1`\n",
    "- `0_0_2`\n",
    "- `0_1_0`\n",
    "- `0_1_1`\n",
    "- `0_0_3` \n",
    "- `1_0_0`.\n",
    "\n",
    "Within the job_request.yaml file that is passed to the \"recipe run\" command we probably want to run the latest compatible release of a given minor version.  This can be specified by putting an asterick character (`*`, often called 'star') in as the release number.  The recipe run command will look to see which versions match the major and minor version numbers and will pick the latest release number.  So if you specify\n",
    "\n",
    "    \"recipe-version\": \"0_0_*\"\n",
    "\n",
    "for this sequence of recipe versions we match with\n",
    "- `0_0_0`\n",
    "- `0_0_1`\n",
    "- `0_0_2`\n",
    "- `0_0_3`\n",
    "\n",
    "Since `0_0_3` is the latest version it will be selected. The recipe-version of the recipe that is used is written to the log.\n",
    "\n",
    "Here is an example of a job_request file asking for the latest version where major version is \"0\" and minor version is \"0\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "#!job_request.yaml dag_id=tutorial_02a_1\n",
    "\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_02a_1\n",
    "recipe_version: \"0_0_*\"\n",
    "description: \"A description of the job\"\n",
    "configuration:\n",
    "    email: e.xample@example.com\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can even specify a star for the number of a minor version and the latest minor version with a matching major version will be picked.  So with the sequence of recipe creation shown above, specifying a recipe-version of `\"1_*_*\"`  will result in `\"1_0_0\"` being run since that is the latest release that has a major version number of `\"1\"`.  As a small convenience, if only the major portion of a version number is specified only one star is required, so `\"1_*\"` is the same as `\"1_*_*\"`.\n",
    "\n",
    "A recipe-version string of `\"*_*_1\"` is illegal since it would specify you want release 1 of any version.  Since changing the upper version numbers is supposed to signal some sort of significant or incompatible change asking for a wild card like this would be nonsensical.\n",
    "\n",
    "Major version number changes are supposed to be reserved for changes that would make the results of the recipe incompatible with previous versions, so you cannot specify '\\*' for the major version number.\n",
    "\n",
    "To make this recipe versioning scheme work the `recipe publish` command will refuse to accept recipes whose version falls before the latest version already present in the recipe library.  It is allowable to skip a number, so `0_0_3` can follow `0_0_0` but once `0_0_3` is published there cannot be a `0_0_1` or `0_0_2` added to the library.  Attempting to add `0_0_3` will result in a rejection because that recipe version is already in the library."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the [next tutorial](./running-a-recipe-example.ipynb) we'll look at a step by step example."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
