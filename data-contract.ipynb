{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial: Data Contracts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "This tutorial covers\n",
    "\n",
    "- What a data contract is\n",
    "- Using data contracts to abstract the data\n",
    "- Using the `impala_hook` to access impala format files\n",
    "- Issuing a query in spark\n",
    "- Using a data contract to specify the query\n",
    "- Special characters in identifiers\n",
    "- Adding data contract to example dag\n",
    "- Hooks that support data contracts\n",
    "- Data Contracts used with operators\n",
    "- Changing the type of data source via the job request file\n",
    "- Wildcards for column mapping and column expressions\n",
    "- Connection ID Consistency requirements\n",
    "- No output contract support\n",
    "\n",
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whenever you write an sql query in a recipe to pull some data from an upstream system for processing, you implictly establish an input data contract for your recipe. Input data contracts are part of the recipe interface in the same way as the 'configuration' that you define. However, while metadata configuration is explicit, sql queries in the dag code are hidden from the end user. \n",
    "\n",
    "Recipe metadata lets you make a data contract of your recipe as an explicit part of the recipe interface.\n",
    "\n",
    "Data contracts also allow you to abstract a query from the data source. The query uses aliases to tables and columns which are then defined in the metadata and job request."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Example dag\n",
    "First of all, let's start with a sample dag that has just one task that executes one query using ```ImpalaHook```:\n",
    "\n",
    "> **Note: The hook's connection id used below is deprecated, but is maintained here to show a non-spark example.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import logging\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from airflow.operators.python_operator import PythonOperator\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.hooks.impala_hook import ImpalaHook\n",
    "from dss_airflow_utils.utils import from_config\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'queue': {\n",
    "        \"worker_type\": \"python-worker\",\n",
    "        \"request_memory\": \".5G\",\n",
    "        \"request_cpu\": \".2\"\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def query_impala(*args, **kwargs):\n",
    "    hook = ImpalaHook(conn_id='impala_ndx_uat')\n",
    "\n",
    "    sql = \"\"\"\n",
    "              SELECT t1.dwh_final_baseline, t1.dwh_regular_price, t2.tpr_long_description\n",
    "              FROM rmsnspazuna.tpdw_datawarehouse as t1 \n",
    "              JOIN rmsnspazuna.trtm_time_period as t2 \n",
    "                ON t1.dwh_tpr_id=t2.tpr_id\n",
    "              WHERE t2.tpr_week_last_day='2017-01-07'\n",
    "              LIMIT 5\n",
    "          \"\"\"\n",
    "\n",
    "    logging.info(hook.get_pandas_df(sql))\n",
    "\n",
    "\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_data_contract_step_1', default_args=default_args, \n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        impala_query = PythonOperator(\n",
    "            python_callable=query_impala,\n",
    "            task_id=\"query_impala\",\n",
    "            provide_context=True            \n",
    "        )\n",
    "\n",
    "        return dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The query above implicitly says that in order for the dag to execute successfully, the following two tables must exist at the source where ```impala_ap_uat``` connection id points to:\n",
    "<pre>  1) database name: rmsnspazuna \n",
    "     table name: tpdw_datawarehouse\n",
    "     columns: dwh_tpr_id, dwh_final_baseline, dwh_regular_price</pre>\n",
    "<pre>  2) database name: rmsnspazuna\n",
    "     table name: trtm_time_period\n",
    "     columns: tpr_id, tpr_long_description, tpr_week_last_day</pre>\n",
    "\n",
    "Each table potentially can have more columns, but we don't care about them.\n",
    "Here are metadata and job request to run the above dag:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the metadata.yaml and job_request.yaml to execute the dag above"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "#!metadata.yaml dag_id=tutorial_data_contract_step_1\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_1\n",
    "recipe_version: \"0_0_0\"\n",
    "description: Tutorial data contract step 1\n",
    "configuration:\n",
    "  properties:\n",
    "    email:\n",
    "      type: string\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!job_request.yaml dag_id=tutorial_data_contract_step_1\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_1\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration:\n",
    "    email: \"e.xample@example.com\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem with the above recipe is that by looking at the metadata we don't know that the two tables must exist. Also the dag above hardwires the connection id of the upstream system, so if we want, for example, to switch to a different source, we would need to edit the dag to point to a new connection. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, hardwiring database, table and column names from a specific upstream table might be ugly:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```sql\n",
    "SELECT t1.complex_col_name, t1.abbreviated, t2.specific_to_env, t2.\"with spaces\"\n",
    "FROM specific_table_in_ap_uat1 as t1 JOIN specific_table_in_ap_uat2 as t2 ON t1.id = t2.rid\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's add a data contract to the example dag."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### What is a data contract?\n",
    "\n",
    "Simply put, a data contract is a mapping from an alias to a table definition. An alias is a string that will be used in the query instead of the actual table name. The table definition specifies connection id, database name, table name and column mappings. Column mappings themselves are a map of column alias to column name, where column alias is the string value that will be used in the query and column name is the name of the column at the source.\n",
    "Here is an example of an \"empty\" data contract entry that defines one table:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Empty data contract entry:\n",
    "```yaml\n",
    "table_alias:\n",
    "  connection_id: \n",
    "  db: \n",
    "  table: \n",
    "  columns:\n",
    "    column_alias1: \n",
    "    column_alias2: \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Example where the values should go:\n",
    "```yaml\n",
    "table_alias:\n",
    "  connection_id: <string value>\n",
    "  db: <string value>\n",
    "  table: <string value>\n",
    "  columns:\n",
    "    column_alias1: <string value>\n",
    "    column_alias2: <string value>\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data contracts are specified in both recipe metadata and job request.\n",
    "Recipe metadata defines which aliases are used in the recipe and can optionally provide default values for them. If any of the values are not set in the recipe metadata, then the job request must provide them. Job request can also overwrite any of the default values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Technically, the empty data contract example above is invalid. If there is no default value for *connection_id, db* or *table* field, that field must be omitted altogether. Here is an example of a minimal valid entry that defines a table alias ```table_alias``` that has only one column ```col_alias```:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "table_alias:\n",
    "  columns:\n",
    "    col_alias: \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Note on special characters in identifiers:\n",
    "Table alias, connection id value, db name, table name, column mapping alias and column mapping value are all identifiers. While it is possible to have identifiers that contain special characaters (e.g., a space ' '), it is highly recommended to avoid them. We understand that you as a recipe developer don't control the upstream sources, so you might have no choice with the upstream database, table and column names. \n",
    "\n",
    "Data contracts automatically quote values for database and table names, however, column values are left intact. \n",
    "\n",
    "For example, this is a **valid** data contract entry:\n",
    "```yaml\n",
    "table1: \n",
    "  connection_id: some_connection\n",
    "  db: my database with spaces\n",
    "  table: my table with spaces\n",
    "  columns:\n",
    "    col1: my_column_no_spaces\n",
    "```\n",
    "\n",
    "while the following data contract entry is <span style=\"color:red\">**invalid**</span>:\n",
    "```yaml\n",
    "table1: \n",
    "  connection_id: some_connection\n",
    "  db: my database with spaces\n",
    "  table: my table with spaces\n",
    "  columns:\n",
    "    col1: my column with spaces     # INVALID\n",
    "```\n",
    "\n",
    "to fix the problem, column values could be wrapped into ```{}```, like this:\n",
    "while the following data contract entry is **valid**:\n",
    "```yaml\n",
    "table1: \n",
    "  connection_id: some_connection\n",
    "  db: my database with spaces\n",
    "  table: my table with spaces\n",
    "  columns:\n",
    "    col1: {my column with spaces}     # VALID\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Adding data contract to example dag\n",
    "\n",
    "To add data contracts to a recipe, we need to update dag.py, metadata.yaml and job_request.yaml files as shown below. The main difference is in how we write the query now. Instead of using database, table and column names as they are named at the source, we write the query using more eye friendly aliases. We then define those aliases with metadata.yaml with possible default values. And then specify the exact database, table and column names in the job request if needed.\n",
    "\n",
    "> **Note: The hook's connection id used below is deprecated, but is maintained here to show a non-spark example.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#!dag.py dag_id=tutorial_data_contract_step_2\n",
    "\n",
    "import os\n",
    "import logging\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from airflow.operators.python_operator import PythonOperator\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.hooks.impala_hook import ImpalaHook\n",
    "from dss_airflow_utils.utils import from_config\n",
    "\n",
    "# This utility function help to extract data contract aliases from job request\n",
    "from dss_airflow_utils.data_contract import get_input_data_contract\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'pool': 'dss-testing-pool',\n",
    "    'queue': {\n",
    "        \"worker_type\": \"python-worker\",\n",
    "        \"request_memory\": \".5G\",\n",
    "        \"request_cpu\": \".2\"\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def run_original_query():\n",
    "    hook = ImpalaHook(conn_id='impala_ndx_uat')\n",
    "\n",
    "    sql = \"\"\"\n",
    "              SELECT t1.dwh_final_baseline, t1.dwh_regular_price, t2.tpr_long_description\n",
    "              FROM rmsnspazuna.tpdw_datawarehouse as t1 \n",
    "              JOIN rmsnspazuna.trtm_time_period as t2 \n",
    "                ON t1.dwh_tpr_id=t2.tpr_id\n",
    "              WHERE t2.tpr_week_last_day='2017-01-07'\n",
    "              LIMIT 5\n",
    "          \"\"\"\n",
    "\n",
    "\n",
    "    pdf = hook.get_pandas_df(sql)\n",
    "    logging.info(pdf)\n",
    "    return pdf\n",
    "\n",
    "\n",
    "def query_sql_source(*args, **context):    \n",
    "    # Instead of using database, table and column names as they are at the source, we write the query\n",
    "    # using more eye friendly aliases for them.\n",
    "    sql = \"\"\"\n",
    "              SELECT t1.baseline_units, t1.baseline_price, t2.period_description\n",
    "              FROM sales as t1 \n",
    "              JOIN period as t2 \n",
    "                ON t1.period_id=t2.period_id\n",
    "              WHERE t2.week_last_day='2017-01-07'\n",
    "              LIMIT 5\n",
    "          \"\"\"\n",
    "    \n",
    "    # List the aliases that needs to be used together in this query\n",
    "    input_dc_aliases = ['sales', 'period']  \n",
    "    \n",
    "    # Create data contract object that represents those two aliases\n",
    "    input_data_contract = get_input_data_contract(context, input_dc_aliases)\n",
    "    \n",
    "    # Create a hook based on the data contract that know which source to connect to\n",
    "    hook = input_data_contract.get_hook()  \n",
    "\n",
    "    # Run the query and get results as pandas dataframe\n",
    "    new_pdf = hook.get_pandas_df(sql)\n",
    "    logging.info(new_pdf)\n",
    "    \n",
    "    # This is just for testing to make sure that new query returns the same result \n",
    "    original_pdf = run_original_query()\n",
    "    assert np.array_equal(new_pdf.values, original_pdf.values)\n",
    "\n",
    "\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_data_contract_step_2', default_args=default_args, \n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        impala_query = PythonOperator(\n",
    "            python_callable=query_sql_source,\n",
    "            task_id=\"query_sql_source\",\n",
    "            provide_context=True            \n",
    "        )\n",
    "\n",
    "        return dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Metadata with data contract:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!metadata.yaml dag_id=tutorial_data_contract_step_2\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_2\n",
    "recipe_version: \"0_0_0\"\n",
    "description: Tutorial data contract step 2\n",
    "data_contract:\n",
    "  in:\n",
    "    sales:\n",
    "      connection_id: impala_ndx_uat\n",
    "      db: rmsnspazuna\n",
    "      table: tpdw_datawarehouse\n",
    "      columns:\n",
    "        period_id: dwh_tpr_id\n",
    "        baseline_units: dwh_final_baseline\n",
    "        baseline_price: dwh_regular_price\n",
    "    period:\n",
    "      connection_id: impala_ndx_uat\n",
    "      db: rmsnspazuna\n",
    "      table: trtm_time_period\n",
    "      columns:\n",
    "        period_id: tpr_id\n",
    "        period_description: tpr_long_description\n",
    "        week_last_day: tpr_week_last_day\n",
    "configuration:\n",
    "  properties:\n",
    "    email:\n",
    "      type: string\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the metadata.yaml actually defined three aliases. In general, metadata file should list all input tables, then in the code, we can specify which aliases are needed for each query.\n",
    "\n",
    "Because metadata provides default values for all data contract fields, if we are satisfied with those default values, then job request doesn't have to change. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!job_request.yaml dag_id=tutorial_data_contract_step_2\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_2\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration:\n",
    "    email: \"e.xample@example.com\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we could overwrite all or some of default values defined in the metadata yaml.\n",
    "Note that the job request **must** provide values when there are no default.\n",
    "Here is an example of the job request that overwrites only source database and table names for the second alias."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_2\n",
    "recipe_version: \"0_0_0\"\n",
    "data_contract:\n",
    "  in:\n",
    "    sales:\n",
    "      connection_id: impala_ap_uat\n",
    "      db: ap_us_prod\n",
    "      table: retail_trend_sales\n",
    "      columns:\n",
    "        period_id: period_id\n",
    "        baseline_units: baseline_units\n",
    "        baseline_price: baseline_price\n",
    "    period:\n",
    "      connection_id: impala_ap_uat\n",
    "      db: ap_us_prod\n",
    "      table: period\n",
    "      columns:\n",
    "        period_id: period_id\n",
    "        period_description: period_desc\n",
    "        week_last_day: period_end_date\n",
    "configuration:\n",
    "    email: \"e.xample@example.com\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hooks that support data contracts:\n",
    "The following operators from ```dss_airflow_utils.hooks``` module support data contracts:\n",
    "* JdbcHook\n",
    "* OracleHook\n",
    "* MySqlHook\n",
    "* PostgresHook\n",
    "* MsSqlHook\n",
    "* ImpalaHook\n",
    "* SparkHook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Operator use case\n",
    "Data contracts can also be used directly on the operator level. The list  of aliases is specified as ```input_dc_aliases``` argument to the operator constructor. Nothing special needs to be done in the callable function.\n",
    "\n",
    "Here is an example of the SparkOperator in action:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "def query_stuff(spark, context):\n",
    "    spark.sql(\"select my_col1, my_col2 from \"\n",
    "              \"my_table1 join my_table2 using (id)\").take(10)\n",
    "\n",
    "    \n",
    "spark_query = SparkOperator(\n",
    "    func=query_stuff,\n",
    "    task_id=\"query_stuff\",\n",
    "    input_dc_aliases=[\"my_table1\", \"my_table2\"],\n",
    "    spark_master_url=\"local[1]\",\n",
    "    queue={\n",
    "        'worker_type': 'spark-worker',\n",
    "        'request_memory': '1G',\n",
    "        'request_cpu': '1'\n",
    "    },\n",
    "    scale=1.0\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following operators from ```dss_airflow_utils.operators``` module support data contracts:\n",
    "* JdbcOperator,\n",
    "* OracleOperator,\n",
    "* MySqlOperator,\n",
    "* PostgresOperator,\n",
    "* MsSqlOperator\n",
    "* SparkOperator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of how we can use SparkOperator with data contracts to write our query once, but then swtich between NDX and AP data sources via parameters in a job request:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#!dag.py dag_id=tutorial_data_contract_step_3\n",
    "\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from dss_airflow_utils.spark import SparkOperator\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "import logging\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'pool': 'dss-testing-pool',\n",
    "    'queue': {                              \n",
    "        \"worker_type\": \"spark-worker\",     \n",
    "        \"request_memory\": \".5G\",            \n",
    "        \"request_cpu\": \".2\"                 \n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def do_a_select(spark, context):\n",
    "    sql = \"\"\"\n",
    "            SELECT product_id, description \n",
    "            FROM product \n",
    "            LIMIT 2\n",
    "          \"\"\"\n",
    "    \n",
    "    result = spark.sql(sql).toPandas()\n",
    "    logging.info(\"{!r}\".format(result))\n",
    "    \n",
    "    # This is for internal testing\n",
    "    assert len(result) == 2\n",
    "    assert result.columns.tolist() == [\"product_id\", \"description\"]\n",
    "\n",
    "\n",
    "spark_config = SparkConfig(num_nodes=1,\n",
    "                           request_cpu_per_node=1,\n",
    "                           request_memory_gb_per_node=2)\n",
    "\n",
    "\n",
    "@dag_factory(DagConfig(spark_config))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_data_contract_step_3',\n",
    "             default_args=default_args,\n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        t1 = SparkOperator(\n",
    "            task_id='query_one',\n",
    "            func=do_a_select,\n",
    "            input_dc_aliases=[\"product\"],\n",
    "            queue={\n",
    "                \"worker_type\": \"spark-worker\",\n",
    "                \"request_memory\": \"1G\"\n",
    "            },\n",
    "            scale=1.0\n",
    "        )\n",
    "\n",
    "        dag >> t1\n",
    "\n",
    "        return dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!metadata.yaml dag_id=tutorial_data_contract_step_3\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_3\n",
    "recipe_version: \"0_0_0\"\n",
    "description: Tutorial data contract step 3\n",
    "data_contract:\n",
    "  in:\n",
    "    product:\n",
    "      columns:\n",
    "        product_id:\n",
    "        description:\n",
    "configuration:\n",
    "  properties:\n",
    "    email:\n",
    "      type: string\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in the above metadata.yaml, we did not set any default values for the connection_id, database, table or column names. The job request must provide those values.\n",
    "\n",
    "Here is the job request to query NDX source:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!job_request.yaml dag_id=tutorial_data_contract_step_3\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_3\n",
    "recipe_version: \"0_0_0\"\n",
    "data_contract:\n",
    "  in:\n",
    "    product:\n",
    "      connection_id: metastore_ndx_uat_adls\n",
    "      db: rmsnspazuna\n",
    "      table: trpr_item_description\n",
    "      columns:\n",
    "        product_id: itm_id\n",
    "        description: itm_description\n",
    "configuration:\n",
    "    email: \"e.xample@example.com\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here is the job request to query AP source:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_data_contract_step_3\n",
    "recipe_version: \"0_0_0\"\n",
    "data_contract:\n",
    "  in:\n",
    "    product:\n",
    "      connection_id: metastore_ap_uat\n",
    "      db: ap_us_prod\n",
    "      table: product\n",
    "      columns:\n",
    "        product_id: product_id\n",
    "        description: product_short_desc\n",
    "configuration:\n",
    "    email: \"e.xample@example.com\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Replacing source\n",
    "\n",
    "With data contracts it is also possible to change the type of data source via job request. There are five data sources: \n",
    "\n",
    "Database - either sql database or a hive metastore. We've already seen how to provide this kind of source above. \n",
    "```yaml\n",
    "product:\n",
    "  connection_id: metastore_ap_uat\n",
    "  db: ap_us_prod\n",
    "  table: product\n",
    "  columns:\n",
    "    product_id: product_id\n",
    "    description: product_short_desc\n",
    "```\n",
    "Datacache - a file from datacache.\n",
    "```yaml\n",
    "alias:\n",
    "  connection_id: datacache       # note: reserved connection id\n",
    "  query: datacacche_query\n",
    "  tolerance:                     # optional, should match datacache tolerance schema\n",
    "    type: version\n",
    "    value: 1\n",
    "  options:                       # optional spark reader options supported by the format of the file in data cache.\n",
    "    sep: ,                       # for example, a character that separates fields in csv file format. \n",
    "                                 # Note, file format cannot be changed via options since datacache entry relies on datacache client\n",
    "                                 # to read the file and datacache identifies file format by file extension\n",
    "  columns:\n",
    "    col1: col1\n",
    "    col2: col2        \n",
    "```\n",
    "\n",
    "Query - an sql query that the source database engine understands. The source database engine is identified by connection_id. Note that column mappings are not used in this case. The query must return correct column names.\n",
    "```yaml\n",
    "alias:\n",
    "  connection_id: jdbc_based_or_metastore_airflow_connection_id\n",
    "  query: \"sql query here\"\n",
    "```\n",
    "\n",
    "Workspace - a file from dag workspace. File must have an extension and it must be one of the types supported by spark ('csv', 'parquet', 'text', 'orc', 'json').\n",
    "```yaml\n",
    "alias:\n",
    "  connection_id: workspace       # note: reserved connection id\n",
    "  path: path/in/workspace        # relative path in the dag workspace\n",
    "  options:                       # optional spark options to parse the file\n",
    "    sep: ,\n",
    "  columns:\n",
    "    col1: col1\n",
    "    col2: col2         \n",
    "```\n",
    "\n",
    "URI - a complete Uniform Resource Identifier (URI) to a file or directory. This is currently only supported in the Media Research environment to read files directly from the second-tier datalake S3 bucket.\n",
    "```yaml\n",
    "alias:\n",
    "  connection_id: uri                  # note: reserved connection id\n",
    "  uri: s3://my-bucket/data.parquet    # complete URI including the scheme\n",
    "  options:                            # optional spark options to parse the file. By default spark will use parquet data reader\n",
    "    format: parquet                   # but the format can be overriden here, any other spark reader supported options can be provided here\n",
    "  columns:\n",
    "    col1: col1\n",
    "    col2: col2         \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Spark operator/hook case supports all five sources, whereas non spark use case supports only ```Database``` and ```Query``` sources."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advanced usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Star column mapping\n",
    "It is possible to define a 'star' column mapping (```*: *```) in the metadata.yaml file. Such mapping means that all provided columns will be selected \"as-is\" (using names in the source table). Star mappings can also be combined with regular mappings so that all provided columns will be selected, but some columns must be mapped to required aliases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following query:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```sql\n",
    "SELECT *\n",
    "FROM table_alias\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of a metadata data contract for the above query:\n",
    "```yaml\n",
    "table_alias:\n",
    "  columns:\n",
    "    column_alias1: \n",
    "    *: *\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of a job request\n",
    "```yaml\n",
    "table_alias:\n",
    "  connection_id: some_conn_id\n",
    "  db: some_db\n",
    "  table: some_table\n",
    "  columns:\n",
    "    column_alias1: source_col_name     # Must provide a column name in source table for required mapping\n",
    "    other_col: source_other_col        # Optionally can rename some of the columns in the table\n",
    "    other2: some_wierd_col_name\n",
    "    *: *                               # Optionally can say to select the rest of the columns \"as-is\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assuming that the source table ```some_db.some_table``` has the following columns:\n",
    "\n",
    "\n",
    "```first_column | source_col_name | col2 | source_other_col| some_wierd_col_name | last_column```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result of the query given the above job request would have the following columns:\n",
    "\n",
    "```first_column | col2 | last_column| column_alias1 | other_col | other2```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the job request did not specify the star mapping, then the result of the query would only be 3 columns:\n",
    "\n",
    "```column_alias1 | other_col | other2```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Column expressions\n",
    "\n",
    "The right-hand side of a column mapping allows the user to specify column expresssions that are valid at the data source. For example, if the source is a spark table, then spark functions can be used. If the source is a Postgres database, then postgres specific functions can be used. \n",
    "\n",
    "Here are several examples: \n",
    "\n",
    "Concatenate two columns from a Postgres source:\n",
    "```yaml\n",
    "table_alias:\n",
    "  columns:\n",
    "    col_alias: concat(source_col1, source_col2)\n",
    "```\n",
    "\n",
    "Extract the quarter of a given date as an integer from a Spark source:\n",
    "```yaml\n",
    "table_alias:\n",
    "  columns:\n",
    "    col_alias: quarter(source_col1)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Something to keep in mind\n",
    "\n",
    "#### Connection id consistency\n",
    "When a query relies on more than one table, it is currently not supported to specify conneciton ids to different upstream sources for those tables. For example, the following data contract is <span style=\"color:red\">**invalid**</span>:\n",
    "\n",
    "<div style=\"background:red\">\n",
    "    \n",
    "```yaml\n",
    "table1:\n",
    "  connection_id: connection1\n",
    "table2:\n",
    "  connection_id: connection2\n",
    "```\n",
    "\n",
    "</div>\n",
    "\n",
    "However, it is possible to mix \"native\" airflow connection ids with the reserved data sources mentioned above (datacache, workspace, and query). For example, the following data contract is **valid** for either SQL or Spark use case:\n",
    "```yaml\n",
    "table1:\n",
    "  connection_id: connection1\n",
    "table2:\n",
    "  connection_id: query\n",
    "  query: select * from source_table\n",
    "```\n",
    "\n",
    "\n",
    "And the following example is **valid** for a Spark use case:\n",
    "```yaml\n",
    "table1:\n",
    "  connection_id: connection1\n",
    "table2:\n",
    "  connection_id: workspace\n",
    "  path: my/path\n",
    "table3:\n",
    "  connection_id: datacache\n",
    "  query: datacache_query\n",
    "table4:\n",
    "  connection_id: query\n",
    "  query: select * from source_table\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Output contracts\n",
    "There is no support for output contracts yet.  Each DAG run gets its own [workspace](./workspace-and-xcom-push.ipynb) so each run can write to the same base filename and different files will be created.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "****\n",
    "\n",
    "In the next lesson, [The Datacache Client](./dss-datacache-client.ipynb), we will look in more detail at how data can be persisted through the datacache."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}