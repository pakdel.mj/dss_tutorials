import sys
import os
import pytest
from pyspark.sql import SparkSession
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression

if sys.version[0] == '3':
    # this environment variable is required in Python 3.x
    os.environ['PYSPARK_PYTHON']='/usr/bin/python3'
    os.environ['PYSPARK_DRIVER_PYTHON']='/usr/bin/python3'
    # mock got moved in unittest in python 3
    import unittest.mock as mock
else:
    import mock


# Import of the custom modules should start with the '...dag'.
from ...dag.dag import (create_query,
                        get_dataframe,
                        compute_means,
                        do_analysis,
                        get_inputs,
                        builder_func,
                        task_function)

# Test functions must start with "test_" prefix
def test_create_query():
    # query as extracted from the metadata after it was written to the datacache
    # The create_query function nust return exactly this value for it to match
    # when we call get() on the datacache.
    query=( "select a.*, \n"
            "       case \n"
            "          when promo_pri_index < 0.95 then 1 \n"
            "          else 0 \n"
            "       end as tpr /* indicator of a price reduction greater than 5% */\n"
            "from \n"
            "(select str.nielsen_scantrack_mkt_code as Scantrack_Code,\n"
            "       str.nielsen_scantrack_mkt_name as Scantrack_Market, \n"
            "       str.store_name as Banner,\n"
            "       str.pod_id,\n"
            "       to_date(substring(fct.period_description_short,\n"
            "                  length(fct.period_description_short)-8, 9), 'MM/dd/yy') as period,\n"
            "       fct.product_key,\n"
            "       fct.units,\n"
            "       fct.dol,\n"
            "       fct.base_units,\n"
            "       fct.base_dol,\n"
            "       (fct.dol / fct.units) as act_pri_uni,             /* actual price per unit */\n"
            "       (fct.Base_DOL / fct.Base_Units) as base_pri_uni,  /* base price per unit */\n"
            "       case\n"
            "          when (fct.dol / fct.units) / (fct.Base_DOL / fct.Base_Units)  > 1.0 then 1.0\n"
            "          else (fct.dol / fct.units) / (fct.Base_DOL / fct.Base_Units)\n"
            "       end as promo_pri_index  /* in case had problems with round() function */\n"
            "from aod_pg_prototxi_02.str\n"
            "inner join aod_pg_prototxi_02.fct\n"
            "   on fct.pod_id = str.pod_id\n"
            "inner join aod_pg_prototxi_02.prdc\n"
            "   on prdc.product_key = fct.product_key\n"
            "where str.channel like '%Grocery - Supermarket-Conventional%'\n"
            "  and str.store_name = 'Albertsons'\n"
            "  and str.nielsen_scantrack_mkt_code not like '%NOT APPLICABLE%'\n"
            "  and prdc.pg_category like '%LAUNDRY CARE%'\n"
            "  and (fct.product_key = 72295087)\n"
            "  and fct.units > 0\n"
            "  and fct.dol > 0\n"
            "  and fct.base_units > 0\n"
            "  and fct.base_dol > 0\n"
            "  and (fct.dol / fct.units) > 0.10  /* restrict to reasonable prices */\n"
            "  ) as a\n")
    
    sql = create_query({
        'sales_database': 'aod_pg_prototxi_02',
        'catg': 'LAUNDRY CARE',
        'channel': 'Grocery - Supermarket-Conventional',
        'banner': 'Albertsons',
        'product_keys': [ 72295087 ]
    })
    if query != sql:
        for n, (qc, sc) in enumerate(zip(query, sql)):
            if qc != sc:
                print('difference at character {}'.format(n))
                print('q {}^{}^{}'.format(query[n-9:n],query[n],query[n+1:n+10]))
                print('s {}^{}^{}'.format(sql[n-9:n],sql[n],sql[n+1:n+10]))
                break
                  
    assert query == create_query({
        'sales_database': 'aod_pg_prototxi_02',
        'catg': 'LAUNDRY CARE',
        'channel': 'Grocery - Supermarket-Conventional',
        'banner': 'Albertsons',
        'product_keys': [ 72295087 ]
    })
    

def get_fake_context():
    class fake:
        conf = {"configuration": {
                'sales_database': 'aod_pg_prototxi_02',
                'catg': 'LAUNDRY CARE',
                'channel': 'Grocery - Supermarket-Conventional',
                'banner': 'Albertsons',
                'product_keys': [ 72295087 ]
        }}
    fake_context = {'dag_run': fake()}
    return fake_context


def test_get_inputs():
    result = get_inputs(get_fake_context())
    assert result['sales_database'] == 'aod_pg_prototxi_02'
    assert result['catg'] == 'LAUNDRY CARE'
    assert result['channel'] == 'Grocery - Supermarket-Conventional'
    assert result['banner'] == 'Albertsons'
    assert result['product_keys'] == [ 72295087 ]

    
def test_builder_func():
    result = builder_func({})
    assert len(result) == 6
    assert result['spark.dynamicAllocation.maxExecutors'] == '5'
    assert result['spark.executor.memory'] == '4G'
    assert result['spark.executor.cores'] == '2'
    assert result['spark.driver.memory'] == '4G'
    assert result['spark.driver.maxResultSize'] == '4G'
    assert result['spark.executor.logs.rolling.maxRetainedFiles'] == '1'

def spark_and_data():
    colnames = ['Scantrack_Code', 'Scantrack_Market', 'Banner', 'pod_id',
                'period', 'product_key', 'units', 'dol', 'base_units',
                'base_dol', 'act_pri_uni', 'base_pri_uni', 'promo_pri_index',
                'tpr']
    dat = [
           (42, "Salt Lake City-Boise", "Albertsons",  234134, "2018-11-17",
            72295087, 1.0, 5.39, 0.737, 3.97, 5.39, 5.386702849389417, 1.0000, 0),
           (42, "Salt Lake City-Boise", "Albertsons",   43911, "2018-10-27",
            72295087, 1.0, 5.99, 0.982, 5.88, 5.99, 5.987780040733197, 1.0000, 0),
           (42, "Salt Lake City-Boise", "Albertsons",   10017, "2018-11-17",
            72295087, 3.0,16.17, 2.703,14.57, 5.39, 5.390307066222716, 0.9999, 0),
           (42, "Salt Lake City-Boise", "Albertsons",  181332, "2018-11-03",
            72295087, 8.0, 24.0, 1.08,  5.82, 3.00, 5.388888888888888, 0.5567, 1),
           (36, "             Phoenix", "Albertsons", 5037047, "2018-11-03",
            72295087,11.0,38.17, 1.128, 6.75, 3.47, 5.984042553191490, 0.5798, 1),
           (42, "Salt Lake City-Boise", "Albertsons",   10835, "2018-10-27",
            72295087, 2.0,10.78, 1.75,  9.43, 5.39, 5.388571428571429, 1.0000, 0),
           (42, "Salt Lake City-Boise", "Albertsons",  234134, "2018-12-22",
            72295087, 1.0, 5.39, 1.50,  8.08, 5.39, 5.386666666666667, 1.0000, 0),
           (51, "           Las Vegas", "Albertsons",   36150, "2018-11-03",
            72295087, 7.0,24.29, 0.05,  0.30, 3.47, 5.999999999999999, 0.5783, 1),
           (42, "Salt Lake City-Boise", "Albertsons",   70203, "2018-12-01",
            72295087, 1.0, 5.39, 1.644, 8.86, 5.39, 5.389294403892944, 1.0000, 0),
           (42, "Salt Lake City-Boise", "Albertsons",   10671, "2018-12-08",
            72295087, 1.0, 3.15, 1.293, 6.97, 3.15, 5.390564578499613, 0.5843, 1),
           (42, "Salt Lake City-Boise", "Albertsons",  181332, "2018-12-22",
            72295087, 1.0, 5.39, 1.343, 7.24, 5.39, 5.390915860014892, 0.9998, 0),
           (42, "Salt Lake City-Boise", "Albertsons",   10566, "2018-12-15",
            72295087, 2.0,10.78, 0.726, 3.91, 5.39, 5.385674931129477, 1.0000, 0),
           (13, "              Denver", "Albertsons",    8331, "2018-11-10",
            72295087, 3.0,14.97, 1.715, 7.70, 4.99, 4.489795918367347, 1.0000, 0),
           (51, "           Las Vegas", "Albertsons", 5189772, "2018-11-03",
            72295087, 5.0,17.35, 0.017, 0.10, 3.47, 5.882352941176470, 0.5899, 1),
           (51, "           Las Vegas", "Albertsons",  264078, "2018-11-10",
            72295087, 1.0, 3.47, 0.239, 1.43, 3.47, 5.983263598326360, 0.5790, 1),
           (42, "Salt Lake City-Boise", "Albertsons",  294950, "2018-12-29",
            72295087, 2.0,11.98, 2.166,12.97, 5.99, 5.987996306555864, 1.0000, 0),
           (42, "Salt Lake City-Boise", "Albertsons",   43912, "2018-11-03",
            72295087, 4.0,20.00, 1.565, 9.37, 5.00, 5.987220447284344, 0.8351, 1),
           (51, "           Las Vegas", "Albertsons",  341654, "2018-10-27",
            72295087, 2.0, 6.94, 0.396, 2.37, 3.47, 5.984848484848484, 0.5797, 1),
           (42, "Salt Lake City-Boise", "Albertsons",  190785, "2018-12-22",
            72295087, 1.0, 5.39, 0.618, 3.33, 5.39, 5.388349514563107, 1.0000, 0),
           (42, "Salt Lake City-Boise", "Albertsons",    9813, "2018-11-24",
            72295087, 1.0, 5.99, 0.385, 2.30, 5.99, 5.974025974025974, 1.0000, 0),
          ]

    spark = (SparkSession.builder
             .master("local[1]")
             .appName("test_end_to_end_example")
             .config('spark.dynamicAllocation.maxExecutors', '4')
             .config('spark.executor.memory', '4G')
             .config('spark.executor.cores', '2')
             .config('spark.driver.memory', '4G')
             .config('spark.driver.maxResultSize', '4G')
             .config('spark.executor.logs.rolling.maxRetainedFiles', '1')
             .getOrCreate())
    
    return spark.createDataFrame(dat, colnames), spark
    

def test_compute_means():
    df, spark = spark_and_data()
    df = compute_means(df, spark)
    assert(df.count() == 20)
    assert('ln_price_tpr_c' in df.columns)
    spark.stop()


@mock.patch('dss_datacache_client.client.get')
def test_run_analysis(mock_client_get):
    df, spark = spark_and_data()

    #mock_get_dataframe.return_value = spark.createDataFrame(dat, colnames)
    class MockMetadata:
        def to_spark(self, spark):
            return df
    mock_client_get.return_value = MockMetadata()


    fake_context = get_fake_context()
    task_function(spark, fake_context)
    
    spark.stop()

