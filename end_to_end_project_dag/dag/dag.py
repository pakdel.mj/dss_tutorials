# import from the python standard library
import textwrap
import logging
from datetime import datetime, timedelta

# imports from downloaded packages
from airflow import DAG

# these imports are used by Airflow's DAG model
from dss_airflow_utils.spark import SparkOperator, SparkConfig
from dss_airflow_utils.dag_factory import dag_factory, DagConfig
from dss_airflow_utils.workspace_utils import path_in_workspace
from dss_airflow_utils.utils import get_config
from dss_datacache_client import client

# imports for spark functions
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression


# These are some default arguments that will be passed to each task.
# You can override them on a task basis if needed.
DEFAULT_ARGS = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 0, # use 0 while debugging, increase for production
    'retry_delay': timedelta(seconds=10),
    # queue must be either a string or a dictionary of string -> string
    'queue': {
        # here you can ask for specific worker container
        "worker_type": "python-worker",
        # and how much maximum computing resource a task will need
        "request_memory": "16G",
        "request_cpu": "4"
    }
}


def get_inputs(context):
    """ read in data passed from the job request yaml file"""
    inputs = {}
    config = get_config(context)
    for key in ['sales_database',
                'catg',
                'channel',
                'banner',
                'product_keys'
               ]:
        inputs[key] = config[key]
    return inputs


def create_query(inputs):
    return textwrap.dedent("""\
        select a.*, 
               case 
                  when promo_pri_index < 0.95 then 1 
                  else 0 
               end as tpr /* indicator of a price reduction greater than 5% */
        from 
        (select str.nielsen_scantrack_mkt_code as Scantrack_Code,
               str.nielsen_scantrack_mkt_name as Scantrack_Market, 
               str.store_name as Banner,
               str.pod_id,
               to_date(substring(fct.period_description_short,
                          length(fct.period_description_short)-8, 9), 'MM/dd/yy') as period,
               fct.product_key,
               fct.units,
               fct.dol,
               fct.base_units,
               fct.base_dol,
               (fct.dol / fct.units) as act_pri_uni,             /* actual price per unit */
               (fct.Base_DOL / fct.Base_Units) as base_pri_uni,  /* base price per unit */
               case
                  when (fct.dol / fct.units) / (fct.Base_DOL / fct.Base_Units)  > 1.0 then 1.0
                  else (fct.dol / fct.units) / (fct.Base_DOL / fct.Base_Units)
               end as promo_pri_index  /* in case had problems with round() function */
        from {db}.str
        inner join {db}.fct
           on fct.pod_id = str.pod_id
        inner join {db}.prdc
           on prdc.product_key = fct.product_key
        where str.channel like '%{channel}%'
          and str.store_name = '{banner}'
          and str.nielsen_scantrack_mkt_code not like '%NOT APPLICABLE%'
          and prdc.pg_category like '%{category}%'
          and ({products})
          and fct.units > 0
          and fct.dol > 0
          and fct.base_units > 0
          and fct.base_dol > 0
          and (fct.dol / fct.units) > 0.10  /* restrict to reasonable prices */
          ) as a
        """).format(db=inputs['sales_database'],
                    channel=inputs['channel'],
                    banner=inputs['banner'],
                    category=inputs['catg'],
                    products=' OR '.join(["fct.product_key = {}".format(p)
                                          for p in inputs['product_keys']])
                   )


def get_dataframe(sql, spark):
    """ Take in spark SQL statement and the spark context and return
        the data as a spark data frame. """
    CACHE_ONLY = {"type": "range", "to": "latest_cached"}
    meta_data = client.get(query=sql,
                           # no hook needed since we always go to cache, never to database
                           #hook=SparkHook(conn_id="metastore_ndx_uat_adls")
                           tolerance=CACHE_ONLY)
    if meta_data is None:
        logging.error("Failed to find cached data for sql: %s", sql)
        raise Exception
    sdf = meta_data.to_spark(spark)
    sdf.cache()
    return sdf


def compute_means(sdf, spark):
    sdf.registerTempTable('tbl')

    # log our units and price variables
    # compute interaction variable
    # also drop outliers we identified in exploratory analyses (not shown here)
    nsdf0 = spark.sql("""
      select *,
        log(units) as ln_units,
        log(act_pri_uni) as ln_price,
        log(base_pri_uni) as ln_base_price,
        log(promo_pri_index) as ln_ppi,
        log(act_pri_uni) * tpr as ln_price_tpr
      from tbl
      where act_pri_uni >= 2.25 /* one record at 99 cents, one at $2.23, rest $3.00+ */
        and units < 95          /* one very high outlier */
    """)
    nsdf0.createOrReplaceTempView('nsdf_v')

    sql = textwrap.dedent("""\
    select pod_id,
           mean(ln_units)      as avg_ln_units,
           mean(ln_price)      as avg_ln_price,
           mean(ln_base_price) as avg_ln_base_price,
           mean(ln_ppi)        as avg_ln_ppi,
           mean(tpr)           as avg_tpr,
           mean(ln_price_tpr)  as avg_ln_price_tpr
           from nsdf_v
           group by pod_id
    """)
    nsdf_means = spark.sql(sql)
    nsdf_means.createOrReplaceTempView('nsdf_means_v')

    # subtract the means
    sql = textwrap.dedent("""\
    select a.Scantrack_Code, a.Scantrack_Market, a.Banner, a.pod_id, a.period, a.product_key,
           a.units, a.dol, a.base_units, a.base_dol, a.act_pri_uni, a.base_pri_uni,
           a.promo_pri_index, a.tpr,
           (a.ln_units - b.avg_ln_units) as ln_units_c,
           (a.ln_price - b.avg_ln_price) as ln_price_c,
           (a.ln_base_price - b.avg_ln_base_price) as ln_base_price_c,
           (a.ln_ppi - b.avg_ln_ppi) as ln_ppi_c,
           (a.tpr - b.avg_tpr) as tpr_c,
           (a.ln_price_tpr - b.avg_ln_price_tpr) as ln_price_tpr_c
    from nsdf_v as a
    left join nsdf_means_v as b
       on b.pod_id = a.pod_id
    """)

    return spark.sql(sql)


def do_analysis(sdf, spark, outstream):
    """ Do the analysis on the passed in sdf (a spark data frame), and spark
        (the spark context object) and write the results to outstream.
    """
    nsdf = compute_means(sdf, spark)

    my_predictors = ['ln_price_c', 'tpr_c', 'ln_price_tpr_c']

    vectorAssembler = VectorAssembler(inputCols=my_predictors, outputCol='features')
    va_nsdf = vectorAssembler.transform(nsdf)
    va_nsdf = va_nsdf.select(['ln_units_c', 'features', 'pod_id', 'period', 'tpr', 'Scantrack_Market'])

    lr = LinearRegression(featuresCol='features',
                          labelCol='ln_units_c',
                          maxIter=150,
                          regParam=0.0,
                          elasticNetParam=0.0,
                          fitIntercept=True
                         )
    lr_model = lr.fit(va_nsdf)

    outstream.write("           RMSE: {:f}\n".format(lr_model.summary.rootMeanSquaredError))
    outstream.write("      R-squared: {:f}\n".format(lr_model.summary.r2))

    outstream.write("Coefficients for {}\n".format(my_predictors))
    outstream.write("   Coefficients: [{}]\n".format(', '.join(['{:f}'.format(c) for c in lr_model.coefficients])))
    outstream.write("      Intercept: {:f}\n".format(lr_model.intercept))

    return lr_model.summary.predictions, lr_model.summary.residuals

def ignore(_):
    # Shut up complaints about unused variables
    return True


def task_function(spark, context):
    """ This function runs the analysis.  It is called by the task in the DAG."""
    ignore(context)
    path_to_results = path_in_workspace('ModelResults.txt')
    logging.info("Writing results to {}".format(path_to_results))
    with open(path_to_results, "w") as f:
        sql = create_query(get_inputs(context))
        df = get_dataframe(sql, spark)
        do_analysis(df, spark, f)


# spark builder function
def builder_func(unused_airflow_context):
    """ A builder function used in a DAG takes in the Airflow context object
        and returns a dictionary with spark properties as keys and the
        associated attribute as values. In comments are the lines from
        the notebook version of the builder_func.

        Make sure that your SparkConfig parameters are large enough to
        hold the driver and executors specified here.
        If you ask for too much you get a message in the log like:
        ValueError: Requested 6.0 G of memory per executor.  The spark
        cluster can only allocate 4.0 G of memory per executor.

    """
    prop_dict = {}
    prop_dict['spark.dynamicAllocation.maxExecutors'] = '5'
    prop_dict['spark.executor.memory'] = '4G'
    prop_dict['spark.executor.cores'] = '2'
    prop_dict['spark.driver.memory'] = '4G'
    prop_dict['spark.driver.maxResultSize'] = '4G'
    prop_dict['spark.executor.logs.rolling.maxRetainedFiles'] = '1'
    return prop_dict


# If you ask for too many nodes, or too much memory or cpu, the request
# will be reduced to the maximum allowed. The default values are the
# minimums, 1 cpu on 1 node and 2 gigibytes of memory.  You definitely
# want to specify these to be something larger.

# This decorator is required, so do not touch it!
# Don't put anything between the decorator and def keyword
@dag_factory(DagConfig(SparkConfig(num_nodes=3,
                                   request_cpu_per_node=6,
                                   request_memory_gb_per_node='4G')))
def create_dag():
    with DAG(dag_id='End2EndProjectDAG',
             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:

        # Size of Spark Driver.  The spark driver is a spark node that is spun up to
        # handle non-parallel parts of the application.  It typically will have
        # just one CPU. The amount of work it does grows with the size of the cluster.
        spark_driver_config = {
            "worker_type": "spark-worker",
            "request_memory": "4G",
            "request_cpu": "1"
        }

        SparkOperator(
            task_id='the_task',
            func=task_function,
            queue=spark_driver_config,
            spark_confs=builder_func # spark executors
        )

        return dag  # Do not change this
