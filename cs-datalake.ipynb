{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CSDatalakeOperator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*****\n",
    "\n",
    "This tutorial covers:\n",
    "-   [How it works](#How-it-works)\n",
    "-   [Limitations](#Limitations)\n",
    "    \n",
    "*****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *CSDatalakeOperator* allows you to write data to the Connected Systems (CS) Datalake via the Central Data Management (CDM) workflow and register your datasets with the CS Hive metastore. For more information on how CDM works, please visit their confluence page at https://adlm.nielsen.com/confluence/display/public/CDM/Central+Data+Management"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How it works"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*CSDatalakeOperator* (*dss_airflow_utils.operators.cs_datalake_operator.CSDatalakeOperator*) requires a *python_callable* to be passed as the input argument. The callable should be a python function that accepts two input arguments - spark session and airflow context, similarly to SparkOperator, and returns an instance of the CSDatalake class (*dss_airflow_utils.cs_datalake.cs_datalake.CSDatalake*).\n",
    "\n",
    "The *CSDatalake* requires three main input arguments:\n",
    "- dataset_metadata (*dss_airflow_utils.cs_datalake.models.DataSetMetaData*) - defines the dataset profile attributes to be associated with the data when ingested into CDM. This will be internally passed to Data Catalog for Validation and will create unique dataset id. For explanation of the dataset metadata attributes please see: https://adlm.nielsen.com/confluence/display/NCTECH/Data+Profile+Attributes\n",
    "- data_source (*dss_airflow_utils.cs_datalake.data_source.DataSource*) - defines the mappings between the desired table name and the actual data for each table\n",
    "- target_database_configuration (*dss_airflow_utils.cs_datalake.models.TargetDatabaseConfiguration*) - defines the name and the configuration parameters, such as language, compression, and database name."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is the code snippet of the operator in action where we register one spark dataframe as a dataset in CS datalake with one table.\n",
    "\n",
    "First, we describe the dataset using the DataSetMetaData class. Then, the code assumes the upstream task, named 'create_data', wrote data to the datacache and pushed datacache metadata object to airflow xcom. So, we convert the datacache entry into the spark dataframe, 'sdf'. We then create an instance of the DataSource class that will include only one entry - an instance of the SparkDataSourceEntry class. Note, that you can provide multiple data source entries at once that will be registered as part of one dataset. Finally, we ceate an instance of the TargetDatabaseConfiguration class where we specify the name of the database we want to be created in the CS Hive metastore."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "from dss_airflow_utils.operators.cs_datalake_operator import CSDatalakeOperator\n",
    "\n",
    "...\n",
    "\n",
    "def ingest_data(spark, context):\n",
    "    from dss_airflow_utils.cs_datalake.models import (\n",
    "        DataSetMetaData,\n",
    "        TargetDatabaseConfiguration\n",
    "    )\n",
    "    from dss_airflow_utils.cs_datalake.data_source import (\n",
    "        DataSource, SparkDataSourceEntry\n",
    "    )\n",
    "    from dss_airflow_utils.cs_datalake.cs_datalake import CSDatalake\n",
    "\n",
    "    dataset_metadata = DataSetMetaData(dataset_name=DATASET_NAME,\n",
    "                                       dataset_owner_org='Nielsen Global',\n",
    "                                       dataset_source='Third Party',\n",
    "                                       dataset_type='RAW',\n",
    "                                       solution='NOT APPLICABLE',\n",
    "                                       dataset_owner_user=REAL_NIELSEN_EMAIL)\n",
    "\n",
    "    m = context['ti'].xcom_pull('create_data')\n",
    "    sdf = m.to_spark(spark)\n",
    "    \n",
    "    data_source = DataSource(SparkDataSourceEntry(table_name=TABLE_NAME, data=sdf))\n",
    "\n",
    "    target_database_configuration = TargetDatabaseConfiguration(target_database=DATABASE_NAME)\n",
    "\n",
    "    return CSDatalake(dataset_metadata, data_source, target_database_configuration, overwrite=True)\n",
    "\n",
    "...\n",
    "\n",
    "ingest_data_task = CSDatalakeOperator(\n",
    "    python_callable=ingest_data,\n",
    "    task_id='ingest_data'\n",
    ")\n",
    "\n",
    "...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NOTE**: Since all recipes running in DSS, *regardless of the tenant*, communicate to the same CS datalake and the same CDM, the *dataset_name* in the *DataSetMetaData* class and the *target_database* in the *TargetDatabaseConfiguration* class are **not** namespaced in any way. Meaning, if two recipes define the same dataset_name and/or target_database, they will override each other depending on their execution order. Therefore, we strongly suggest to prefix the dataset_name and target_database values with tenant and recipe id."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Limitations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- only spark dataframes can be ingested into CS datalake\n",
    "- there is no way to update just one table in the existing dataset, all tables will need to be re-ingested\n",
    "- when your dag runs in DSS **ProdI** environment, CSDatalakeOperator will register your data with CDM **Non-Prod** environment\n",
    "- when your dag runs in DSS **ProdS** environment,  CSDatalakeOperator will register your data with CDM **Prod** environment"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
