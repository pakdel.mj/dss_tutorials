{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial: The Datacache Client\n",
    "## For both Airflow DAGs and Notebook Usage\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "This tutorial covers:\n",
    "\n",
    "- What the datacache client is and its uses\n",
    "- Interactive session help\n",
    "- Get and Put from/to the Datacache\n",
    "- The DatacacheMetadata object and retrieving data\n",
    "- Warning for the Spark Dataframes Use Case\n",
    "- Examples of the Datacache\n",
    "- Example of passing table information in the job request\n",
    "\n",
    "***\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook shows an example of how to use a datacache client to interrogate data cached in the datacache. The implementation of the datacache client allows users to use the same functionality either from a PythonOperator in a DAG or from the Python Notebook.\n",
    "\n",
    "The datacache client can be used to read/write data from/to the datacache. The datacache is populated with the 'get' and 'put'. Both the get and put functions return a DatacacheMetadata object.\n",
    "\n",
    "## Get\n",
    "### dss_datacache_client.client.get(query, tolerance, hook) -> DatacacheMetadata\n",
    "\n",
    "> Parameters are: query, tolerance, hook. You can get more information about the parameters by executing help(client.get).\n",
    "\n",
    "dss_datacache_client.client.get() method can be used to search for specific data cache entries. This function works like a cache-through system: if the query is not found in the datacache, we will execute it and populate the datacache with the result.\n",
    "\n",
    "### WARNING: This function only works if you provide an Airflow Hook!\n",
    "The hook needs to be instantiated before using this feature. The hook is required so that we know:\n",
    "- How/where to execute the query\n",
    "- Apply a data contract to the query if the hook supports it\n",
    "\n",
    "\n",
    "> If the query+tolerance match an entry, a DatacacheMetadata object is returned. If there are no records in the datacache that match provided parameters, then None is returned.\n",
    "\n",
    "\n",
    "#### Examples of tolerance values:\n",
    "- To always udate the datacache data:\n",
    "tolerance={'type': 'latest'}\n",
    "\n",
    "- To get the latest stored data for a query (WARNING: careful if other users are using the same query, they might update your expected value):\n",
    "tolerance={'type': 'range', 'to': 'latest'}\n",
    "\n",
    "- Get a known version of the data (More robust way!):\n",
    "tolerance={'type': 'version', 'value': 1}\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Arguments\n",
    "\n",
    "\n",
    "| argument name | description |\n",
    "|:---:|:---|\n",
    "| `task_id` | This is common among all operators, it's used as part of a composite key along with the DAG id and the execution time to identify this task |\n",
    "| `hook_type` | Hook represent the type connection.  Currently Analytics Services has `DummyHook`, `NBASFtpHook`, `NBASFtpSHook`, `HiveMetaHook`, `ImpalaHook`, and `SparkHook`.  If the hook is not specified data that was stored in the datacache with a previous call to put can be retrieved but no data will be written to the cache and data contracts (see tutorial 10-data-contract) will not be applied to any search. |\n",
    "| `conn_id` | This is the connection string to the upstream service. Airflow stores predefined connection string you can use in the Airflow UI under admin/connection/ Many times these are optional because the Hooks themselves set a default `conn_id` |\n",
    "| `query` | this is the SQL query to execute, or the query id that was supplied when the data was stored in the datacache with a call to the DatacacheClient's put method. |\n",
    "| `tolerance` | Tolerance is the policy that determines when it's acceptable to use cached data. It is described in more detail below. |\n",
    "\n",
    "#### The tolerance object has 3 different types. \n",
    "#####  latest\n",
    "```json\n",
    "{ 'type': 'latest' }\n",
    "```\n",
    "This will always go upstream to the data source and bypass the cache. It is a mistake to specify this tolerance parameter if you are retrieving data that was stored with the datacache client's push() call. In that case there is no upstream data and 'latest' will cause the retrieval to fail.\n",
    "\n",
    "\n",
    "##### version\n",
    "\n",
    "```json\n",
    "{ 'type': 'version', 'value': 1 }\n",
    "This will try to return the version of the cache specified by the value.  A new version is recorded each time the remote source is pulled from.\n",
    "```\n",
    "\n",
    "##### range\n",
    "```json\n",
    "{ 'type': 'range', 'from':  '<datetime>', 'to': '<datetime>|latest|latest_cache'}\n",
    "```\n",
    "*Range* checks the dates of previous pulls and ensure the data was pulled and cached within some `'datetime'` range. Both the `'from'` and `'to'` fields are optional but *at least* one must be specified. If you don't specify `'from'` the cache will set `'from'` to the beginning of time, and if you don't specify `'to'` it will use the latest, or if nothing is specified, the cache will be ignored and the data will be pulled from upstream.  In the `'to'` column `'latest'` means grab either the latest datacache pull or go upstream, `'latest_cache'` means grab the latest data from the cache but if no cache is present then fail.  `'latest_cache'` could be useful if the pull operation was very expensive and you didn't want to put pressure on the upstream system at this time.  If you are retrieving data stored with the datacache client's push() method `'latest_cache'` should be the only tolerance specified.\n",
    "\n",
    "* * *\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function get in module dss_datacache_client.api:\n",
      "\n",
      "get(query, tolerance, hook=None, verbose=False, context=None)\n",
      "    Cache through function: we cache the query if it does not exist.\n",
      "    \n",
      "    This function uses the Airflow logic through Hooks. It is also aware of\n",
      "    data contracts and will apply them if provided through the hook.\n",
      "    \n",
      "    IMPORTANT NOTES: The data contracts can only be applied if a hook\n",
      "    supporting data contracts is provided.\n",
      "    \n",
      "    :param query: Query to which we will try to apply data contracts\n",
      "    :param tolerance: Datacache Tolerance (example: {'type': 'latest'})\n",
      "    :param hook: Airflow Hook\n",
      "    :param verbose: if set to True, set logging level to DEBUG\n",
      "    :param context: Airflow context\n",
      "    :return:  DatacacheMetadata\n",
      "\n"
     ]
    }
   ],
   "source": [
    "from dss_datacache_client import client\n",
    "help(client.get)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Put\n",
    "### dss_datacache_client.client.put(data, query) -> DatacacheMetadata\n",
    "\n",
    "This function directly accepts data:\n",
    "\n",
    "> Parameters are: data, query. More info with: help(client.put).\n",
    "\n",
    "Data types supported:\n",
    "- pyspark.sql.DataFrame\n",
    "- pandas.DataFrame\n",
    "- filepath to local file or directory\n",
    "- pickable python object\n",
    "\n",
    "\n",
    "This function stores the data in the datacache backend.  This function does not apply any data contract. If you wish to use data contracts, and therefore Hooks, please use the 'get' function (above) instead. The `query` parameter's value here can is only an id to retrieve the data. The query will never be executed and can be an abritrary string, unique to your team. This function is very useful if you run some transformations on a pandas/spark dataframe and want to store the state of the dataframe to read it later in other Airflow tasks.\n",
    "\n",
    "#### Spark Dataframes Use Case\n",
    "You can choose columns to partition your spark dataframe by using the `partitionBy` parameter. This is an optional parameter and you won't need it in most cases. It is particularly useful if you want to ingest this data into the Media Data Lake because ingestion requires files to be partitioned by data columns.\n",
    "\n",
    "**WARNING** To store spark dataframes in the datacache you MUST be using a Spark Session that you received through the dss_airflow_utils.hooks.SparkHook. This is for security and data access reasons.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function put in module dss_datacache_client.api:\n",
      "\n",
      "put(data, query, verbose=False, context=None, partitionBy=None)\n",
      "    Simply stores the data in the datacache backend.\n",
      "    \n",
      "    This function does not apply any data contract. If you wish to use data\n",
      "    contracts, therefore Hooks, please use the 'get' function of this same\n",
      "    module instead.\n",
      "    \n",
      "    :param data: The data to store (Spark or Pandas Dataframe, filepath or\n",
      "    python pickable object)\n",
      "    :param query: The query to map the data stored\n",
      "    (useful to retrieve the data) The query can be any type of string,\n",
      "    such as a uuid.\n",
      "    :param verbose: if set to True, set logging level to DEBUG\n",
      "    :param context: Airflow context\n",
      "    :param partitionBy: Optional list of columns to use to partition the data\n",
      "    :return:  DatacacheMetadata\n",
      "\n"
     ]
    }
   ],
   "source": [
    "from dss_datacache_client import client\n",
    "help(client.put)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DatacacheMetadata interface\n",
    "\n",
    "To actually get the data referenced by the metadata object, you have the following function:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function to_local_file in module dss_datacache_client.datacache_client:\n",
      "\n",
      "to_local_file(self, local_path=None, nb_threads=None, chunksize=268435456, buffersize=4194304, blocksize=4194304)\n",
      "    Copies a file/folder from datacache to the local file system.\n",
      "    Will raise an exception if local file already exists.\n",
      "    Will create the path if it does not exists\n",
      "    :param local_path: If empty, will use a temporary location\n",
      "    :param nb_threads: Number of Threads\n",
      "    :param chunksize: Chunk Size\n",
      "    :param buffersize: Buffer Size\n",
      "    :param blocksize: Block Size\n",
      "    :return: the local path where the data was copied\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Download DatacacheMetadata to local file system \n",
    "\n",
    "from dss_datacache_client import DatacacheMetadata\n",
    "help(DatacacheMetadata.to_local_file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function to_spark in module dss_datacache_client.datacache_client:\n",
      "\n",
      "to_spark(self, spark_session)\n",
      "    Reads a file from datacache to a spark df\n",
      "    :param spark_session: Required\n",
      "    :return:\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Convert DatacacheMetadata to Spark Dataframe\n",
    "\n",
    "from dss_datacache_client import DatacacheMetadata\n",
    "help(DatacacheMetadata.to_spark)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function to_pandas in module dss_datacache_client.datacache_client:\n",
      "\n",
      "to_pandas(self)\n",
      "    Reads data from file path and return it as a pandas dataframe.\n",
      "    :return: Pandas DataFrame\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# Convert DatacacheMetadata to Pandas Dataframe\n",
    "\n",
    "from dss_datacache_client import DatacacheMetadata\n",
    "help(DatacacheMetadata.to_pandas)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Search\n",
    "### dss_datacache_client.client.search() -> List[DatacacheMetadata]\n",
    "To see what's available in the datacache, there's a search function to the client as well. This is particularly useful to query the datacache using pattern matching as opposed to the exact-matching done with get()."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function search in module dss_datacache_client.api:\n",
      "\n",
      "search(query=None, tolerance=None, dag_id=None, run_id=None, task_id=None, owner=None, limit=None, hook=None, verbose=False, view_all_access_types=False, match_type=None)\n",
      "    Search function that looks for entries within the datacache. No\n",
      "    parameters are required. However, each parameter passed to this function\n",
      "    will help narrow down the search results\n",
      "    \n",
      "    IMPORTANT NOTES: This function uses the Airflow logic through Hooks. It is\n",
      "    also aware of data contracts and will apply them if provided through the\n",
      "    hook. Further, the data contracts can only be applied if a hook supporting\n",
      "    data contracts is provided.\n",
      "    \n",
      "    :param query: Query that was stored within the datacache\n",
      "    :param tolerance: Datacache Tolerance (example: {'type': 'latest'})\n",
      "    :param dag_id: The id of the dag that put the entry in the datacache. The\n",
      "    dag id can be found on the airflow/admin/dagrun/ page in Airflow. If\n",
      "    the entry was added to the datacache from within a Jupyter notebook, use\n",
      "    the value 'Notebook'.\n",
      "    :param run_id: The id of the dag's run. The run id can be found on the\n",
      "    airflow/admin/dagrun/ page in Airflow. If the entry was added to the\n",
      "    datacache from within a Jupyter notebook, the run id is defaulted to the\n",
      "    iso timestamp representing when the data was added. When searching for\n",
      "    a datacache entry from a notebook, this parameter should probably be left\n",
      "    unset or set to None\n",
      "    :param task_id: The id of the dag task that inserted the datacache entry.\n",
      "    If the entry was added to the datacache from within a Jupyter notebook,\n",
      "    search for the value 'Notebook'.\n",
      "    :param owner: The email address of the user that created entry within\n",
      "    the datacache\n",
      "    :param limit: integer that represents a maximum number of DatacacheMetadata\n",
      "    entries to be returned. If unset or set to None, the number of entries\n",
      "    returned will be unlimited\n",
      "    :param hook: Airflow Hook\n",
      "    :param verbose: if set to True, set logging level to DEBUG\n",
      "    :param view_all_access_types: boolean that indicates whether or not\n",
      "    to return metadata entries associated with read and materialize calls to the\n",
      "    datacache. This parameter is False by default, meaning that only create calls\n",
      "    to the datacache will be returned\n",
      "    :param match_type: A string indicating how search parameters will be used to find\n",
      "    matching metadata entries. The match_type defaults to an exact search. Valid values\n",
      "    are 'exact', 'contains', 'starts_with', and 'ends_with'.\n",
      "    :return:  Array of DatacacheMetadata representing any entries within\n",
      "    the datacache that match the criteria passed to this function\n",
      "\n"
     ]
    }
   ],
   "source": [
    "from dss_datacache_client import client\n",
    "help(client.search)"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Expire\n",
    "### dss_datacache_client.client.expire()\n",
    "\n",
    "Data can be removed from the datacache backend using the expire function. When the expire function is applied to a\n",
    "query, the data from the existing versions of the query will be deleted, leaving the metadata of the query intact. You\n",
    "will get a 200 response from the datacache service if this request is fulfilled. If data is put into the datacache with \n",
    "a query that has already been used, the versions will continue to increment from the last version. \n",
    "\n",
    "If an invalid query is provided to the datacache client, you will get an error indicating that the data does not exist. "
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [
    "from dss_datacache_client import client\n",
    "help(client.expire)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# EXAMPLE 1: SparkHook\n",
    "\n",
    "from dss_airflow_utils.hooks import SparkHook\n",
    "from dss_datacache_client import client\n",
    "\n",
    "hook = SparkHook(conn_id='metastore_ndx_uat_adls')\n",
    "\n",
    "TOLERANCE_CACHE_THROUGH = {'type': 'range', 'to': 'latest'}\n",
    "query = 'select * from rmsnspazuna.tmms_market_segment limit 2'\n",
    "\n",
    "spark = hook.get_spark_session()\n",
    "\n",
    "# assert the test data is there\n",
    "records_count = spark.sql(query).count()\n",
    "assert records_count == 2\n",
    "\n",
    "metadata1 = client.get(\n",
    "    query=query, tolerance=TOLERANCE_CACHE_THROUGH, hook=hook)\n",
    "# the following 2 should not store anything in the datacache\n",
    "metadata2 = client.get(\n",
    "    query=query, tolerance=TOLERANCE_CACHE_THROUGH, hook=hook)\n",
    "metadata3 = client.get(\n",
    "    query=query, tolerance=TOLERANCE_CACHE_THROUGH, hook=hook)\n",
    "\n",
    "# Make sure all the metadatas are the same as it is a cache-through function!\n",
    "assert metadata1.vid == metadata2.vid == metadata3.vid\n",
    "\n",
    "\n",
    "# Actually get the DATA BACK\n",
    "cached_spark_df = metadata1.to_spark(spark_session=spark)\n",
    "assert cached_spark_df.count() == 2\n",
    "\n",
    "# End your spark session to release resources back to the cluster.\n",
    "spark.stop()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# EXAMPLE 2: How to use the the datacache as a caching system without hooks if you really want to\n",
    "\n",
    "from uuid import uuid4\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from pandas.util.testing import assert_frame_equal\n",
    "from dss_datacache_client import client\n",
    "\n",
    "\n",
    "TOLERANCE_CACHE_THROUGH = {'type': 'range', 'to': 'latest'}\n",
    "\n",
    "\n",
    "def caching_without_hook(query, data, tolerance):\n",
    "    metadata = client.get(query=query, tolerance=tolerance)\n",
    "    if metadata is None:\n",
    "        metadata = client.put(data=data, query=query)\n",
    "    return metadata\n",
    "\n",
    "# this is an ID to retrieve the data\n",
    "query = 'ID-{}'.format(uuid4())\n",
    "\n",
    "transformation_data = pd.DataFrame(\n",
    "    np.random.randint(0, 100, size=(100, 7)),\n",
    "    columns=list('ABCDEFG'))\n",
    "\n",
    "metadata1 = caching_without_hook(\n",
    "    data=transformation_data,\n",
    "    query=query,\n",
    "    tolerance=TOLERANCE_CACHE_THROUGH)\n",
    "\n",
    "metadata2 = caching_without_hook(\n",
    "    data=transformation_data,\n",
    "    query=query,\n",
    "    tolerance=TOLERANCE_CACHE_THROUGH)\n",
    "\n",
    "# Same data cached and reused!\n",
    "assert metadata1.vid == metadata2.vid\n",
    "assert_frame_equal(metadata1.to_pandas(), metadata2.to_pandas())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# EXAMPLE 3: Pytest example to test the upload of files \n",
    "\n",
    "# ran with $ python -m pytest -k test_put_file\n",
    "from dss_datacache_client import client\n",
    "from uuid import uuid4\n",
    "\n",
    "def test_put_file(tmpdir):\n",
    "    src = tmpdir.mkdir('src').join('hello.txt')\n",
    "    unique_content = 'Content:{}'.format(uuid4().hex)\n",
    "    src.write(unique_content)\n",
    "    upload_metatada = client.put(data=src.strpath, query='query')\n",
    "\n",
    "    dest_path1 = upload_metatada.to_local_file()\n",
    "    dest_path2 = upload_metatada.to_local_file(\n",
    "        tmpdir.join('downloaded_hello.txt').strpath)\n",
    "\n",
    "    with open(dest_path1, 'r') as f:\n",
    "        assert f.read() == unique_content\n",
    "\n",
    "    with open(dest_path2, 'r') as f:\n",
    "        assert f.read() == unique_content"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# EXAMPLE 4: Let the datacache pickle python objects for you\n",
    "\n",
    "import pickle\n",
    "from uuid import uuid4\n",
    "from dss_datacache_client import client\n",
    "\n",
    "data = list(uuid4().hex)\n",
    "pickled_path = client.put(data=data, query='query').to_local_file()\n",
    "\n",
    "with open(pickled_path, 'rb') as f:\n",
    "    cached_data = pickle.load(f)\n",
    "    assert data == cached_data\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example DAG\n",
    "\n",
    "> **Note: The hook's connection id used below is deprecated, but is maintained here to show a simplified, non-spark example that builds off of the data-contract tutorial.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "#!dag.py dag_id=tutorial_11_5\n",
    "\n",
    "# Example of datacache with Impala\n",
    "import json\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from airflow.operators.bash_operator import BashOperator\n",
    "from airflow.operators.python_operator import PythonOperator\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.workspace_utils import path_in_workspace\n",
    "from dss_airflow_utils.utils import get_config\n",
    "from dss_airflow_utils.hooks.impala_hook import ImpalaHook\n",
    "from dss_datacache_client import client\n",
    "from dss_airflow_utils.data_contract import get_input_data_contract\n",
    "from dss_airflow_utils.utils import get_data_contract\n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'pool': 'dss-testing-pool',\n",
    "    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string\n",
    "        \"worker_type\": \"python-worker\",     # here you can ask for specific worker container\n",
    "        \"request_memory\": \".5G\",            # and how much computing resource a task will need\n",
    "        \"request_cpu\": \".2\"                 # don't worry, you can override this on a task basis.\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "def impala_query(**context):\n",
    "    TOLERANCE_CACHE_THROUGH = {'type': 'range', 'to': 'latest'}\n",
    "\n",
    "    query = \"\"\"\n",
    "              SELECT t1.baseline_units, t1.baseline_price, t2.period_description\n",
    "              FROM sales as t1 \n",
    "              JOIN period as t2 \n",
    "                ON t1.period_id=t2.period_id\n",
    "              WHERE t2.week_last_day='2017-01-07'\n",
    "              LIMIT 5\n",
    "          \"\"\"\n",
    "    input_dc_aliases = ['sales', 'period']  \n",
    "    input_data_contract = get_input_data_contract(context, input_dc_aliases)\n",
    "    hook = input_data_contract.get_hook()\n",
    "\n",
    "    metadata1 = client.get(query=query, tolerance=TOLERANCE_CACHE_THROUGH, hook=hook, verbose=True)\n",
    "    # the following 2 should not store anything in the datacache\n",
    "    metadata2 = client.get(query=query, tolerance=TOLERANCE_CACHE_THROUGH, hook=hook, verbose=True)\n",
    "    metadata3 = client.get(query=query, tolerance=TOLERANCE_CACHE_THROUGH, hook=hook, verbose=True)\n",
    "\n",
    "    # Make sure all the metadatas are the same as it is a cache-through function!\n",
    "    assert metadata1.vid == metadata2.vid == metadata3.vid\n",
    "    \n",
    "    # Make sure that the data contract is returned and matches our expectations\n",
    "    expected_dc_json = json.dumps(input_data_contract.utilized_raw_data_contract, sort_keys=True)\n",
    "    assert json.dumps(metadata1.data_contract, sort_keys=True) == expected_dc_json\n",
    "    \n",
    "    # Make sure that the expanded query has been stored\n",
    "    assert 'SELECT' in metadata1.expanded_query\n",
    "    assert query != metadata1.expanded_query\n",
    "    \n",
    "\n",
    "# This decorator is required, so do not touch it! \n",
    "# Don't put anything between the decorator and def keyword\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_11_5',\n",
    "             default_args=default_args, schedule_interval=None) as dag:\n",
    "        cmd = \"echo 'Hi there, I am the first task in this DAG'\"\n",
    "\n",
    "        t1 = BashOperator(\n",
    "            task_id='start',\n",
    "            bash_command=cmd\n",
    "        )\n",
    "\n",
    "        # See the link below for documentation of the PythonOperator\n",
    "        # https://airflow.apache.org/code.html#airflow.operators.PythonOperator\n",
    "        t2 = PythonOperator(\n",
    "            task_id='end',\n",
    "            python_callable=impala_query,\n",
    "            provide_context=True\n",
    "        )\n",
    "\n",
    "        # Define dependencies between operators here, for example:\n",
    "\n",
    "        t1 >> t2\n",
    "\n",
    "        return dag  # Do not change this\n",
    "    \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!metadata.yaml dag_id=tutorial_11_5\n",
    "\n",
    "```yaml\n",
    "    spec_version: v2\n",
    "    recipe_id: tutorial_11_5\n",
    "    recipe_version: \"0_0_0\"\n",
    "    configuration:\n",
    "      properties:\n",
    "        email:\n",
    "          description: This required parameter specifies your email address.\n",
    "          type: string\n",
    "      required: [email]\n",
    "    data_contract:\n",
    "      in:\n",
    "        sales:\n",
    "          connection_id: impala_ap_uat\n",
    "          db: rmsnspazuna\n",
    "          table: tpdw_datawarehouse\n",
    "          columns:\n",
    "            period_id: dwh_tpr_id\n",
    "            baseline_units: dwh_final_baseline\n",
    "            baseline_price: dwh_regular_price\n",
    "        period:\n",
    "          connection_id: impala_ap_uat\n",
    "          db: rmsnspazuna\n",
    "          table: trtm_time_period\n",
    "          columns:\n",
    "            period_id: tpr_id\n",
    "            period_description: tpr_long_description\n",
    "            week_last_day: tpr_week_last_day\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!job_request.yaml dag_id=tutorial_11_5\n",
    "\n",
    "```yaml\n",
    "    spec_version: v2\n",
    "    recipe_id: tutorial_11_5\n",
    "    recipe_version: \"0_0_0\"\n",
    "    configuration:\n",
    "        email: \"e.xample@example.com\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****\n",
    "\n",
    "Next we will look at the [Log Level](./changing-log-level-in-jupyter-notebooks-and-terminal.ipynb) in Jupyter Notebooks and the terminal and how to change it."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}