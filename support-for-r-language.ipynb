{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Support for R Language"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*****\n",
    "\n",
    "This tutorial covers:\n",
    "-   [ROperator](#ROperator)\n",
    "-   [R Worker Image](#R-Worker-Image)\n",
    "-   [A sample recipe with a ROperator in action](#A-sample-recipe-with-ROperator-in-action)\n",
    "-   [SparkROperator](#SparkROperator)\n",
    "-   [Including additional R scripts from within your main R script](#Including-additional-R-scripts-from-within-your-main-R-script)\n",
    "    \n",
    "*****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Currently the R programming language can be used in both Jupyter notebooks as a notebook kernel and in recipes via the *ROperator*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ROperator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *ROperator* allows you to execute R scripts that are packaged together with your recipe code in a similar fashion to how the *PythonOperator* lets you run python code. \n",
    "You can find the *ROperator* in *dss_airflow_utils.operators.r_operator* module. Here is the docstring of its constructor method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2019-06-10 18:41:39,459] {settings.py:174} INFO - setting.configure_orm(): Using pool settings. pool_size=5, pool_recycle=1800\n",
      "\n",
      "        Executes R scripts that are bundled together with the dag.\n",
      "\n",
      "        Note, that the r_scripts_dir will be extracted to a temporary location\n",
      "        and will be used as the working directory when executing the main r\n",
      "        script.\n",
      "\n",
      "        :param r_scripts_dir: relative path starting from the dag location,\n",
      "          e.g., if scripts live in lib/r, then set r_scripts_dir to 'lib/r'.\n",
      "        :param main_r_script: the path of the main r script to execute\n",
      "          relative to the r_scripts_dir.\n",
      "          For example, if the main r script is called main.r and it lives\n",
      "          directly under the `r_scripts_dir` directory, then set main_r_script\n",
      "          to 'main.r'. If the r_scripts_dir is set to 'lib/r', but the main\n",
      "          script lives in 'lib/r/main_package/main_script.r', then set\n",
      "          main_r_script to 'main_package/main_script.r'.\n",
      "        :param main_args: either a list of arguments to pass to the\n",
      "          main r script or a python callable function that accepts an airflow\n",
      "          context as input parameter and optionally returns a list of input\n",
      "          arguments that should be passed to the main r script.\n",
      "          Such function can do any pre-execution preparations like read values\n",
      "          from job request or xcom, create necessary dirs or files, etc.\n",
      "          Will also accept a single number or string as one parameter.\n",
      "          Note, input parameters will be converted to string by calling\n",
      "          str(value) before passing them to Rscript command.\n",
      "        \n"
     ]
    }
   ],
   "source": [
    "from dss_airflow_utils.operators.r_operator import ROperator\n",
    "print(ROperator.__init__.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To use the operator, you need to place your R scripts inside the recipe lib directory and then provide the location of that directory and the main R script inside it as the input parameters to the operator. The operator will use the *r_scripts_dir* location as the working directory from where it will run the main script. Note that the paths you specify to both *r_scripts_dir* and *main_r_script* must be relative. Here is the simplest instantiation of the ROperator that assumes you followed the default convention and put the R scripts into *lib/r* directory and named the main script as *main.r*:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "r_task = ROperator(task_id='some_task_id')\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "During task execution, it will 'cd' into *lib/r* directory and execute ```Rscript main.r```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to pass any arguments to the main R script, you can use the *main_args* input parameter. You can pass either a list of strings "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "input_file_path = path_in_workspace('input')\n",
    "output_file_path = path_in_workspace('output')\n",
    "\n",
    "r_task = ROperator(task_id='some_task_id', \n",
    "                   main_args=[input_file_path, output_file_path])\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or a python function that takes as input the Airflow context and is expected to return a list of strings that will be passed as input arguments to the main R script. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "from dss_airflow_utils.utils import get_config\n",
    "\n",
    "def get_main_args(context):\n",
    "    config = get_config(context)\n",
    "    \n",
    "    return [config.get('some_parameter', 'some_default_value')]\n",
    "\n",
    "r_task = ROperator(task_id='some_task_id', \n",
    "                   main_args=get_main_args)\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above two examples will result in task 'cd'-ing into the *lib/r* directory and running ```Rscript main.r WORKSPACE/input WORKSPACE/output``` and ```Rscript main.r value_for_some_parameter``` where *WORKSPACE* will be the actual path of the dag workspace and *some_value* will be the value passed in the job request."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note that the arguments are first converted to string with python *str(value)* method before  they are passed to Rscript command"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using a Python function for the *main_args* parameter is useful if you want to read input parameters from the job request or from other tasks via xcom. You can also use the *main_args* function to preconfigure the task environment - to create certain folders, to write input data into the dag workspace, or do any logic to prepare the input arguments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dag workspace is available inside R code via the *WORKSPACE* environment variable. Alternatively, use the *get_workspace()* utility function and pass its value as an input argument to the main R script via the *main_args* python function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### R Worker Image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The worker type and the task resources can be provided via the *queue* parameter in the same way as with any other operator. The *ROperator* needs a Docker image containing R and related resources to run. The following images contain all of the necessary packages and libraries to run R code.\n",
    "\n",
    "- *spark2.3.0-r3.6.0-worker*\n",
    "- *r-worker* (deprecated)\n",
    "\n",
    "Here is an example configuration using the *spark2.3.0-r3.6.0-worker* image:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "r_task = ROperator(\n",
    "    task_id='some_task_id', \n",
    "    queue={\n",
    "        'worker_type': 'spark2.3.0-r3.6.0-worker',\n",
    "        'request_memory': '2G',\n",
    "        'request_cpu': '1',\n",
    "    }\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you don't specify a default worker on the dag level (via the *default_args* passed to the *DAG* constructor) and don't explicitly pass the queue parameter to the *ROperator*, it will automatically default to the queue above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The R Worker Image has the following R packages installed: car, caret, data.table, dbi, devtools, dplyr, ggmap, ggplot2, ggvis, glmnet, haven, htmlwidgets, httr, jsonlite, lme4, lubridate, multcomp, quantmod, randomForest, rgl, rjava, rlist, rmarkdown, roxygen2, Rcpp, stringr, testthat, tidyr, vcd, xlsx, xtable, xts, zoo, r-recommended."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A sample recipe with ROperator in action"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a sample recipe that uses the ROperator, please see *r_recipe* recipe inside the *sample_dags* directory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### SparkROperator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to running R code within a recipe, you can also use R to run Spark jobs. With the *SparkROperator*, you can execute R scripts that make calls to Spark. The *SparkROperator* is a hybrid of the *ROperator* and the *SparkOperator*, so the usage is similar to both of those operators.\n",
    "\n",
    "Just as with the [ROperator](#ROperator), you can specify the `r_scripts_dir`, `main_r_script`, and `main_args` constructor parameters, to indicate what script to run and what arguments it takes. The `r_scripts_dir` parameter lets you specify the working directory from which the operator will run the R script, and the value defaults to `lib/r`. The `main_r_script` parameter lets you provide the name of the main R script that you want to run, and defaults to `main.R`. If you supply a custom R script name, you should use an uppercase `.R` for the file extension. Note that the paths you specify for both `r_scripts_dir` and `main_r_script` must be relative. So, if you pass in `r_scripts_dir='lib/my_r_dir'` and `main_r_script='main.R'` to the *SparkROperator*, the operator will look in your recipe for a file located at `lib/my_r_dir/main.R`.\n",
    "\n",
    "In addition, you need to configure a SparkConfig to size and configure the Spark cluster as you would do for a [SparkOperator](spark-configuration.ipynb). You can pass in custom `spark_confs` to customize the Spark's behavior, and the `scale` parameter works the same way that it does for the *SparkOperator*. Also, the metastore_connection_id can be passed to the operator to specify a connection to a hive metastore. See the \"spark-configuration\" tutorial for more information on how to size and configure a Spark cluster.\n",
    "\n",
    "Here is the docstring of its constructor method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "        Executes R scripts that are bundled together with the dag.\n",
      "\n",
      "        Note, that the r_scripts_dir will be extracted to a temporary location\n",
      "        and will be used as the working directory when executing the main r\n",
      "        script.\n",
      "\n",
      "        Spark Options:\n",
      "        :param queue (object, optional): describes the docker container that\n",
      "            will be execute func\n",
      "        :param spark_confs (object, optional): Any additional spark configurations\n",
      "            you want to pass.\n",
      "        :param metastore_conn_id (str, optional): The airflow connection id\n",
      "        :param spark_master_url (str, optional): The url to the spark master node.\n",
      "\n",
      "        R Options:\n",
      "        :param r_scripts_dir: relative path starting from the dag location,\n",
      "          e.g., if scripts live in lib/r, then set r_scripts_dir to 'lib/r'.\n",
      "        :param main_r_script: the path of the main r script to execute\n",
      "          relative to the r_scripts_dir.\n",
      "          For example, if the main r script is called main.r and it lives\n",
      "          directly under the `r_scripts_dir` directory, then set main_r_script\n",
      "          to 'main.r'. If the r_scripts_dir is set to 'lib/r', but the main\n",
      "          script lives in 'lib/r/main_package/main_script.r', then set\n",
      "          main_r_script to 'main_package/main_script.r'.\n",
      "        :param main_args: either a list of arguments to pass to the\n",
      "          main r script or a python callable function that accepts an airflow\n",
      "          context as input parameter and optionally returns a list of input\n",
      "          arguments that should be passed to the main r script.\n",
      "          Such function can do any pre-execution preparations like read values\n",
      "          from job request or xcom, create necessary dirs or files, etc.\n",
      "          Will also accept a single number or string as one parameter.\n",
      "          Note, input parameters will be converted to string by calling\n",
      "          str(value) before passing them to Rscript command.\n",
      "        \n"
     ]
    }
   ],
   "source": [
    "from dss_airflow_utils.operators.spark_r_operator import SparkROperator\n",
    "print(SparkROperator.__init__.__doc__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is DAG containing two different invocations of the SparkROperator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Examples:\n",
    "```python\n",
    "from dss_airflow_utils.operators.spark_r_operator import SparkROperator\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "...\n",
    "...\n",
    "...\n",
    "\n",
    "\n",
    "from dss_airflow_utils.utils import get_config\n",
    "\n",
    "def get_main_args(context):\n",
    "    config = get_config(context)\n",
    "    return [config.get('some_parameter', 'some_default_value')]\n",
    "\n",
    "\n",
    "# Specify your Spark sizing here, with the appropriate SparkConfig\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=1,\n",
    "                request_cpu_per_node=1,\n",
    "                request_memory_gb_per_node=2)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='e2e_spark_r_operator',\n",
    "             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "\n",
    "    # Specify the main_args as a list.\n",
    "    # Since the r_scripts_dir and main_r_script arguments are not set, look for the R\n",
    "    # script in the default location of \"lib/r/main.R\"\n",
    "    r_task = SparkROperator(\n",
    "        task_id='r_task',\n",
    "        # provide optional arguments to the R Script (can be a list or callable)\n",
    "        main_args=[\"a\", \"b\"],\n",
    "    )\n",
    "\n",
    "    # Specify the main_args as a callable and set custom values for the\n",
    "    # main script. This operator will look for an R script in 'lib/my_r_dir/my_main.R'\n",
    "    r_task2 = SparkROperator(\n",
    "        task_id='r_task2',\n",
    "        r_scripts_dir=\"lib/my_r_dir\",\n",
    "        main_r_script=\"my_main.R\",\n",
    "        main_args=get_main_args,\n",
    "    )\n",
    "    \n",
    "    r_task >> r_task2\n",
    "    \n",
    "    return dag  # Do not change this\n",
    "\n",
    "```\n",
    "\n",
    "\n",
    "> **Important**: The *SparkROperator* relies on the worker image having Spark and R installed - which are present in the default image. If you specify a worker type other than spark2.3.0-r3.6.0-worker or r-worker in the DEFAULT_ARGS, provide a `queue` argument to the *SparkROperator* with `{'worker_type': 'spark2.3.0-r3.6.0-worker'}`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Including additional R scripts from within your main R script"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "R main scripts executed by both the *ROperator* and the *SparkROperator* (scripts specified by the `main_r_scirpt` argument) can include additional R scripts using R's normal [source()](https://www.rdocumentation.org/packages/base/versions/3.6.0/topics/source) function. This can help you break apart your R code into multiple files for better modularity. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Say you have a recipe directory structure as follows, that makes use of an ROperator with arguments `main_r_script=\"main.R\"` and `r_scripts_dir=\"lib/r\"`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre>\n",
    "my_recipe\n",
    ".\n",
    ". *omitted*\n",
    ".\n",
    "|-- __init__.py\n",
    "|-- config.yaml\n",
    "`-- dag\n",
    "    |-- __init__.py\n",
    "    |-- dag.py\n",
    "    `-- lib\n",
    "        |-- __init__.py\n",
    "        `-- r\n",
    "            |-- main.R\n",
    "            `-- sub\n",
    "                |-- my_custom_script_add.R\n",
    "                `-- my_custom_script_mult.R\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perhaps the `main.R` script will make use of multiply and add functions defined in `sub/my_custom_script_mult.R` and `sub/my_custom_script_add.R`. Using R's `source()` function, you can load files from these other R Scripts. Below is how you might achieve this.\n",
    "\n",
    "Contents of `main.R`:\n",
    "\n",
    "```r\n",
    "# these source statements include the functions for add and mult\n",
    "source(\"sub/my_custom_script_add.R\")\n",
    "source(\"sub/my_custom_script_mult.R\")\n",
    "\n",
    "# we can now use the functions\n",
    "mult(4, 2)\n",
    "add(1, 1)\n",
    "```\n",
    "\n",
    "Contents of `sub/my_custom_script_mult.R`:\n",
    "\n",
    "```r\n",
    "mult <- function(x, y) {\n",
    "   x*y\n",
    "}\n",
    "```\n",
    "\n",
    "Contents of `sub/my_custom_script_add.R`:\n",
    "```r\n",
    "add <- function(x, y) {\n",
    "   x+y\n",
    "}\n",
    "\n",
    "```\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "IMPORTANT: when you use `source()` to include code, the path given as the argument should be relative to the `r_scripts_dir` directory."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
