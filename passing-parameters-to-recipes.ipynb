{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"top\"></a>\n",
    "# Tutorial: Passing Parameters\n",
    "\n",
    "*****\n",
    "\n",
    "In this tutorial we will cover:\n",
    "\n",
    "- Passing parameters to recipes from the job request yaml\n",
    "- Using the DataCachePullOperator to get data into a DAG\n",
    "- Using templates inside an Airflow DAG\n",
    "\n",
    "****"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes you'll want to be able to pass different parameters to a recipe.  This can be especially useful for creating generic recipes that can be used by other analysts. To elucidate how, first recall the three different files used to invoke a recipe in our example:\n",
    "\n",
    "- `dag/dag.py`, which defines the body of our recipe;\n",
    "- `metadata.yaml`, which can be thought of as a function signature for our recipe;\n",
    "- `job_request.yaml` which defines our parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "****\n",
    "Let's copy a sample DAG and needed yaml files as a starting point for this tutorial. Executing the following cell will copy the needed files under a tutorial/passing_parameters subdirectory in your home directory. \n",
    "****"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cd ~\n",
    "!mkdir -p ~/tutorial \n",
    "!cp -r ~/example/tutorial/sample_dags/passing_parameters ~/tutorial/\n",
    "!ls ~/tutorial/passing_parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define Parameters\n",
    "First lets change the `metadata.yaml` file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_03_1\n",
    "recipe_version: \"0_0_0\"\n",
    "description: \"testing <your_name>'s example recipe\"\n",
    "configuration:\n",
    "  properties:\n",
    "    email:\n",
    "      description: This parameter will notify you of the success or failure of your dag run.\n",
    "      type: string\n",
    "  required: [email]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "by adding in a new parameter called `table_name` to the configuration element."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!metadata.yaml dag_id=tutorial_03_1\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_03_1\n",
    "recipe_version: \"0_0_0\"\n",
    "description: \"testing <your_name>'s example recipe\"\n",
    "configuration: \n",
    "  properties:\n",
    "    email:\n",
    "      description: This parameter will notify you of the success or failure of your dag run.\n",
    "      type: string\n",
    "    table_name:\n",
    "      description: \"This is the name of the table we'll query from\"\n",
    "      type: string\n",
    "  required: [email]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that configuration field in the metadata yaml must be a valid jsconschema expressed in yaml format.\n",
    "For a friendly jsonschema reference, see https://spacetelescope.github.io/understanding-json-schema/index.html. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Access Configuration\n",
    "\n",
    "Let's move to the `dag.py` file and access and use this parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "\n",
    "### New Imports\n",
    "from dss_airflow_utils.operators import DataCachePullOperator\n",
    "from dss_airflow_utils.hooks import DummyHook \n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'queue': {                              # Must be either a st ring (imagename) or a dictionary of string -> string\n",
    "        \"worker_type\": \"python-worker\",     # here you can ask for specific worker container\n",
    "        \"request_memory\": \".5G\",            # and how much computing resource a task will need\n",
    "        \"request_cpu\": \".2\"                 # don't worry, you can override this on a task basis.\n",
    "    }\n",
    "}\n",
    "\n",
    "\n",
    "# This decorator is required, so do not touch it! Don't put anything between the decorator and def keyword\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(dag_id='REPLACE_THIS_WITH_PROPER_DAG_ID', default_args=default_args, schedule_interval=None) as dag:\n",
    "        t1 = DataCachePullOperator(\n",
    "            task_id='read_data',\n",
    "            hook_type=DummyHook,\n",
    "            query='SELECT * FROM anytable LIMIT 10',\n",
    "            tolerance={'type': 'range', 'to': 'latest'}\n",
    "        )\n",
    "\n",
    "        return dag  # Do not change this\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Currently our query is hard coded, but other than a string literal, the query can be defined as a function which accepts one argument `'context'` that resolves to a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!dag.py dag_id=tutorial_03_1\n",
    "\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from airflow.operators.bash_operator import BashOperator\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "\n",
    "### New Imports\n",
    "from dss_airflow_utils.operators import DataCachePullOperator\n",
    "from dss_airflow_utils.hooks import DummyHook \n",
    "from dss_airflow_utils.utils import from_config \n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'pool': 'dss-testing-pool',\n",
    "    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string\n",
    "        \"worker_type\": \"python-worker\",     # here you can ask for specific worker container\n",
    "        \"request_memory\": \".5G\",            # and how much computing resource a task will need\n",
    "        \"request_cpu\": \".2\"                 # don't worry, you can override this on a task basis.\n",
    "    },\n",
    "}\n",
    "\n",
    "def query_db(context):\n",
    "    return \"SELECT * FROM {} LIMIT 10\".format(from_config('table_name')(context))\n",
    "\n",
    "\n",
    "# This decorator is required, so do not touch it! Don't put anything between the decorator and def keyword\n",
    "@dag_factory\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_03_1', default_args=default_args, schedule_interval=None) as dag:\n",
    "        t1 = DataCachePullOperator(\n",
    "            task_id='read_data',\n",
    "            hook_type=DummyHook,\n",
    "            query=query_db,\n",
    "            tolerance={'type': 'range', 'to': 'latest'}\n",
    "        )\n",
    "        \n",
    "        t2 = BashOperator(\n",
    "            task_id='start',\n",
    "            bash_command=\"echo 'table_name: {{ jr.table_name }}'\"\n",
    "        )\n",
    "        \n",
    "        t1 >> t2\n",
    "\n",
    "        return dag  # Do not change this\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a couple things to unpack here. Please note the use of `'query_db'`. In this example, the function (or callable) is being passed as a parameter to `'DataCachePullOperator'`. The function is not invoked when we pass it as a parameter because `'DataCachePullOperator'` will invoke it for us when the task executes. This point begs the question \"So if `'DataCachePullOperator'` calls the `'query_db'` function for me, what parameters, if any, does it pass to it?\". The answer is that `'DataCachePullOperator'` looks at the query parameter and, if it detects that it is a callable (read here as function), it will invoke the function with the single argument `'context'`, which is described further below. If it detects a string, then the string is used in an unmodified form. The power here comes from being able to define a function that can dynamically define a query based on the `'context'`. The query callable must be defined as having exactly one argument, as in the example above.\n",
    "\n",
    "The `'context'` object contains information about the current recipe being run. This information is represented as a dict. \n",
    "\n",
    "##### Disregard the following unless you're familiar with PythonOperator's `'python_callable'` parameter:\n",
    "> This `'context'` is the same as the `'**kwargs'` passed to the `'python_callable'` function if you set the `'provide_context'` parameter to be `'True'` during the instantiation of a `'PythonOperator'`. An example of this can be found in `example/skeleton/dag/dag.py`\n",
    "\n",
    "In the BashOperator defined in t2, we show another way to access our user-defined configuration properties. Airflow takes advantage of *Jinja*, a python templating library which allows a user to provide placeholders in strings that automagically get rendered and replaced at a later time. Here, in our bash_command string, we use `{{ jr.table_name }}` to reference the `table_name` property of the job request(jr) - see below. Airflow has built-in macros as well that can be referenced via Jinja expressions. A good example is `{{ ds }}` which returns today's date stamp. For more information around Jinja templates and Airflow support, check out the Airflow documentation [here](https://airflow.apache.org/tutorial.html#templating-with-jinja).\n",
    "\n",
    "**Note: this feature only works within operator fields and only some particular fields get templated.** The BashOperator's bash_command and env fields are templated as well as the PythonOperator's templates_dict field.\n",
    "\n",
    "### Pass Parameters\n",
    "Now open `~/tutorial/passing_parameters/job_request.yaml`:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_03_1\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration: \n",
    "    email: e.xample@example.com\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we define the value of the `'table_name'` parameter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#!job_request.yaml dag_id=tutorial_03_1\n",
    "\n",
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_03_1\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration:\n",
    "    email: e.xample@example.com\n",
    "    table_name: \"products\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A single recipe can be called with different Job Request files as long as they conform to the recipe's associated metadata requirements.  \n",
    "\n",
    "\n",
    "Take a taste of this recipe, and watch it run.  Open a terminal and run:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "cd ~/tutorial/passing_parameters/\n",
    "recipe taste job_request.yaml "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now go to the Airflow  UI and watch it run. \n",
    "\n",
    "* * *\n",
    "\n",
    "[Next](./data_hooks_and_connections.ipynb) Let's actually access some data.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
