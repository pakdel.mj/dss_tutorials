from datetime import datetime, timedelta
import logging

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import fastparquet

from dss_airflow_utils.dag_factory import dag_factory
from dss_airflow_utils.operators import DataCacheSparkPullOperator
from dss_airflow_utils.utils import from_config
from dss_airflow_utils.workspace_utils import path_in_workspace

# These are some default arguments that will be passed to each tasks.
# You can override them on a task basis if needed.
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string
        "worker_type": "python-worker",     # here you can ask for specific worker container
        "request_memory": ".5G",            # and how much computing resource a task will need
        "request_cpu": ".2"                 # don't worry, you can override this on a task basis.
    }
}

def query_db(context):
    return "SELECT * FROM {} LIMIT 10".format(from_config('table_name')(context))

def read_products(task_id_t2, **context):
    from fastparquet import ParquetFile

    file_path = context['ti'].xcom_pull(task_ids=task_id_t2)
    df = ParquetFile(file_path).to_pandas()
    logging.info('new df \n : {}'.format(df))

# This decorator is required, so do not touch it! Don't put anything between the decorator and def keyword
@dag_factory
def create_dag():
    with DAG(dag_id='tutorial_06_2', default_args=default_args, schedule_interval=None) as dag:

        def write_data(*args, **context):
            from dss_datacache_client import client
            metadata_dict = context['ti'].xcom_pull(
                task_ids=t1.task_id, 
                key=DataCacheSparkPullOperator.key)

            TOLERANCE = {'type': 'version', 'value': metadata_dict.get('vid')}
            md = client.get(metadata_dict.get('query'), tolerance=TOLERANCE)

            data_frame = md.to_pandas()
            file_name = path_in_workspace('product-ids.parquet')
            logging.info('Check out your file at {}.'.format(file_name))
            fastparquet.write(file_name, data_frame[['Product_Key']].copy(), compression='SNAPPY')
            return file_name

        t1 = DataCacheSparkPullOperator(
            task_id='read_data',
            conn_id="metastore_ndx_uat_adls",
            query=query_db,
            tolerance={'type': 'range', 'to': 'latest'},
            queue={'worker_type': 'spark-worker',
                   'request_memory': '2G',
                   'request_cpu': '1'
                  }
        )

        t2 = PythonOperator(
            task_id='write_updated_data', 
            provide_context=True,
            python_callable=write_data
        )

        t3 = PythonOperator(
            task_id='read_updates',
            provide_context=True,
            python_callable=read_products,
            op_args=['write_updated_data']
        )

        t1 >> t2 >> t3

        return dag  # Do not change this