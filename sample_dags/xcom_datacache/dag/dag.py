from datetime import datetime, timedelta
from airflow import DAG

from dss_airflow_utils.dag_factory import dag_factory

### New Imports
from dss_airflow_utils.operators import DataCacheSparkPullOperator
from dss_airflow_utils.utils import from_config 

# These are some default arguments that will be passed to each tasks.
# You can override them on a task basis if needed.
default_args = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string
        "worker_type": "python-worker",     # here you can ask for specific worker container
        "request_memory": ".5G",            # and how much computing resource a task will need
        "request_cpu": ".2"                 # don't worry, you can override this on a task basis.
    }
}

def query_db(context):
    return "SELECT * FROM {} LIMIT 10".format(from_config('table_name')(context))


# This decorator is required, so do not touch it! Don't put anything between the decorator and def keyword
@dag_factory
def create_dag():
    with DAG(dag_id='tutorial_04_1', default_args=default_args, schedule_interval=None) as dag:
        t1 = DataCacheSparkPullOperator(
            task_id='read_data',
            conn_id="metastore_ndx_uat_adls",
            query=query_db,
            tolerance={'type': 'range', 'to': 'latest'},
            queue={'worker_type': 'spark-worker',
                   'request_memory': '2G',
                   'request_cpu': '1'}
        )

        return dag  # Do not change this