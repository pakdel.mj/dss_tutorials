#!dag.py dag_id=tutorial_running_recipe
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

from dss_airflow_utils.dag_factory import dag_factory
from dss_airflow_utils.workspace_utils import path_in_workspace
from dss_airflow_utils.utils import get_config


# Import of the custom packages and modules must be relative!
from .lib.my_module import my_business_logic

# These are some default arguments that will be passed to each tasks.
# You can override them on a task basis if needed.
DEFAULT_ARGS = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'pool': 'dss-testing-pool',
    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string
        "worker_type": "python-worker",     # here you can ask for specific worker container
        "request_memory": ".5G",            # and how much computing resource a task will need
        "request_cpu": ".2"                 # don't worry, you can override this on a task basis.
    }
}


def my_func(file_path, **context):
    # Here is how you can get parameters passed via job request
    config = get_config(context)
    my_business_logic(file_path,
                      config.get("name_of_the_first_parameter"),
                      config.get("name_of_the_second_parameter"))

# This decorator is required, so do not touch it!
# Don't put anything between the decorator and def keyword
# pylint: disable=pointless-statement
@dag_factory
def create_dag():
    with DAG(dag_id='tutorial_02_1',
             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:
        # Define operators here, for example:

        output_file = path_in_workspace("foo.txt")

        cmd = "echo 'Hi there, I am the first task in this DAG' > {}".format(
            output_file)

        t_1 = BashOperator(
            task_id='start',
            bash_command=cmd
        )

        # See the link below for documentation of the PythonOperator
        # https://airflow.apache.org/code.html#airflow.operators.PythonOperator
        t_2 = PythonOperator(
            task_id='end',
            python_callable=my_func,
            provide_context=True,
            op_args=[output_file]
        )

        # Define dependencies between operators here, for example:

        t_1 >> t_2

        return dag  # Do not change this