"""
Here you can define any complex computation logic to support dag execution.
You can also create more modules under the "dag" directory.
Just remember to use *relative* imports.
"""

import logging


def my_business_logic(file_path, string_param, num_param):
    """
    Log file content and values fo the two provided input parameters.
    """
    try:
        with open(file_path, "r") as file_handler:
            logging.info("I just read this %s file, here is its content: %s",
                         file_path, file_handler.read())

        logging.info("String param is %s", string_param)
        logging.info("Numeric param is %s", num_param)
    except IOError:
        logging.exception("Seems like I failed to read the %s file. "
                          "Can somebody make sure that file exist?", file_path)
        raise
