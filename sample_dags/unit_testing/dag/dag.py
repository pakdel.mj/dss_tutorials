from datetime import datetime, timedelta
from airflow import DAG
from dss_airflow_utils.dag_factory import dag_factory
from dss_airflow_utils.operators import SparkOperator

DEFAULT_ARGS = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
    'queue': {
        "request_memory": "16G",
        "request_cpu": "4"
    }
}
# Reminder notes for DEFAULT_ARGS syntax:
#  'queue' Must be either a string (imagename) or a dictionary of string -> string
#  'request_memory' is how much maximum computing resource a task will need;
#     don't worry, it starts small and will retry with more if it runs
#     out of memory or CPU


DEFAULT_SPARK_CONFS = {
    'spark.hadoop.parquet.enable.summary-metadata': 'True',
    'spark.executor.memory': '8G',
    'spark.driver.memory' : '8G',
    'spark.debug.maxToStringFields': '500'
}

def read_data(spark, context):
    from dss_airflow_utils.utils import get_config
    from dss_datacache_client import client
    from .lib.my_module import data_tranformation
    # read data from datacache
    job_req = get_config(context)
    query_tag = job_req['qry_tag']
    demo_data_spk = client.get(\
       query=query_tag, \
       tolerance={"type": "range", "to": "latest_cached"}\
                              ).to_spark(spark)
    data_tranformation(demo_data_spk, context)

# This decorator is required, so do not touch it!
# Don't put anything between the decorator and def keyword
@dag_factory
def create_dag():
    with DAG(dag_id='spark_unitTest',
             default_args=DEFAULT_ARGS, schedule_interval=None) as dag:

        SparkOperator(
            task_id='read_data',
            func=read_data,
            spark_confs=DEFAULT_SPARK_CONFS
        )
        return dag
