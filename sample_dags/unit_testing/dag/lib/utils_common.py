import os

def get_abs_filename(filename, calling_dir=None):
    """
    Helper function that returns absolute path for filename provided
    params:
       filename - filename to return abs filename of
    output:
       String - absolute file path (of file specified in "filename") and file name

    Assumption : File exists in working directory

    """
    if calling_dir is None:
        calling_dir = os.path.join(
            os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
            'tests/data')

    calling_dir_with_file = "file://" + calling_dir
    print(calling_dir_with_file)

    return os.path.join(calling_dir_with_file, filename)
