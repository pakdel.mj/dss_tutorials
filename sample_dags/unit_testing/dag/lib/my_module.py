from dss_airflow_utils.utils import get_config

def data_tranformation(sdf, context):
    # filter columns from existing dataframe
    job_req = get_config(context)
    flt_col = job_req['filter_columns']
    sdf.select(*flt_col).show()
    return sdf
