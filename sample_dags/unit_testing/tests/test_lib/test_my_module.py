import pytest
from ...dag.lib.my_module import data_tranformation
from ...dag.lib.utils_common import get_abs_filename

@pytest.fixture(scope="session", name="context")
def get_fake_context():
    '''
    This function creates a fake airflow context for unit tests
    '''
    class fake:
        conf = {"configuration": {
                'qry_tag' : "demo_data_july",
                'filter_columns' : ['nan_key','sales_unit','sales_value']
        }}  
    fake_context = {'dag_run': fake()}
    return fake_context 

@pytest.fixture(scope="session", name="sample_demo_data")
@pytest.mark.usefixtures("spark")
def load_sample_demo_data(spark):
    """ fixture for loading a CSV to spark
    Args:
        request: pytest.FixtureRequest object
        request: spark session
    """
    sample_demo_data = spark.read \
        .format("csv") \
        .option("header", "true") \
        .option("mode", "DROPMALFORMED") \
        .load(get_abs_filename("demo_data.csv"))

    yield sample_demo_data

@pytest.mark.usefixtures("sample_demo_data", "context")
def test_data_tranformation(sample_demo_data, context):
    sdf = data_tranformation(sample_demo_data, context)
    assert True
