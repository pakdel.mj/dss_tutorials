import os
import pytest
from pyspark.sql import SparkSession

@pytest.fixture(scope="session", name="spark", autouse=True)
def create_spark_session(request):
    """ fixture for creating a spark context
    Args:        request: pytest.FixtureRequest object
    """
    os.environ['PYSPARK_PYTHON'] = 'python3'
    spark = SparkSession.builder \
        .appName("pytest fixture") \
        .master("local[*]") \
        .config("spark.executor.memory", "1g") \
        .config("spark.driver.memory", "5g") \
        .getOrCreate() 
    yield spark
    request.addfinalizer(lambda: spark.stop())