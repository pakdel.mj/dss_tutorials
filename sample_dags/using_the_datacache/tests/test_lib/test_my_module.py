import os
import logging
import fastparquet
import pandas as pd
import pytest
from dss_airflow_utils.workspace_utils import path_in_workspace


# Import of the custom modules should start with the 'dag' folder.
from dag.lib.my_module import file_check_func

@pytest.fixture
def test_files():
    df1 = pd.DataFrame({'lines':['This is this the first line\n',
                                 'This is this the next line\n',
                                 'This is this the last line\n']})
    df2 = pd.DataFrame({'lines':['This is this the first line\n',
                                 'This is this the next line\n',
                                 'This is this the last line\n']})
    df3 = pd.DataFrame({'lines':['This is this the first line\n',
                                 'This is this the next line\n',
                                 'This is this a different last line\n']})

    file1 = path_in_workspace('Using-the-Datacache-example.parquet')
    file2 = path_in_workspace('Using-the-Datacache-example_ver2.parquet')
    file3 = path_in_workspace('Using-the-Datacache-example.txt')
    fastparquet.write(file1, df1)
    fastparquet.write(file2, df2)
    fastparquet.write(file3, df3)

    yield (file1, file2, file3)

    os.remove(file1)
    os.remove(file2)
    os.remove(file3)


class mock_xcom:
    def xcom_pull(self, task_ids=None):  #pylint: disable=unused-argument,no-self-use
        return 'query'


class mock_client:
    def get(self, *args, **kwargs):  #pylint: disable=unused-argument,no-self-use
        return mock_metadata()


class mock_metadata:
    def to_local_file(self, new_file):  #pylint: disable=no-self-use
        return path_in_workspace(new_file)

    def to_pandas(self):  #pylint: disable=no-self-use
        df1 = pd.DataFrame({'lines': ['This is this the first line\n',
                                      'This is this the next line\n',
                                      'This is this the last line\n']})
        return df1


def test_file_check_func(caplog, test_files):
    full_set_vid = xform_set_vid = 0
    retrieval_info = "file_from_task", full_set_vid, xform_set_vid
    file_check_func(retrieval_info,
                    mock_client(),
                    test_files[0],
                    'fake query string')

    assert caplog.record_tuples == [
        ('root', logging.INFO, 'file_from_task passed from transform_task '
                               'by xcom is file_from_task'),
        ('root', logging.INFO, 'files are the same'),
        ('root', logging.INFO,
         "After retrieving the new_dataframe we have 3 records")
    ]


def test_file_check_func_diff(caplog, test_files):
    full_set_vid = xform_set_vid = 0
    retrieval_info = "file_from_task", full_set_vid, xform_set_vid
    file_check_func(retrieval_info,
                    mock_client(),
                    test_files[2],
                    'fake query string')

    log_msgs = caplog.record_tuples
    assert len(log_msgs) == 4
    assert log_msgs[1] == (
         'root', logging.ERROR,
         'Differs in position 2,0: This is this a different last line != '
         'This is this the last line')
    assert log_msgs[2] == ('root', logging.ERROR, 'files differ')
