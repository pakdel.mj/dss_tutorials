import logging
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from dss_airflow_utils.dag_factory import dag_factory
from dss_airflow_utils.workspace_utils import path_in_workspace
from dss_airflow_utils.spark import SparkOperator, SparkConfig
from dss_airflow_utils.dag_factory import DagConfig
from dss_airflow_utils.operators.datacache_operator import (
    DataCacheSparkPullOperator, DataCachePullOperator)
from dss_airflow_utils.utils import get_config
# obtaining a reference to the datacache client
from dss_datacache_client import client
# a function defined in the dag library rather than directly in the dag.py
from .lib.my_module import file_check_func

# These are some default arguments that will be passed to each airflow task.
# You can override them on a task basis if needed.
DEFAULT_ARGS = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 4,  # This will allow the dag to start small and scale up
                   # if needed
    'retry_delay': timedelta(seconds=10),
    'queue': {
        "worker_type": "python-worker",
        "request_memory": "16G",
        "request_cpu": "1"
    }
}

# size of the spark workers
SPARK_QUEUE = {
    # The  queue specification must be either a string (imagename) or
    # a dictionary of string -> string

    # with worker_type you ask for a specific worker container
    'worker_type': 'spark-worker',
    # start small and DSS will retry with more if it runs out
    'base_request_memory': '500M',
    'base_request_cpu': '.2',
    # specify the maximum computing resource a task will need
    'request_memory': '2G',
    'request_cpu': '1'
}

# Size of Spark Driver. The spark driver is a spark node that is spun
# up to handle non-parallel parts of the application. It typically will
# have just one CPU.  The amount of work it does grows with the size of
# the cluster. We have to provide this to each spark task's initialization.
SPARK_DRIVER_CONFIG = {
    "worker_type": "spark-worker",
    "request_memory": "2G",
    "request_cpu": "1"
    }

# by defining this variable at the global level it
# is shared with all the functions in the file.
FILE_NAME = path_in_workspace('Using-the-Datacache-example.parquet')

# This variable is used in one task to store a dataframe and in another to
# get the dataframe back.  Defining it here ensures the same value is used in
# both tasks.
THREE_COL_QUERY = "three selected columns"


# this tolerance dictionary value specifies that we want the latest in the
# cache if it exists, but go to the upstream data source and retrieve
# the data if the cache is empty.
TOLERANCE_CACHE_THROUGH = {'type': 'range', 'to': 'latest'}


# to prevent errors in spelling table names as well as to prevent malicious
# users from inserting extra statements in the sql (https://xkcd.com/327/)
# we list here expected table names that can go into the sql
VALID_TABLES = ["aod_pg_prototxi.fct", "rmsnspazuna.tmms_market_segment"]


def query_db(context):
    """ Construct a query of the table whose name is passed in
        from job_request.yaml.  This is an example of a function
        that dynamically constructs SQL.  The SQL could be more complex,
        specifying joins of multiple tables and using the selection criteria
        to subset the data, scale it, etc.

        :param context: Airflow context object
        :returns: SQL query as a string
     """
    config_dict = get_config(context)
    tbl_name = config_dict.get('table_name', None)
    if not tbl_name:
        msg = "Table name not present in job request file"
        logging.error(msg)
        raise RuntimeError
    elif tbl_name not in VALID_TABLES:
        msg = ("table name parameter in job request ({}) doesn't "
               "appear in list of valid tables".format(tbl_name))
        logging.error(msg)
        raise RuntimeError
    query = 'select * from {} limit 2'.format(tbl_name)
    logging.info('Query to load the datacache is {}'.format(query))
    return query


def selection_func(spark, context):
    """ This function calls the datacache (through the datacache client) and
        requests that the query used to pull the data in the
        DataCacheSparkPullOperator be used to get the data from the datacache
        (thus not doing another database operation) and we create a spark
        dataframe from the result.  What comes back is written to a file in
        the DAGs workspace to prepare for the next step when it is shown that
        multiple cache gets() produce the same result. Then some manipulation
        is done on the data and and a report created (actually in this case
        just a log messsage).

    :param spark: Spark context object
    :param context: Airflow context dictionary
    :return: Query string associated with the dataset pulled
    """
    full_set_query = query_db(context)
    config_dict = get_config(context)

    pulled_version = context['ti'].xcom_pull(task_ids='pull_task',
                                             key=DataCachePullOperator.key)

    if not pulled_version:
        raise RuntimeError('No data was pulled in upstream task')

    cache_metadata = client.get(full_set_query,
                                tolerance={'type': 'version',
                                           'value': pulled_version['vid']})

    # check that it was found
    if cache_metadata is None:
        logging.error("Unable to find matching datacache entry: "
                      "query = '%s', tolerance = %s", full_set_query,
                      TOLERANCE_CACHE_THROUGH)
        # in a real system you might reissue the get with a different
        # tolerance so that it pulls from upstream or take some other action
        return None

    dataframe = cache_metadata.to_spark(spark)

    # save the dataframe with all the data to a file
    logging.info("Saving dataframe to file %s", FILE_NAME)  # DEBUG
    dataframe.write.parquet('file://' + FILE_NAME)

    # select out some columns to demonstrate an operation (selection in this
    # case) on a spark dataframe
    rows_to_select = config_dict.get('rows_to_select', [])
    if rows_to_select:
        new_dataframe = dataframe.select(rows_to_select)
    else:
        new_dataframe = dataframe
    logging.info("After retrieval from new_dataframe we have {} records".
                 format(new_dataframe.count()))

    # If the dataframe is to be used in a later task you can put
    # the dataframe into the datacache with no hook.  Passing the
    # context fills in the metadata for user, dag, etc.
    xform_metadata = client.put(query=THREE_COL_QUERY, data=new_dataframe,
                                context=context)

    # The query string that the file is written with is returned and
    # passed to next task via XCOM.  Also being passed are the version
    # numbers of the cached data to be sure we don't cross with anyone
    return full_set_query, cache_metadata.vid, xform_metadata.vid

def build(builder):
    """ Specifying a builder function allows the specification spark
        features and parameters that can override at cluster build time
        the parameters specified in config dictionaries.

    :param builder: Builder dictionary to be passed to spark initializer
    :return: None
    """
    builder.config("spark.driver.cores", "1")


def file_check_function(**context):
    """ In the previous task we retrieved some data via the datacache,
        and wrote it out to a file.  To show that we didn't need to
        write it out to a file in this task we ask the datacache to
        write to a file the data it is holding and then we compare the
        file that was written last time with the file written by the
        datacache.


        The functionality of the is implemented in the my_module.py
        file in the lib directory to show how a library function can
        be called and tested apart from the DAG.

    :param context: Airflow context dictionary
    :return: None
    """
    logging.info("Reading selected cols from %s", FILE_NAME)
    # use xcom and the current task instance (context['ti']) to retrieve
    # the data passed with the return from the transform task (xcom_pull's
    # default key).  This is the name of the file written by the
    # 'transform_task'.
    retrieval_info = context['ti'].xcom_pull(task_ids='transform_task')
    if retrieval_info is None:
        # upstream error, not able to retrieve from datacache
        return
    file_check_func(retrieval_info, client, FILE_NAME, THREE_COL_QUERY)


# This decorator is required, so do not touch it!
# Don't put anything between the decorator and def keyword
@dag_factory(DagConfig(SparkConfig(num_nodes=1,
                                   request_cpu_per_node=1,
                                   request_memory_gb_per_node=2)))
def create_dag():
    with DAG(dag_id='Using-the-Datacache-example',
             default_args=DEFAULT_ARGS,
             schedule_interval=None) as dag:

        pull_task = DataCacheSparkPullOperator(
            task_id="pull_task",
            conn_id="metastore_ndx_uat_adls",
            builder_func=build,
            query=query_db,  # executable that returns select statement,
            tolerance={"type": "latest"},  # always goes to DB in first task
            queue=SPARK_QUEUE,  # needed to use spark worker
        )

        transform_task = SparkOperator(
            task_id='transform_task',
            func=selection_func,
            queue=SPARK_DRIVER_CONFIG
        )

        check_transfer_task = PythonOperator(
            task_id='check_transfer_task',
            python_callable=file_check_function,
            provide_context=True
        )

        # Define dependencies between operators here, for example:

        pull_task >> transform_task >> check_transfer_task

        return dag  # Do not change this
