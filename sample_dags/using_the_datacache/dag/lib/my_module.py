"""
Here you can define any complex computation logic to support dag execution.
You can also create more modules under the "dag" directory.
Just remember to use *relative* imports.
"""
import logging
import itertools
import pandas as pd
from dss_airflow_utils.workspace_utils import path_in_workspace


def cstrip(val):
    """ Conditional strip """
    try:
        return val.strip()
    except AttributeError:
        if pd.isnull(val):
            return 'nan'  # in this case we want nan's to compare equal
        return val


def parquet_files_same(orig_file, new_file):
    """ compare two files, log differences """
    orig_f = pd.read_parquet(orig_file)
    new_f = pd.read_parquet(new_file)
    if orig_f.equals(new_f):
        return True
    elif orig_f.shape != new_f.shape:
        logging.info('Shapes differ, {} != {}'
                     .format(orig_f.shape, new_f.shape))
    else:
        x, y = orig_f.shape
        for i, j in itertools.product(range(x), range(y)):
            if cstrip(orig_f.loc[i][j]) != cstrip(new_f.loc[i][j]):
                logging.error('Differs in position {},{}: {} != {}'.
                              format(i, j, cstrip(orig_f.loc[i][j]),
                                     cstrip(new_f.loc[i][j])))
    return False


def file_check_func(retrieval_info, client, file_name, query_string):
    """ In the previous task we retrieved some data via the datacache,
        and wrote it out to a file.  To show that we didn't need to
        write it out to a file in this task we ask the datacache to
        write to a file the data it is holding and then we compare the
        file that was written last time with the file written by the
        datacache.
    """
    # break the tuple apart
    file_from_task, full_set_vid, xform_set_vid = retrieval_info
    logging.info("file_from_task passed from transform_task by xcom is {}"
                 .format(file_from_task))

    metadata = client.get(file_from_task,
                          tolerance={'type': 'version',
                                     'value': full_set_vid})

    # check that it was found
    if metadata is None:
        logging.error("Unable to find matching datacache entry: "
                      "query = '%s', version = %s", file_from_task,
                      full_set_vid)
        # in a real system you might reissue the get with a different
        # tolerance so that it pulls from upstream or take some other action
        return
    ver2_file_name = path_in_workspace('Using-the-Datacache-example_ver2.parquet')

    # use metadata returned from client.get to write out a file
    metadata.to_local_file(ver2_file_name)

    # compare the new file with the one that we stored in the datacache
    if parquet_files_same(file_name, ver2_file_name):
        logging.info("files are the same")
    else:
        logging.error("files differ")

    # we can also retrieve the dataframe that was stored from our python
    # function in the previous task
    new_dataframe_metadata = client.get(query=query_string,
                                        tolerance={'type': 'version',
                                                   'value': xform_set_vid})

    # check that it was found
    if metadata is None:
        logging.error("Unable to find matching datacache entry: "
                      "query = '%s', version = %s", query_string,
                      xform_set_vid)
        # in a real system you might reissue the get with a different
        # tolerance so that it pulls from upstream or take some other action
        return

    # even though we put in a spark dataframe we can retrieve the data
    # as a pandas dataframe.
    new_dataframe = new_dataframe_metadata.to_pandas()
    logging.info("After retrieving the new_dataframe we have {} records".
                 format(new_dataframe.shape[0]))
