from datetime import datetime, timedelta
from airflow import DAG

from dss_airflow_utils.dag_factory import dag_factory
from dss_airflow_utils.operators.r_operator import ROperator
from dss_airflow_utils.utils import get_config


default_args = {
    'owner': 'airflow',
    'start_date': datetime(2015, 6, 1),
    'retries': 0,
    'retry_delay': timedelta(seconds=10),
    'queue': {
                'worker_type': 'r-worker',
                'request_memory': '2G',
                'request_cpu': '1',
            }
}


def get_main_args(context):
    config = get_config(context)
    return [config.get('summand1', 0), config.get('summand2', 0)]


@dag_factory
def create_dag():
    with DAG(dag_id='compute_sum_in_r',
             default_args=default_args,
             schedule_interval=None) as dag:

        ROperator(
            task_id='compute_sum_in_r', 
            main_args=get_main_args
        )

        return dag
