{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Spark Cluster Configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "This User Guide covers:\n",
    "\n",
    "- Introduction to Spark Cluster Configuration\n",
    "- Spark Context Size and Configuration\n",
    "- Option 1: Adjusting the cluster size via `scale` SparkOperator parameter\n",
    "- Option 2: Adjusting the cluster size by adjusting the Spark Configurations directly in DAG\n",
    "- Option 3: Adjusting the cluster size by adjusting the Spark Configurations in Job Request\n",
    "- Advanced Topic: Debug `spark_confs` set by the `scale` parameter\n",
    "- Advanced Topic: Example of Global templateable configs\n",
    "\n",
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction to Spark Cluster Configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every recipe that uses a Spark operator can either use the Spark system cluster or create a dedicated cluster for the DAG(s) in the recipe. The Spark cluster configuration defines the dimensions for a Spark cluster in a DAG.  This configuration includes:\n",
    "\n",
    "- num_nodes:  number of workers in the spark cluster, defaults to four\n",
    "- request_cpu_per_node: number of CPU cores to request per node, defaults to one\n",
    "- request_memory_gb_per_node: number of gigabytes of memory to request per node, defaults to two\n",
    "- worker_type: string containing the name of the docker image to use. See the homepage's cluster information for available Spark worker types.\n",
    "\n",
    "The spark configuration allows recipe developers to specify what spark resources will be available to a run of a recipe.  It is not a requirement to specify any of these values as they have defaults, but tuning these parameters can ensure the success of a recipe run. The defaults are generally small to prevent accidental creation of a large cluster so proper sizing is recommended. It is also not a requirement to include all of the parameters. One important note when using a different Spark worker is to make sure that the worker_type matches the QUEUE's worker_type set for the Airflow task as these need to be the same.\n",
    "\n",
    "When writing a DAG, the Spark configuration can be built right into the DagFactory decorator as seen in the code below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from dss_airflow_utils.spark import SparkOperator, SparkClusterStart\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "\n",
    "def write_stuff(spark, context):\n",
    "    print(\"Does something Spark Related\")\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "}\n",
    "    \n",
    "# Size of Spark Driver\n",
    "spark_queue = {\n",
    "    \"worker_type\": \"spark-worker\",\n",
    "    \"request_memory\": \"1G\",\n",
    "    \"request_cpu\": \"1\"\n",
    "}\n",
    "\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=2, request_cpu_per_node=2, request_memory_gb_per_node=2)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_spark_cluster_configuration_0_0_1', default_args=default_args, \n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        spark_op_1 = SparkOperator(\n",
    "            func=write_stuff,\n",
    "            task_id=\"write_stuff\",\n",
    "            queue=spark_queue)\n",
    "        \n",
    "        return dag\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The configuration of the Spark cluster as seen above is fixed.  Once the recipe is published, this configuration cannot be changed though there are different ways that this information can be overwritten.  During the creation of the Spark cluster, the config is updated with information from the job request submitted with the recipe and then with information pushed in as xcoms from inside the recipe.  \n",
    "\n",
    "An example of a job request is provided which demonstrates how a job request can be supplied with Spark cluster configuration information.  Note that the system_configuration section included in this request is not and does not need to be reflected in the recipe's metadata.yaml file.  Outside of worker_type, the values are all numbers (not strings).   \n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "# job_request.yaml\n",
    "\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_spark_cluster_configuration\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration:\n",
    "    email: e.xample@example.com\n",
    "system_configuration:\n",
    "    spark_config:\n",
    "        num_nodes: 2\n",
    "        request_cpu_per_node: 4\n",
    "        request_memory_gb_per_node: 8\n",
    "        worker_type: 'spark2.4.4-python3.7-worker'\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Including the Spark cluster configuration in the job request allows for flexibity in recipe execution by sizing the spark cluster more appropriately against the input data and expected computational complexity for each individual recipe run.  Any values included in the job request's system configuration will overwrite the values that were written into the DagFactory decorator, otherwise the original values will be used.  \n",
    "\n",
    "Xcoms provide the final opportunity to tune the spark cluster before it is created.  Like the SparkConfig written inside of the DagFactory decorator, once the recipe is published, the method that pushes the xcoms will be fixed.  What the Xcom push approach offers is the ability to size the Spark cluster programatically and dynamically.  Xcoms values will take priority over any values supplied from the job request and original SparkConfig."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from dss_airflow_utils.spark import SparkOperator, SparkClusterStart\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "from airflow.operators.python_operator import PythonOperator\n",
    "import logging\n",
    "\n",
    "\n",
    "def write_stuff(spark, context):\n",
    "    print(\"Does something Spark Related\")\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "}\n",
    "    \n",
    "# Size of Spark Driver\n",
    "spark_queue = {\n",
    "    \"worker_type\": \"spark-worker\",\n",
    "    \"request_memory\": \"1G\",\n",
    "    \"request_cpu\": \"1\"\n",
    "}\n",
    "\n",
    "\n",
    "def push_xcoms(*args, **context):\n",
    "    logging.info(\"Pushing Spark Configs!\")\n",
    "    context['ti'].xcom_push(key=SparkClusterStart.spark_config_key,\n",
    "                            value=SparkConfig(num_nodes=4,\n",
    "                                              request_memory_gb_per_node=12).\n",
    "                                                  to_dict())\n",
    "\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=2, request_cpu_per_node=2,\n",
    "                request_memory_gb_per_node=2)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_spark_cluster_configuration_0_0_2',\n",
    "             default_args=default_args,\n",
    "             schedule_interval=None) as dag:\n",
    "        \n",
    "        final_call = PythonOperator(\n",
    "            task_id=\"push_spark_xcom\",\n",
    "            python_callable=push_xcoms,\n",
    "            provide_context=True\n",
    "        )\n",
    "\n",
    "        spark_op_1 = SparkOperator(\n",
    "            func=write_stuff,\n",
    "            task_id=\"write_stuff\",\n",
    "            queue=spark_queue)\n",
    "\n",
    "        final_call >> spark_op_1\n",
    "\n",
    "        return dag\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**The xcom is pushed with spark_config_key and it takes the same list of values as seen above.**\n",
    "\n",
    "Note that the task that pushes the spark cluster configuration xcom precedes the Spark Operator.  If a Spark Operator task is run before the xcoms are pushed, the Spark cluster will be built using information from the job request and SparkConfig.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spark Context Size and Configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, when you use a SparkOperator, the job that executes will use the full Spark cluster. However, if you have more than one Spark Job that should run around the same time, it may be desirable to specify a Spark context size such that each Spark job uses only a portion of the resources available in cluster. The following techniques give you more control over how your resources are allocated in Spark."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Option 1: Adjusting the cluster size via `scale` SparkOperator parameter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Spark Scaling](./images/spark-scaling.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `scale` argument to the SparkOperator makes Spark context sizing really easy by allowing you to size the context based on a percentage. The `scale` parameter achieves its goal by adjusting the [Spark Configurations](https://spark.apache.org/docs/latest/configuration.html) (`spark_confs`) sent to the Spark master. When creating a DAG that uses a SparkOperator, you can specify the optional `scale` constructor argument with a value greater than 0 and less than or equal to 1.  A value of 1 indicates that the Spark context should be sized to use 100% of the resources in the cluster; a value of 0.5 would indicate that the Spark context should use 50% of the cluster resources. \n",
    "\n",
    "Note: If you have multiple parallel SparkOperators with a scale set, the sum of the scales does not necessarily need to add up to 1. However, if the `scale` values sum to greater than one, some of the Spark jobs may wait until they have enough resources before they start running.\n",
    "\n",
    "See the example below:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from dss_airflow_utils.spark import SparkOperator, SparkClusterStart\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "\n",
    "def write_stuff(spark, context):\n",
    "    print(\"Does something Spark Related\")\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "}\n",
    "    \n",
    "# Size of Spark Driver\n",
    "spark_queue = {\n",
    "    \"worker_type\": \"spark-worker\",\n",
    "    \"request_memory\": \"1G\",\n",
    "    \"request_cpu\": \"1\"\n",
    "}\n",
    "\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=2, request_cpu_per_node=2, request_memory_gb_per_node=2)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_spark_cluster_size_1', default_args=default_args, \n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        spark_op_1 = SparkOperator(\n",
    "            func=write_stuff,\n",
    "            task_id=\"write_stuff\",\n",
    "            queue=spark_queue,\n",
    "            # portion of spark cluster to allocate to context\n",
    "            scale=0.5)  # NOTE THIS\n",
    "        \n",
    "        return dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Note: \n",
    "\n",
    "- If you have a parent DAG trigger a child DAG, and if you define a `scale` parameter in a SparkOperator in the child DAG, the child will scale according to the parent's total resources. For example, if you allocate 2 nodes, 4 cores per node, and 8GB per node in your parent SparkConfig() and set a scale of 0.5 for a SparkOperator in the child DAG, that SparkOperator will run with a context size of 2 cores and 4GB RAM.\n",
    "- Scaled CPU resources will be rounded up to the nearest CPU. For example, if you set request_cpu_per_node=3 and a scale of 0.5, the context will be sized to use 2 CPU cores.\n",
    "- RAM is considered in terms of Gigabytes. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Option 2: Adjusting the cluster size by adjusting the Spark Configurations directly in DAG\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You have the ability to directly pass `spark_confs` to the SparkOperator.  Modification of the Spark Configuration can be done by adding a `spark_confs` key to the SparkOperator constructor.\n",
    "\n",
    "The `spark_confs` constructor argument can take one of two forms: (1) it can be a Python dictionary where the key represents the Spark configuration variable, and the value represents the Spark configuration value; or (2) it can be a callable that takes an Airflow context and returns a dictionary of the aforementioned format. \n",
    "\n",
    "Setting Spark Configurations in this way will **override** any overlapping variables set by the aforementioned `scale` argument."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Note:\n",
    "\n",
    "When setting the spark_confs constructor explicitly as seen in options two and three, the parameters setting the cores per executor, memory per executor, and minimum number of executors must respect the limits of the Spark cluster.  If the requests in the spark_confs exceed what the spark cluster can allocate, the task will fail.\n",
    " \n",
    "See the [Spark Configuration](https://spark.apache.org/docs/latest/configuration.html) reference for more info on available configurations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from dss_airflow_utils.spark import SparkOperator, SparkClusterStart\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "\n",
    "def write_stuff(spark, context):\n",
    "    print(\"Does something Spark Related\")\n",
    "\n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "}\n",
    "    \n",
    "# NOTE THIS ---- NOTE THIS\n",
    "# Set custom Spark Configuration\n",
    "spark_confs_1 = {\n",
    "    'spark.executor.cores': 4,\n",
    "    'spark.executor.memory': '2G',\n",
    "}\n",
    "\n",
    "spark_confs_2 = {\n",
    "    'spark.executor.cores': 2,\n",
    "    'spark.executor.memory': '1G',\n",
    "}\n",
    "# END NOTE THIS ---- END NOTE THIS\n",
    "\n",
    "# Size of Spark Driver\n",
    "spark_queue = {\n",
    "    \"worker_type\": \"spark-worker\",\n",
    "    \"request_memory\": \"1G\",\n",
    "    \"request_cpu\": \"1\"\n",
    "}\n",
    "\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=2, request_cpu_per_node=4, request_memory_gb_per_node=2)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_spark_cluster_size_2', default_args=default_args, \n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        spark_op_1 = SparkOperator(\n",
    "            func=write_stuff,\n",
    "            task_id=\"test_stuff_1\",\n",
    "            queue=spark_queue,\n",
    "            spark_confs=spark_confs_1)  # NOTE THIS\n",
    "\n",
    "        spark_op_2 = SparkOperator(\n",
    "            func=write_stuff,\n",
    "            task_id=\"test_stuff_2\",\n",
    "            queue=spark_queue,\n",
    "            spark_confs=spark_confs_2)  # NOTE THIS\n",
    "\n",
    "        spark_op_1 >> spark_op_2\n",
    "\n",
    "    return dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This DAG will create one Spark cluster with two nodes each having 4 CPU cores and 2GB of memory. The two Spark applications will run sequentially, not competing for the same Spark cluster. \n",
    "\n",
    "Both Spark operators will allocate a context as big as cluster. The first operator will allocate a Spark context with 2 executors, 4 cores, and 2GB each. The second will allocate a Spark context with 4 executors, 2 cores, and 1GB RAM.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we were to structure the same DAG as below, both SparkOperators will run at the same time. However, because both operators ask for the full Spark cluster, the Spark jobs might run sequentially.  To truly run \n",
    "in parallel, you need to set spark.dynamicAllocation.maxExecutors = 1  in both Spark operators. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "dag >> [spark_op_1, spark_op_2]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Option 3: Adjusting the cluster size by adjusting the Spark Configurations in Job Request"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to specify values for the `spark_confs` in the job_request.yaml, that will get passed to the SparkOperator. Through the power of [Jinja2](http://jinja.pocoo.org) templating, any value in `spark_confs` dictionary can refer to configurations set in the job_request.yaml or any other DAG context attribute. An example might help to illustrate. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can define a metadata.yaml as follows, in which you will store the values for your Spark Configuration. In this case, we defined one as `spark_confs_for_op1`. For each of the properties that represent Spark Configurations to be passed to the Spark master, name the properties the same as the Spark Configuration key, replacing the periods with underscores.  For example, a Spark Configuration like `spark.executor.cores` would be created as a property called `spark_executor_cores`.  See below for an example. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "# metadata.yaml\n",
    "\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_spark_cluster_size_3\n",
    "recipe_version: \"0_0_0\"\n",
    "description: \"testing <your_name>'s example recipe\"\n",
    "configuration: \n",
    "    email:\n",
    "        description: This parameter will notify you of the success or failure of your dag run.\n",
    "        type: String\n",
    "        required: True\n",
    "    spark_confs_for_op1:\n",
    "        description: The Spark Configuration for SparkOperator 1\n",
    "        type: object\n",
    "        properties:\n",
    "            spark_executor_cores: \n",
    "                type: number\n",
    "            spark_executor_memory:\n",
    "                type: string\n",
    "        default:\n",
    "            spark_executor_cores: 1\n",
    "            spark_executor_memory: \"1G\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we define a job_request.yaml where we put values to the keys defined in metadata.yaml. Next, enter the desired [Spark Configuration](https://spark.apache.org/docs/latest/configuration.html) values that you wish to set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "# job_request.yaml\n",
    "\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_spark_cluster_size_3\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration:\n",
    "    email: e.xample@example.com\n",
    "    spark_confs_for_op1:\n",
    "        spark_executor_cores: 4\n",
    "        spark_executor_memory: \"2G\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the DAG needs to use the new values. Using [Jinja2](http://jinja.pocoo.org) syntax, you can refer to the  variables in the job request as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from dss_airflow_utils.spark import SparkOperator, SparkClusterStart\n",
    "from dss_airflow_utils.dag_factory import dag_factory, DagConfig, SparkConfig\n",
    "\n",
    "\n",
    "def write_stuff(spark, context):\n",
    "    print(\"Does something Spark Related\")\n",
    "\n",
    "    \n",
    "default_args = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 1,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "}\n",
    "    \n",
    "# NOTE THIS ---- NOTE THIS\n",
    "# Set custom Spark Configuration\n",
    "spark_confs_1 = {\n",
    "    'spark.executor.cores': '{{ dag_run.conf.configuration.spark_confs_for_op1.spark_executor_cores }}',\n",
    "    'spark.executor.memory': '{{ dag_run.conf.configuration.spark_confs_for_op1.spark_executor_memory }}',\n",
    "}\n",
    "# END NOTE THIS ---- END NOTE THIS\n",
    "\n",
    "# Size of Spark Driver\n",
    "spark_queue = {\n",
    "    \"worker_type\": \"spark-worker\",\n",
    "    \"request_memory\": \"1G\",\n",
    "    \"request_cpu\": \"1\"\n",
    "}\n",
    "\n",
    "@dag_factory(DagConfig(\n",
    "    # Size of Spark Cluster\n",
    "    SparkConfig(num_nodes=2, request_cpu_per_node=4, request_memory_gb_per_node=2)\n",
    "))\n",
    "def create_dag():\n",
    "    with DAG(dag_id='tutorial_spark_cluster_size_2', default_args=default_args, \n",
    "             schedule_interval=None) as dag:\n",
    "\n",
    "        spark_op_1 = SparkOperator(\n",
    "            func=write_stuff,\n",
    "            task_id=\"test_stuff_1\",\n",
    "            queue=spark_queue,\n",
    "            spark_confs=spark_confs_1)  # NOTE THIS\n",
    "        return dag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced Topic: Debug `spark_confs` set by the `scale` parameter\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to calculate what `spark_confs` will be set by a given `scale`, you can use the following code. This code assumes that you are NOT adjusting the `spark_confs` some other way in addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dss_airflow_utils.hooks.spark_hook import SparkHook\n",
    "from dss_airflow_utils.spark import SparkConfig\n",
    "\n",
    "# This is the configuration for the Spark cluster size.\n",
    "scd = SparkConfig(num_nodes=4, request_cpu_per_node=4, request_memory_gb_per_node=2)\n",
    "\n",
    "# This is the amount of RAM allocated for the Spark Driver and is the same as \n",
    "# the `request_memory` option in the queue for the SparkOperator\n",
    "# If no value is provided, this defaults to 4G.\n",
    "driver_memory = \"4G\"\n",
    "\n",
    "# This is the context scale factor that you want to test \n",
    "# (and would normally pass into the SparkOperator())\n",
    "scale = \".5\"\n",
    "\n",
    "SparkHook.adjust_spark_config_resources(\n",
    "    total_cores=scd.total_cores,\n",
    "    total_mem=scd.total_mem,\n",
    "    request_cpu_per_node=scd.request_cpu_per_node,\n",
    "    request_memory_gb_per_node=int(scd.request_memory_gb_per_node.strip(\"G\")),\n",
    "    driver_memory=\"1G\",\n",
    "    scale=scale)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced Topic: Example of Global templateable configs\n",
    "\n",
    "If you have configuration that apply to all of your Spark operators, you can create a `global` section in the `metadata.yaml` as defined below. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "# metadata.yaml\n",
    "\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_spark_cluster_size_3\n",
    "recipe_version: \"0_0_0\"\n",
    "description: \"testing <your_name>'s example recipe\"\n",
    "configuration: \n",
    "    email:\n",
    "        description: This parameter will notify you of the success or failure of your dag run.\n",
    "        type: String\n",
    "        required: True\n",
    "    spark_confs_for_op1:\n",
    "        description: The Spark Configuration for SparkOperator 1\n",
    "        type: object\n",
    "        properties:\n",
    "            spark_executor_memory:\n",
    "                type: string\n",
    "        default:\n",
    "            spark_executor_memory: \"1G\"\n",
    "    global:\n",
    "        description: The Spark Configuration for SparkOperator 1\n",
    "        type: object\n",
    "        properties:\n",
    "            spark_executor_cores: \n",
    "                type: number\n",
    "        default:\n",
    "            spark_executor_cores: 1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, you can create a `job_request.yaml` as follows: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```yaml\n",
    "spec_version: v2\n",
    "recipe_id: tutorial_spark_cluster_size_3\n",
    "recipe_version: \"0_0_0\"\n",
    "configuration:\n",
    "    email: e.xample@example.com\n",
    "    spark_confs_for_op1:\n",
    "        spark_executor_cores: 4\n",
    "    global:\n",
    "        spark_executor_memory: \"2G\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you would use this global section in a DAG as follows:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "  spark_op_1 = SparkOperator(\n",
    "    func=write_stuff,\n",
    "    task_id=\"test_stuff_1\",\n",
    "    queue=spark_queue,\n",
    "    spark_confs={\n",
    "        \"spark.executor.memory\": \"{{ dag_run.conf.configuration.global.spark_executor_memory }}\",\n",
    "        \"spark.executor.cores\": \"{{ dag_run.conf.configuration.spark_confs_for_op1.spark_executor_cores }}\",\n",
    "    })\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you would use this global section in a DAG as follows:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "  spark_op_1 = SparkOperator(\n",
    "    func=write_stuff,\n",
    "    task_id=\"test_stuff_1\",\n",
    "    queue=spark_queue,\n",
    "    spark_confs={\n",
    "        \"spark.executor.memory\": \"{{ dag_run.conf.configuration.global.spark_executor_memory }}\",\n",
    "        \"spark.executor.cores\": \"{{ dag_run.conf.configuration.spark_confs_for_op1.spark_executor_cores }}\",\n",
    "    })\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use a specific Spark version from a dag"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "from datetime import datetime, timedelta\n",
    "from airflow import DAG\n",
    "from operator import add\n",
    "\n",
    "from dss_airflow_utils.dag_factory import dag_factory\n",
    "from dss_airflow_utils.dag_factory import SparkConfig\n",
    "from dss_airflow_utils.dag_factory import DagConfig\n",
    "from dss_airflow_utils.spark import SparkOperator\n",
    "\n",
    "\n",
    "\n",
    "# These are some default arguments that will be passed to each tasks.\n",
    "# You can override them on a task basis if needed.\n",
    "DEFAULT_ARGS = {\n",
    "    'owner': 'airflow',\n",
    "    'start_date': datetime(2015, 6, 1),\n",
    "    'retries': 4,\n",
    "    'retry_delay': timedelta(seconds=10),\n",
    "    'queue': {                              # Must be either a string (imagename) or a dictionary of string -> string\n",
    "        \"request_memory\": \"2G\",            # and how much maximum computing resource a task will need\n",
    "        \"request_cpu\": \"1\"                  # don't worry, it starts small and will retry with more if it runs\n",
    "    }                                       # out of memory or CPU\n",
    "}\n",
    "\n",
    "SPARK_2_4_4 = \"spark2.4.4-python3.7-worker\"\n",
    "\n",
    "SPARK_QUEUE = {\n",
    "    \"worker_type\": SPARK_2_4_4,\n",
    "    \"request_memory\": \"1G\",\n",
    "    \"request_cpu\": \"1\"\n",
    "}\n",
    "\n",
    "SPARK_CONFIG = SparkConfig(\n",
    "    num_nodes=2,\n",
    "    request_cpu_per_node=2,\n",
    "    request_memory_per_node=2,\n",
    "    worker_type=SPARK_2_4_4\n",
    ")\n",
    "\n",
    "def test_spark2_4_4(spark, context):\n",
    "    sc = spark.sparkContext\n",
    "    spark_version = str(sc.version)\n",
    "    print('We are using Spark version {}'.format(spark_version))\n",
    "    assert spark_version == \"2.4.4\"\n",
    "\n",
    "\n",
    "# This decorator is required, so do not touch it!\n",
    "# Don't put anything between the decorator and def keyword\n",
    "@dag_factory(DagConfig(SPARK_CONFIG))\n",
    "def create_dag():  # pragma: no cover\n",
    "    with DAG(dag_id='spark2_4_4', default_args=DEFAULT_ARGS, schedule_interval=None) as dag:\n",
    "        # Define operators here, for example:\n",
    "\n",
    "        spark_test = SparkOperator(\n",
    "            func=test_spark2_4_4,\n",
    "            task_id=\"spark_test\",\n",
    "            queue=SPARK_QUEUE\n",
    "        )\n",
    "\n",
    "        return dag  # Do not change this\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
