{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Recipe Client\n",
    "\n",
    "This user guide covers:\n",
    "\n",
    "- What is the recipe client?\n",
    "- How to invoke the recipe client\n",
    "- Recipe Initialization\n",
    "- Recipe Validation\n",
    "- Job Request Validation\n",
    "- The structure of recipes\n",
    "- Recipe test and how it works\n",
    "- Test Running and Monitoring\n",
    "- Invoking the test command\n",
    "- Test Discovery and test naming requirements\n",
    "- Recipe Taste\n",
    "- Recipe Taste \\-\\-allow-multiple\n",
    "- Recipe Taste From/To\n",
    "- Recipe Publish\n",
    "- Recipe Run\n",
    "- Recipe Ingredient\n",
    "- Recipe Ingredient Add\n",
    "- Recipe Ingredient Get\n",
    "- Tolerance in an Ingredient get\n",
    "\n",
    "The recipe client is a command line interface that allows you to interact with several DSS services. The main use case of recipe client is to handle recipe lifecycle:\n",
    "```\n",
    "init -> validate -> test -> taste -> publish -> run\n",
    "\n",
    "```\n",
    "\n",
    "but it also has some other useful actions that are documented below.\n",
    "\n",
    "The recipe client is pre-installed into the notebook and can be invoked by typing the word 'recipe' either in the terminal or using notebook syntax to run shell scripts with '!', e.g. (try to run the next cell):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Usage:\n",
      "  recipe [--help] <command> [<args>...]\n"
     ]
    }
   ],
   "source": [
    "!recipe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All recipe client verbs (actions that you can do) are documented in the client itself . You can also get help on each verb by doing something like ```recipe publish --help```:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The Recipe Client connects to the Recipe Services API of the Analytics\n",
      "Services.\n",
      "\n",
      "Usage:\n",
      "  recipe [--help] <command> [<args>...]\n",
      "\n",
      "Commands:\n",
      "  publish         Publish the source folder of the recipe to the Recipe Library\n",
      "\n",
      "  run             Run in airflow the recipe indicated in the job request file.\n",
      "                  The recipe should be already published to the Recipe Library.\n",
      "\n",
      "  taste           Run in airflow the recipe indicated in the job request file\n",
      "                  without publishing it to the Recipe Library.\n",
      "\n",
      "  ingredient      Ingredient refers to the data your recipe relies on. This\n",
      "                  action will not trigger a recipe to run.\n",
      "\n",
      "    add           Adds data into the datacache.\n",
      "\n",
      "    get           Retrieves data in the datacache.\n",
      "\n",
      "  validate        Validate a job request file or the structure of a recipe\n",
      "                  source folder.\n",
      "\n",
      "  test            Run the tests defined in the recipe source folder.\n",
      "\n",
      "Options:\n",
      "  -h --help       Show this screen.\n",
      "  --version       Show version.\n"
     ]
    }
   ],
   "source": [
    "!recipe --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Init\n",
    "\n",
    "The `recipe init` command can be used to initialize a new recipe directory along with the base required files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Usage:\n",
      "    recipe init [<target_directory>] [options]\n",
      "\n",
      "Description:\n",
      "    Initialize a new recipe boilerplate directory.\n",
      "\n",
      "    The recipe init command can be run on a single recipe multiple times to add\n",
      "    \"recipe mixins\". Recipe mixins are recipe templates that allow you to easily add\n",
      "    additional boilerplate for different languages or features. For example, if\n",
      "    you want to add boilerplate to your existing recipe for building a Java project with\n",
      "    Maven, you can run recipe init with maven_java_mixin. See the Examples section below.\n",
      "    You can initialize a new recipe mixin from anywhere within the recipe directory tree.\n",
      "\n",
      "Options:\n",
      "  -h --help                                     Show this screen.\n",
      "  -t <template>, --template=<template>          One of the following:\n",
      "                                                - A DSS built-in template name\n",
      "                                                    Available templates: [python_recipe, maven_java_mixin]\n",
      "                                                    Default is python_recipe.\n",
      "                                                - Path to a directory or zip containing a cookiecutter project template\n",
      "                                                - Git url to a cookiecutter project template repository\n",
      "                                                Refer to the cookiecutter project documentation\n",
      "                                                (https://cookiecutter.readthedocs.io/en/latest/)\n",
      "                                                for additional help with what constitutes a valid template.\n",
      "  -c <context_dict>, --context=<context_dict>   A json mapping of values to use as the template\n",
      "                                                context. Specifying this option with -d, --disable-prompt\n",
      "                                                will opt-out of the user prompting and allows the values\n",
      "                                                to be passed in all at once.\n",
      "  -f <context_file>, --context-file=<context_file> A json file containing files to use as the template context.\n",
      "                                                If values are passed via -c, --context, those values will be merged\n",
      "                                                and take precedence over those in the context file. Otherwise similar to -c\n",
      "  -d, --disable-prompt                          Disable prompting. Will use default values\n",
      "                                                unless a context is passed in via -c, --context.\n",
      "\n",
      "Arguments:\n",
      "  <target_directory>                            Local file path to a directory to put recipe content in.\n",
      "                                                If directory and/or its parents do not exist, will create.\n",
      "                                                DEFAULT to current working directory.\n",
      "\n",
      "Examples:\n",
      "    recipe init -t python_recipe -dc '{\"recipe_id\": \"my-recipe\"}'\n",
      "    recipe init some_dir -t python_recipe -df values.json\n",
      "    recipe init . -t python_recipe -c '{\"recipe_id\": \"my-recipe\"}' -f values.json\n",
      "    recipe init -t <git-url-to-a-cookiecutter-project>\n",
      "\n",
      "    # Run recipe init multiple times in the same recipe to initialize 'recipe mixins'.\n",
      "    recipe init my_recipe\n",
      "    recipe init -t maven_java_mixin my_recipe\n"
     ]
    }
   ],
   "source": [
    "!recipe init --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Init Templates\n",
    "The recipe client comes bundled with several built-in templates - for example, the basic python recipe template that is used by default when calling `recipe init`. Other templates are available and can be used by specifying the name of the built-in template with the `-t` or `--template` option. Available options are listed in the `recipe init --help` output as well as in the output of the init command itself when an unknown template name is provided. \n",
    "\n",
    "As a recipe developer, you can also provide your own templates from a number of locations. The init verb takes advantage of an open-source python project called [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/). Cookiecutter provides a flexible framework for overcoming boilerplate project initialization through templating and loosely defines a structure for writing templates. As long as you have a cookiecutter-compliant template, you can use `recipe init -t <location-of-your-template>` to call your template.\n",
    "\n",
    "A particularly powerful feature of cookiecutter templates is that you can provide a template sourced from a number of different locations - a local file directory, zip archives, or even remote git repositories. Let's say your team has a particular structure and approach to writing recipes or a set of variables/files used in every recipe amongst your team. With the bring-your-own template methodology, you can create your own cookiecutter template, put it in a git repository, and call it directly from the recipe client via `recipe init -t ssh://git@blahblah` (http web url to the git repository works as well) with no extra installations required!\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Validate\n",
    "\n",
    "As seen from the help message, the ```recipe validate``` command can be used to validate both a job request and a recipe that is being developed and has not been published yet:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Usage:\n",
      "  recipe validate <job_request_file> [options]\n",
      "  recipe validate [<source_folder>] [options]\n",
      "\n",
      "Description:\n",
      "  Validate a job request file or the structure of a recipe source folder.\n",
      "\n",
      "Arguments:\n",
      "  <job_request_file>    Job request file. Inspect the structure of a job request. If the recipe name and version\n",
      "                        correspond to a published recipe, the job request configuration will be validated against the \n",
      "                        recipe's metadata configuration.\n",
      "  <source_folder>       Source folder of the recipe. Inspect the structure of the recipe and contents of the dag and \n",
      "                        metadata files. \n",
      "\n",
      "Options:\n",
      "  -h --help             Show this screen.\n",
      "  --api_url=<api_url>   The url of the API to talk to [default: http://recipe-service:8000/api/v1]\n"
     ]
    }
   ],
   "source": [
    "!recipe validate --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Validation\n",
    "DSS assumes a certain recipe directory structure and file contents. The structure of any recipe should follow that of the skeleton which can be seen below.\n",
    "\n",
    "\n",
    "```\n",
    "skeleton\n",
    "    |--dag\n",
    "    |   |--dag.py\n",
    "    |   |--__init__.py\n",
    "    |   |--lib\n",
    "    |      |--__init__.py\n",
    "    |      |--my_module.py\n",
    "    |--__init__.py\n",
    "    |--metadata.yaml\n",
    "    |--tests\n",
    "        |--__init__.py\n",
    "        |--test_lib\n",
    "            |--__init__.py\n",
    "            |--test_my_module.py\n",
    "            |--test.yaml\n",
    "    |--.coveragerc\n",
    "    |--.pylintrc\n",
    "```\n",
    "\n",
    "To make sure that your recipe is valid, run ```recipe validate``` command and provide an optional path to the recipe directory (otherwise the current directory will be used). Recipe validation is performed in two phases - local and server side validations. \n",
    "\n",
    "The local validation will specifically be looking for the following items:\n",
    "\n",
    "* \\_\\_init\\_\\_.py must be present in the root of the directory\n",
    "* /dag/dag.py must be present in the root of the directory\n",
    "* metadata.yaml must be present in the root of the directory\n",
    "* /tests/ must be present in the root of the directory\n",
    "* .coveragerc must be present in the root of the directory\n",
    "* .pylintrc must be present in the root of the directory\n",
    "\n",
    "Lastly, the local validation makes sure that the metadata.yaml has the correct schema.  The metadata.yaml must contain a\n",
    "spec\\_version, a recipe\\_id, and a recipe\\_version.\n",
    "\n",
    "If the local validation succeeded, the recipe is submitted to the recipe service for server side validation. Server side validation will perform additional syntax checks by simulating the way airflow processes the dag.\n",
    "\n",
    "Note, that once the recipe passes both local and server side validation, the recipe is not automatically submitted for publication. You need to run a separate ```recipe publish``` command if you wish to publish your recipe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Job Request Validation\n",
    "To run a published recipe, you will need to create a job request that (a) must be a valid job request and (b) must satisfy configuration requirements defined in that recipe metadata. If you just want to make sure that the job request that you wrote is a valid one, without accidentally triggering the recipe, the ```recipe validate``` command might be very handy.  Just provide a relative or absolute path to the job request file to the ```recipe validate``` command and see what it replies to you.  You can also run a job request through validation that has no corresponding published recipe in case you just want to validate the job request syntax."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Test\n",
    "\n",
    "The `recipe test` command can be used to run tests that you define in your recipe source directory. These tests can be Python-based tests or any tests that produce output in the JUnit format. As it is considered good software development practice to have code that is well tested, the `recipe test` command will be an invaluable tool for testing your code. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recipe test will:\n",
    "\n",
    " - Automatically discover the tests that you define (more info below). \n",
    " - Execute a dag run in Airflow which will run your tests and lint your code in an isolated environment.\n",
    " - Report all of the test output to a single location. \n",
    " - Encourage good software development habits.\n",
    " - Email you when your tests are complete."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test Command Usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simplest way to invoke `recipe test` is with the following command, where `source_folder` is the name of the folder containing your DAG source code:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```!recipe test source_folder```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find more information about the command usage by using the ```--help``` flag."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!recipe test --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test Discovery (required naming conventions)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Prior to running `recipe test`, you need to follow a few conventions so that your tests will be discovered by the test runner. \n",
    "\n",
    "- Everything test-related needs to be located in a `tests/` directory at the top of your `source_folder`.\n",
    "  - This folder must contain a file called `__init__.py` to mark it as a valid Python module.\n",
    "- Your tests must be placed in sub-directories of the `tests/` folder. \n",
    "  - Each `test_` sub-directory name must start with `test_`.\n",
    "    - For example, you might create a folder called `tests/test_lib`, which would contain your test modules.\n",
    "  - Each `test_` sub-directory must also contain an `__init__.py` file to mark it as a Python module.\n",
    "  - Each `test_` sub-directory must also contain a `test.yaml` file. The `test.yaml` file defines a number of configuration parameters that you most likely will not need to change. The example `test.yaml` copied in from the skeleton project should suit most cases.\n",
    "- You should define your actual test modules inside of the `test_` sub-directories.\n",
    "  - Each test module should be a file prefixed with a `test_` prefix, and should have a `.py` extension.\n",
    "    - For example, you might define a test file called `test_my_module.py`.\n",
    "  - Each test module should contain one or more valid Python unit tests (or other type of programmatic tests).\n",
    "\n",
    "Below is an example of how you should configure your testing file and directory structure. In this example, the `source_folder` is named `skeleton`.\n",
    "\n",
    "```\n",
    "skeleton\n",
    "    |--dag\n",
    "    ...\n",
    "    ...\n",
    "    |--metadata.yaml\n",
    "    |--tests\n",
    "        |--__init__.py\n",
    "        |--test_lib\n",
    "            |--__init__.py\n",
    "            |--test_my_module.py\n",
    "            |--test.yaml\n",
    "        |--test_foo\n",
    "            |--__init__.py\n",
    "            |--test_morel.py\n",
    "            |--test.yaml\n",
    "    |--.coveragerc\n",
    "    |--.pylintrc\n",
    "```\n",
    "\n",
    "**An important note:** tests in each test sub-directory will receive their own eponymously named task within the build dag run. This fact can be leveraged to create isolated environments between different groups of tests. If you have an advanced use-case, you can optionally change the `image` config parameter to a different value in a test sub-directorie's test.yaml file, so your tests can run using a different Docker base image.  If you do use a non-Python image, be sure to update the `test_command` to a different command that will produce JUnit test output. See below:\n",
    "\n",
    "```\n",
    "# test.yaml\n",
    "config:\n",
    "    image: python3-worker\n",
    "    test_command: python -m py.test $TEST_SUITE_PATH ...\n",
    "    ...\n",
    "    ...\n",
    "```\n",
    "\n",
    "**Additional note**: The .coveragerc and .pylintrc files are used to determine the required test coverage for the code (defaulted to 50%) and the enforced linting rules, respectively. Both can be updated as necessary.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test Running and Monitoring\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Executing `recipe test`, will trigger a dag run in Airflow that executes the tests found in the tests/ directory.  The output of the `recipe test` command will contain a URL to the dag run. You can copy that URL into a browser and monitor the progress of your tests.  See the \"Check Status at\" text in the command output.\n",
    "\n",
    "![alt text](images/recipe-test-example-01.jpg \"Recipe Test Example 1: Recipe Client Stdout\")\n",
    "\n",
    "The URL will take you to the graph view page of the dag run. Clicking on the 'test_lib' task and choosing 'View Log' will show you the output of your tests.\n",
    "\n",
    "![alt text](images/recipe-test-example-02.jpg \"Recipe Test Example 2: Airflow Build Dag Run Detail\")\n",
    "\n",
    "You should see output similar to the following. This can help you debug issues.\n",
    "\n",
    "![alt text](images/recipe-test-example-03.jpg \"Recipe Test Example 3: Test Task Pod Logs\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Taste\n",
    "\n",
    "The recipe taste command allows developers to run their recipes before they are published.  This is a powerful command that enables iterative development of dags.  Note that in the following descriptions the words 'recipe' and 'dag' are sometimes used interchangeably.  Speaking technically, a recipe could be a single dag or a collection of dags (where a parent dag spawns one or more child dags).\n",
    "\n",
    "To better understand how recipe taste can be used, execute the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!recipe taste --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recipe taste requires at minimum a recipe source folder and a job request.  If the command is executed from inside the top of the recipe's source folder, no source folder destination needs to be specified.  The recipe source folder should have the same structure and contents as the skeleton recipe created with `recipe init`, specifically the metadata.yaml and dag folder with corresponding dag.py file.  The job request should have a recipe version and id that match the information in the metadata as well as any configuration information.  \n",
    "\n",
    "When recipe taste is executed on a recipe, the recipe folder contents and job request will be sent to the recipe service which will kick off a dag run in Airflow.  The dag run will run the entire recipe, beginning to end.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Taste \\-\\-allow-multiple\n",
    "\n",
    "The recipe client will reject an attempt of recipe taste if a dag run corresponding to the tasted recipe version and id already exists and is in a running state.  This is a safety that is built into the recipe client.  When recipe taste is executed, it loads the recipe's source code into Airflow and initiates a dag run from that code.  If the recipe were to be tasted again, it would be dangerous to reload the code (and any changes made to it) with another instance of that dag already running.  To prevent any code confusion, recipe taste limits one taste of a dag at a time.\n",
    "\n",
    "The allow-multiple flag removes the restriction on the number of concurrent runs of recipe taste on a given recipe.  When the flag is used, the recipe service will not upload the recipe code fed with the command and instead it will use the existing recipe code in Airflow to fulfill the job request.  Any changes made to the code since the last execution of recipe taste will not be reflected in that run.  It is important to keep in mind that this flag can only be passed with recipe taste if the recipe being tasted is already in Airflow.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Taste From/To\n",
    "\n",
    "The \\-\\-run_id, \\-\\-from, \\-\\-to, and \\-\\-only-failed flags allows a developer to take an existing dag run of a recipe and re-execute some desired portion of that dag.  This recipe taste feature only requires the \\-\\-run_id flag to work.  **Using this feature requires that this recipe has been tasted at least once.**\n",
    "\n",
    "If recipe taste is supplied with only the run id, the recipe service will match the information from the job request and the run id to a dag run in Airflow and clear all of the tasks in that dag run and re-execute them.  When the --run_id flag is supplied, the --allow-multiple flag will be ignored.  This means that if the dag run specified by the recipe id, version, and run id is already in a running state, the recipe taste command will fail.  The dag run must be in a completed state, successful or failed, for this command to successfully execute. \n",
    "\n",
    "When the \\-\\-from or \\-\\-to flags are provided a single task id or list of task ids, the recipe developer can choose what portion of the dag to re-execute.  Let's take this dag run as an example:\n",
    "\n",
    "![complete_dag.png](./images/complete_dag.png)\n",
    "\n",
    "Here are the different use cases of how recipe taste from/to can be used. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Case 1 \\-\\-from is given a list of tasks\n",
    "\n",
    "The given tasks and their downstream dependencies will be cleared and re-executed. The dag run will start at the given tasks and run all the way to the end.  The correct syntax is displayed in the command seen below. If more than one task is provided, the tasks must be separated by a comma with no spaces in between.\n",
    "```\n",
    "recipe taste job_request.yaml --run_id example --from task_2_1,task_2_2\n",
    "``` \n",
    "\n",
    "![taste_from.png](./images/taste_from.png)\n",
    "\n",
    "Note that both tasks listed and their downstream dependencies are now cleared. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Case 2 \\-\\-to is given a task id\n",
    "The given tasks and their upstream dependencies will be cleared and re-executed. The dag run will start at the beginning and run until it reaches the given tasks.\n",
    "```\n",
    "recipe taste job_request.yaml --run_id example --to task_2_1\n",
    "``` \n",
    "\n",
    "![taste_to.png](./images/taste_to.png)\n",
    "\n",
    "This time only one task is fed into the command and only that  task and its upstream dependencies are cleared. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Case 3 Both \\-\\-from and \\-\\-to are given a list of tasks\n",
    "\n",
    "The given tasks and the tasks between them are cleared and re-executed.\n",
    "```\n",
    "recipe taste job_request.yaml --run_id example --from task_2_1 --to task_3\n",
    "``` \n",
    "\n",
    "![taste_from_to.png](./images/taste_from_to.png)\n",
    "\n",
    "The recipe service validates the tasks ids fed to it.  It ensures that the task listed in the \\-\\-to flag are downstream of the tasks listed in the \\-\\-from flag.  If the tasks in \\-\\-to do not follow the tasks in \\-\\-from, the recipe taste command will not be executed. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Case 4 The same task is given to \\-\\-from and \\-\\-to\n",
    "\n",
    "Only the specified task will be cleared and re-executed.\n",
    "\n",
    "```\n",
    "recipe taste job_request.yaml --run_id example --from task_3 --to task_3\n",
    "``` \n",
    "\n",
    "![taste_one.png](./images/taste_one.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This feature validates that the task ids fed into \\-\\-from and \\-\\-to exist in the dag that is called from the job request. The recipe taste from/to command will only recognize task ids inside the dag that is called by recipe taste.  The command will not be able to recognize any task ids of child dags spawned by the recipe.  The closest you can get to testing a child dag from within the tasted recipe is to assign the RecipeRunOperator task id to the \\-\\-from flag and the corresponding sensor to the \\-\\-to flag.  Another approach would be to execute the child dag on its own.\n",
    "\n",
    "When the \\-\\-only_failed flag is provided, only the failed tasks within the specified portion of the dag will be cleared and re-executed.\n",
    "\n",
    "The recipe taste from/to feature is particularly useful in iterative development because it allows for changes in the recipe's source code to be tested inside of Airflow without having to run the dag in full.  This functionality will operate even in the case that tasks are added or removed from the dag and will also support changes in the job request file.  \n",
    "\n",
    "The recipe service handles the creation and clean-up of the recipe's spark cluster.  The spark-cluster-start and spark-cluster-stop tasks do not need to be listed!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Publish\n",
    "Once you've finished developing a recipe and you are ready to make it available in the recipe library, you need to publish it. To do so, you need to execute the ```recipe publish``` command as:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```recipe publish source_folder```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find more information about the command usage by using the --help flag."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "usage: recipe publish [<source_folder>] [options]\r\n",
      "\r\n",
      "Description:\r\n",
      "  Publish the source folder of the recipe to the Recipe Library.\r\n",
      "\r\n",
      "Arguments:\r\n",
      "  <source_folder>       Source folder of the recipe.\r\n",
      "\r\n",
      "Options:\r\n",
      "  -h --help             Show this screen.\r\n",
      "  --api_url=<api_url>   The url of the API to talk to\r\n",
      "                        [default: http://recipe-service:8000/api/v1]\r\n"
     ]
    }
   ],
   "source": [
    "!recipe publish --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note, that the source_folder is optional if you execute the publish command from inside the recipe root folder.\n",
    "\n",
    "\n",
    "Recipe publish command will:\n",
    "1. Automatically validate the recipe (see Recipe Validate above)\n",
    "2. If recipe is valid, then it is automatically tested (see Recipe Test above). \n",
    "3. If all tests pass, then the recipe will be submitted for review in the form of a Pull Request (PR) on the recipe library repository\n",
    "4. Email you when your tests are complete and PR is opened.\n",
    "\n",
    "\n",
    "Note, once ```recipe publish``` command is executed, it will perform actions 1 and 2 in syncronous manner and will return control back to you as soon as tests start. It will not block your terminal session for the duration of the test. It is up to you to monitor the test job. Once tests are done, a PR will be opened automatically. Link to the PR will be included in the email notification.\n",
    "\n",
    "A recipe is not published to the library until the PR is merged into the master branch. You can use the PR process as a way to get feedback on your recipe from your teammates and make final changes to it. While the recipe is under review, it is not officially published yet - it is not listed in the recipe library and you can still make changes to it by re-executing ```recipe publish```. However, once the PR is merged, the recipe will be officially published and no more changes to that version of the recipe will be allowed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Run\n",
    "While ```recipe taste``` command allows you to execute an unpublished recipe from its source code, ```recipe run``` command allows you to execute a *published* recipe:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```recipe run job_request.yaml```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find more information about the command usage by using the --help flag."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Usage:\r\n",
      "  recipe run <job_request_file> [options]\r\n",
      "\r\n",
      "Description:\r\n",
      "  Run in airflow the recipe indicated in the job request file. The\r\n",
      "  recipe should be already published to the Recipe Library.\r\n",
      "\r\n",
      "Arguments:\r\n",
      "  <job_request_file>    Job request file which should identify a specific\r\n",
      "                        recipe.\r\n",
      "\r\n",
      "Options:\r\n",
      "  -h --help             Show this screen.\r\n",
      "  --api_url=<api_url>   The url of the API to talk to\r\n",
      "                        [default: http://recipe-service:8000/api/v1]\r\n"
     ]
    }
   ],
   "source": [
    "!recipe run --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note, that the run command will start the recipe but will not wait for its completion. It is up to you to monitor the recipe progress via the airflow UI. If you include your email in the job request, then you will be notified when the recipe completes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Recipe Ingredient\n",
    "Recipe ingredient facilitates data migration between your notebook and the datacache. It allows users to add data from their notebook to the datacache for later use in a recipe or for users to retrieve data from the datacache. NOTE this feature is meant to be used with small test data, using larger datasets can cause the notebook to crash. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Ingredient Add\n",
    "`!recipe ingredient add /some/path/to/data.csv`\n",
    "\n",
    "This will add the data found at the path \"/some/path/to/data.csv\" to the datacache. The command line output should tell the user what query and version to use in their recipes to retrieve this data. \n",
    "\n",
    "`recipe ingredient add` can also take the optional parameter `--query=<some string>`, this will put the data in the datacache and use the `<some string>` value as the query key in the datacache."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Recipe Ingredient Get\n",
    "`!recipe ingredient get /some/path/to/write/data.csv`\n",
    "\n",
    "This will retrieve data from the datacache and write it to \"/some/path/to/write/data.csv\" which the recipe developer can use for experimentation. \n",
    "\n",
    "### Tolerance in an ingredient get\n",
    "\n",
    "`recipe ingredient get` can take three different parameters\n",
    "`--tolerance_from=<from date>`, `--tolerance_to=<to date>` or `--version_id=<version id>`. These arguments should be familiar to developers who've used the datacache before.  Tolerances define a date range which will filter the versions of the query returned. The version id can be used to retrieve a specific version of a query."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "\n",
    "Next we continue with the way to run a recipe from a DAG, with a detailed look at [the RecipeRunOperator](./recipe-run-operator.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
